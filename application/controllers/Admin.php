<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    var $adminID='';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminData');
        if(!empty($this->session->userdata('adminuser')) && !empty($this->session->userdata('adminpass')) && !empty($this->session->userdata('adminrole')))
        {
            $data=$this->AdminData->valid($this->session->userdata('adminuser'),$this->session->userdata('adminpass'));
            if($data->num_rows()==1)
            {
                $user='';
                $pass='';
                foreach($data->result() as $row)
                {
                    $this->adminID=$row->id;
                    $user=$row->user;
                    $pass=$row->pass;
                    $this->session->set_userdata("adminprofile",$row->img);
                }
                if($user!=$this->session->userdata('adminuser') && $pass!=$this->session->userdata('adminpass'))
                {
                    session_destroy();
                    redirect(base_url().'Auth', 'refresh');
                }
            }else
            {
                session_destroy();
                redirect(base_url().'Auth', 'refresh');
            }
            
        }else
        {
            session_destroy();
            redirect(base_url().'Auth', 'refresh');
        }
    }

	public function index()
	{
	    $yr=$this->AdminData->getSetting(1);
	    $data['yr']=$yr;
	    $nav['nav']=1;
	    $nav['snav']=0;
	    $this->db->select('YEAR(trans.date) as date,COUNT(*) as c');
        $this->db->from('trans');
        $this->db->join('cart', 'cart.tid = trans.id');
        $this->db->where('cart.stat', 1);
        $this->db->where('cart.yr', $yr);
        $this->db->group_by('YEAR(trans.date)');
        $this->db->order_by('YEAR(trans.date)');
	    $data['d1']=$this->db->get();
	    $this->db->select('MONTH(trans.date) as date,COUNT(*) as c');
        $this->db->from('trans');
        $this->db->join('cart', 'cart.tid = trans.id');
        $this->db->where('cart.stat', 1);
        $this->db->where('cart.yr', $yr);
        $this->db->group_by('MONTH(trans.date)');
        $this->db->order_by('MONTH(trans.date)');
        $data['d2'] = $this->db->get();
        $this->db->select('code ,COUNT(*) as c');
        $this->db->from('enroll');
        $this->db->where('yr', $yr);
        $this->db->group_by('code');
        $data['d3']= $this->db->get();
        $this->db->select('code ,COUNT(*) as t, SUM(cer) as c');
        $this->db->from('enroll');
        $this->db->where('yr', $yr);
        $this->db->group_by('code');
        $data['d4']= $this->db->get();
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/main',$data);
		$this->load->view('admin/footer');
	}
	
	public function logout()
	{
	    session_destroy();
                    redirect(base_url().'Auth', 'refresh');
	}
	
	
	
	public function auth()
	{
		$this->load->view('admin/login');
	}
	public function slider($s)
	{
	    $data['s']=$s;
	    if($s==1)
	    {
	        $nav['snav']=21;
	        $data['slider']=$this->AdminData->select('mainslider');
	    }else
	    {
	        $nav['snav']=22;
	        $data['slider']=$this->AdminData->select('proslider');
	    }
	    $nav['nav']=2;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/slider',$data);
		$this->load->view('admin/footer');
	}
	public function addSlide($s)
	{
	    $data['s']=$s;
	    $nav['nav']=2;
	    $nav['snav']=0;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/slider/add',$data);
		$this->load->view('admin/footer');
	}
	
	public function editSlide($s,$id)
	{
	    $data['s']=$s;
	    if($s==1)
	    {
	        $nav['snav']=21;
	        $data['sider']=$this->AdminData->get($id,'mainslider');
	    }else
	    {
	        $nav['snav']=22;
	        $data['sider']=$this->AdminData->get($id,'proslider');
	    }
	    $nav['nav']=2;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/slider/edit',$data);
		$this->load->view('admin/footer');
	}
	public function deleteSlide($s,$id)
	{
	    if($s==1)
	    {
	        if($this->AdminData->delete($id,'mainslider'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
	    }else
	    {
	        if($this->AdminData->delete($id,'proslider'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
	    }
	}
	public function Page($p)
	{
	    $data['p']=$p;
	    if($p==1)
	    {
	        $data['page']=$this->AdminData->get($p,'pages');
	        $nav['snav']=31;
	    }elseif($p==2)
	    {
	        $data['page']=$this->AdminData->get($p,'pages');
	        $nav['snav']=32;
	    }else
	    {
	        $data['page']=$this->AdminData->get($p,'pages');
	        $nav['snav']=33;
	    }
	    $nav['nav']=3;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/page/addpage',$data);
		$this->load->view('admin/footer');
	}
	
	public function myProfile()
	{
	    $nav['nav']=1;
	    $nav['snav']=0;
	    $data['id']=$this->adminID;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/myProfile',$data);
		$this->load->view('admin/footer');
	}
	
	
	public function post()
	{
	    $nav['nav']=3;
	    $nav['snav']=34;
	    $data['post']=$this->AdminData->select('news');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/post',$data);
		$this->load->view('admin/footer');
	}
	
	public function addPost()
	{
	    $nav['nav']=3;
	    $nav['snav']=34;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/post/add');
		$this->load->view('admin/footer');
	}
	
	public function editPost($id)
	{
	   $data['post']=$this->AdminData->get($id,'news');
	    $nav['nav']=3;
	    $nav['snav']=34;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/post/edit',$data);
		$this->load->view('admin/footer');
	}
		public function deletePost($id)
	{

	        if($this->AdminData->delete($id,'news'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/post/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/post/");
            }
	}
	
	public function notice()
	{
	    $nav['nav']=11;
	    $nav['snav']=0;
	    $data['post']=$this->db->query('select * from notice where who=1');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/notice',$data);
		$this->load->view('admin/footer');
	}
	
	public function addNotice()
	{
	    $nav['nav']=11;
	    $nav['snav']=0;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/notice/add');
		$this->load->view('admin/footer');
	}
	
	public function editNotice($id)
	{
	   $data['post']=$this->AdminData->get($id,'notice');
	    $nav['nav']=3;
	    $nav['snav']=0;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/notice/edit',$data);
		$this->load->view('admin/footer');
	}
	
		public function deleteNotice($id)
	{

	        if($this->AdminData->delete($id,'notice'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/notice/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/notice/");
            }
	}
	
	public function courses()
	{
	    $nav['nav']=4;
	    $nav['snav']=0;
	    $data['courses']=$this->AdminData->select('courses');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/courses',$data);
		$this->load->view('admin/footer');
	}
		public function addCourses()
	{
	    $nav['nav']=4;
	    $nav['snav']=0;
	    $data['teachers']=$this->AdminData->select('teachers');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/courses/add',$data);
		$this->load->view('admin/footer');
	}
	public function editCourse($id)
	{
	   $data['data']=$this->AdminData->get($id,'courses');
	   $data['teachers']=$this->AdminData->select('teachers');
	    $nav['nav']=4;
	    $nav['snav']=0;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/courses/edit',$data);
		$this->load->view('admin/footer');
	}
	public function deleteCourse($id)
	{

	        if($this->AdminData->delete($id,'courses'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/courses/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/courses/");
            }
	}
	
	public function teachers()
	{
	    $nav['nav']=5;
	    $nav['snav']=0;
	    $data['teachers']=$this->AdminData->select('teachers');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/teachers',$data);
		$this->load->view('admin/footer');
	}
	
		public function addTeacher()
	{
	    $nav['nav']=5;
	    $nav['snav']=0;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/teachers/add');
		$this->load->view('admin/footer');
	}
	public function editTeacher($id)
	{
	   $data['teacher']=$this->AdminData->get($id,'teachers');
	    $nav['nav']=5;
	    $nav['snav']=0;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/teachers/edit',$data);
		$this->load->view('admin/footer');
	}
	public function deleteTeacher($id)
	{

	        if($this->AdminData->delete($id,'teachers'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/teachers/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/teachers/");
            }
	}
	
	public function inbox()
	{
	    $nav['nav']=1;
	    $nav['snav']=0;
	    $data['contact']=$this->AdminData->selectdes('contact');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/inbox',$data);
		$this->load->view('admin/footer');
	}
	
	public function viewInbox($id)
	{
	    $nav['nav']=1;
	    $nav['snav']=0;
	    $data['inbox']=$this->AdminData->get($id,'contact');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/inbox/view',$data);
		$this->load->view('admin/footer');
	}
	
	public function deleteInbox($id)
	{

	        if($this->AdminData->delete2($id,'contact'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/inbox/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/inbox/");
            }
	}
	
		public function students()
	{
	    $nav['nav']=6;
	    $nav['snav']=61;
	    $data['students']=$this->AdminData->selectdes('students');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/students',$data);
		$this->load->view('admin/footer');
	}
	public function student()
	{
	    $nav['nav']=6;
	    $nav['snav']=62;
	    $year=$this->AdminData->getSetting(1);
	    $data['students']=$this->AdminData->selectCurrStudent($year);
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/student',$data);
		$this->load->view('admin/footer');
	}
	public function editStudent($id)
	{
	    if(!empty($id))
	    {
    	    $nav['nav']=6;
    	    $nav['snav']=0;
    	    $data['student']=$this->AdminData->get($id,'students');
    		$this->load->view('admin/header',$nav);
    		$this->load->view('admin/students/edit',$data);
    		$this->load->view('admin/footer');
	    }
	    else
	    {
	        redirect(base_url()."Admin/students/");
	    }
	}
	public function viewStudent($id)
	{
	    if(!empty($id))
	    {
    	    $nav['nav']=6;
    	    $nav['snav']=0;
    	    $data['student']=$this->AdminData->get($id,'students');
    	    $data['courses']=$this->AdminData->select('courses');
    	    $data['enroll']=$this->AdminData->stdEnroll($id);
    		$this->load->view('admin/header',$nav);
    		$this->load->view('admin/students/view',$data);
    		$this->load->view('admin/footer');
	    }
	    else
	    {
	        redirect(base_url()."Admin/students/");
	    }
	}
	public function pDoc()
	{
    	    $nav['nav']=6;
    	    $nav['snav']=63;
    	    $data['student']=$this->AdminData->getDoc();
    		$this->load->view('admin/header',$nav);
    		$this->load->view('admin/students/pdoc',$data);
    		$this->load->view('admin/footer');

	}
	public function deleteStudent($id)
	{

	        if($this->AdminData->delete2($id,'students'))
            {
                $this->AdminData->deletef('uid',$id,'doc');
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/students/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/students/");
            }
	}

	public function penroll()
	{
	    $nav['nav']=7;
	    $nav['snav']=71;
	    $year=$this->AdminData->getSetting(1);
	    $data['enroll']=$this->AdminData->selectPenroll($year);
	    $data['type']=1;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/enroll',$data);
		$this->load->view('admin/footer');
	}
	
		public function viewPenroll($id)
	{
	    if(!empty($id))
	    {
    	    $nav['nav']=7;
    	    $nav['snav']=71;
    	    $data['enroll']=$this->AdminData->getPenroll($id);
    		$this->load->view('admin/header',$nav);
    		$this->load->view('admin/enroll/viewPenroll',$data);
    		$this->load->view('admin/footer');
	    }else
	    {
	        redirect(base_url()."Admin/penroll/");
	    }
	}

	public function oenroll()
	{
	    $nav['nav']=7;
	    $nav['snav']=72;
	    $year=$this->AdminData->getSetting(1);
	    $data['enroll']=$this->AdminData->selectOenroll($year);
	    $data['type']=2;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/enroll',$data);
		$this->load->view('admin/footer');
	}
	
		public function viewOenroll($id)
	{
	    if(!empty($id))
	    {
    	    $nav['nav']=7;
    	    $nav['snav']=72;
    	    $data['enroll']=$this->AdminData->getOenroll($id);
    		$this->load->view('admin/header',$nav);
    		$this->load->view('admin/enroll/viewOenroll',$data);
    		$this->load->view('admin/footer');
	    }else
	    {
	        redirect(base_url()."Admin/penroll/");
	    }
	}
	public function cenroll()
	{
	    $nav['nav']=7;
	    $nav['snav']=73;
	    $year=$this->AdminData->getSetting(1);
	    $data['enroll']=$this->AdminData->selectCenroll($year);
	    $data['type']=3;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/enroll',$data);
		$this->load->view('admin/footer');
	}
	
	public function viewCenroll($id)
	{
	    if(!empty($id))
	    {
    	    $nav['nav']=7;
    	    $nav['snav']=73;
    	    $data['enroll']=$this->AdminData->getCenroll($id);
    		$this->load->view('admin/header',$nav);
    		$this->load->view('admin/enroll/viewCenroll',$data);
    		$this->load->view('admin/footer');
	    }else
	    {
	        redirect(base_url()."Admin/penroll/");
	    }
	}
	public function aenroll()
	{
	    $nav['nav']=7;
	    $nav['snav']=74;
	    $year=$this->AdminData->getSetting(1);
	    $data['enroll']=$this->AdminData->selectAenroll($year);
	    $data['type']=4;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/enroll',$data);
		$this->load->view('admin/footer');
	}
	public function viewAenroll($id)
	{
	    if(!empty($id))
	    {
    	    $nav['nav']=7;
    	    $nav['snav']=74;
    	    $data['enroll']=$this->AdminData->getAenroll($id);
    		$this->load->view('admin/header',$nav);
    		$this->load->view('admin/enroll/viewAenroll',$data);
    		$this->load->view('admin/footer');
	    }else
	    {
	        redirect(base_url()."Admin/penroll/");
	    }
	}
	
	public function addBatch()
	{
	    $nav['nav']=8;
	    $nav['snav']=81;
	    $c=array();
	    $q=$this->db->query("select distinct(code) from enroll");
	    foreach($q->result() as $row)
	    {
	        $c[]=$row->code;
	    }
	    $this->db->select('title,id,code,price');
        $this->db->where_in('code', $c);
        $data['batch']=$this->db->get('courses');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/batch/add',$data);
		$this->load->view('admin/footer');
	}
	public function pbatch()
	{
	    $nav['nav']=8;
	    $nav['snav']=81;
	    $year=$this->AdminData->getSetting(1);
	    $data['batch']=$this->AdminData->selectPbatch($year);
	    $data['tbatch']=$this->db->query("select distinct(code) from enroll where bid=0");
	    $data['bbatch']=$this->AdminData->selectPbatchstd();
	    $data['sbatch']=$this->db->query('select id, code,sr,yr from batch where stat=0');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/pbatch',$data);
		$this->load->view('admin/footer');
	}
	
	public function cbatch()
	{
	    $nav['nav']=8;
	    $nav['snav']=82;
	    $year=$this->AdminData->getSetting(1);
	    $data['batch']=$this->AdminData->selectcbatch($year);
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/cbatch',$data);
		$this->load->view('admin/footer');
	}
	
	public function pbatchView($id)
	{
	    $nav['nav']=8;
	    $nav['snav']=81;
	    $data['batch']=$this->AdminData->getPbatch($id);
	    $data['students']=$this->AdminData->selectPbatchstdi($id);
	    $data['teacher']=$this->AdminData->select('teachers');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/batch/pview',$data);
		$this->load->view('admin/footer');
	}
	public function cbatchView($id)
	{
	    $nav['nav']=8;
	    $nav['snav']=82;
	    $data['batch']=$this->AdminData->getCbatch($id);
	    $data['teacher']=$this->AdminData->select('teachers');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/batch/cview',$data);
		$this->load->view('admin/footer');
	}
	
		
		public function attend($id)
	{
	    if(!empty($id))
	    {
	        $nav['nav']=2;
	        $nav['snav']=82;
	        $data['id']=$id;
	        $data['bid']=$id;
	        $data['batch']=$this->db->query('select attend from batch where id='.$id);
	        $this->db->select('atten.data,students.name,students.lname,atten.pday,atten.tday,atten.total,atten.id');
            $this->db->from('atten');
            $this->db->join('batch', 'batch.id = atten.bid');
            $this->db->join('enroll', 'enroll.id = atten.eid');
            $this->db->join('students', 'students.id = enroll.uid');
            $this->db->where('atten.bid', $id);
            $data['s']= $this->db->get();
	        $this->load->view('admin/header',$nav);
    		$this->load->view('admin/batch/attend',$data);
    		$this->load->view('admin/footer');
	        
	    }else
	    {
	        redirect(base_url().'Admin/cbatch', 'refresh');
	    }
	}
	
			public function tattend($id)
	{
	    if(!empty($id))
	    {
	        $nav['nav']=2;
	        $nav['snav']=82;
	        $data['id']=$id;
	        $data['bid']=$id;
	        $data['batch']=$this->db->query('select attend from batch where id='.$id);
	        $this->db->select('teachers.name,batch.yr,batch.sr,batch.code');
            $this->db->from('teachers');
            $this->db->join('batch', 'batch.tid = teachers.id');
            $this->db->where('batch.id', $id);
            $data['s']= $this->db->get();
	        $this->load->view('admin/header',$nav);
    		$this->load->view('admin/batch/tattend',$data);
    		$this->load->view('admin/footer');
	        
	    }else
	    {
	        redirect(base_url().'Admin/cbatch', 'refresh');
	    }
	}
	
		public function bstudents($id)
	{
	    if(!empty($id))
	    {
	        $nav['nav']=8;
	        $nav['snav']=82;
	        $data['id']=$id;
	        $this->db->select('students.name,students.lname,students.email,students.mob,enroll.id,enroll.stat');
            $this->db->from('enroll');
            $this->db->join('students', 'students.id = enroll.uid');
            $this->db->where('enroll.bid', $id);
            $data['students']= $this->db->get();
	        $this->load->view('admin/header',$nav);
    		$this->load->view('admin/batch/students',$data);
    		$this->load->view('admin/footer');
	        
	    }else
	    {
	        redirect(base_url().'admin/cbatch', 'refresh');
	    }   
	}
	
	public function tool()
	{
	    $this->db->update('enroll', array('stat'=>$_POST['c']), array('id' => $_POST['id']));
	}
	
		public function result($id)
	{
	    if(!empty($id))
	    {
	        $nav['nav']=8;
	        $nav['snav']=82;
	        $data['id']=$id;
            $data['result']= $this->AdminData->getf('eid',$id,'result');
	        $this->load->view('admin/header',$nav);
    		$this->load->view('admin/batch/result',$data);
    		$this->load->view('admin/footer');
	        
	    }else
	    {
	        redirect(base_url().'admin/cbatch', 'refresh');
	    }   
	}
	
			public function getMarks($id)
	{
	    if(!empty($id))
	    {
            $q= $this->AdminData->getf('eid',$id,'result');
            echo "<table class='table'>";
            echo "<tr><th>Evaluación</th><th>Examen</th><th>Calificación</th></tr>";
            $i=1;
            foreach($q->result() as $row)
            {
                echo "<tr><th>".$i."</th><th>".$row->title."</th><th>".$row->marks."</th></tr>";
                $i++;
            }
            echo "</table>";
	        
	    } 
	}
	
	public function addResult($id=0)
	{
	        $data=array();
	        $nav['nav']=8;
	        $nav['snav']=82;
	        if(!empty($id))
	        {
	            $data['id']=$id;
	        }
	        if(!empty($rid))
	        {
	            $data['result']=$this->AdminData->get($rid,'result');
	        }
	        $this->load->view('admin/header',$nav);
    		$this->load->view('admin/batch/addResult',$data);
    		$this->load->view('admin/footer');
	}
	
		public function viewResult($id,$rid)
	{
	        $data=array();
	        $nav['nav']=8;
	        $nav['snav']=82;
	        if(!empty($id))
	        {
	            $data['id']=$id;
	        }
	        if(!empty($rid))
	        {
	            $data['result']=$this->AdminData->get($rid,'result');
	        }
	        $this->load->view('admin/header',$nav);
    		$this->load->view('admin/batch/viewResult',$data);
    		$this->load->view('admin/footer');
	}
	
		public function deleteResult($id,$rid)
	{
	    
	        if($this->AdminData->delete2($rid,'result'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/result/".$id);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/result/".$id);
            }
	}
	
	public function gen($coid)
	{
		$data='';
		$q=$this->db->query('select * from courses where id='.$coid);
	                    foreach($q->result() as $row)
	                    {
	                        $data=$row->data;
	                    }
	    $data=json_decode($data);
		$date1=$data->start;
		$date2=$data->end;
		$dt=$data->daytime;
		$day=array();
		foreach($dt as $r)
		{
			$day[]=$r->day;
		}
		$date=array();
		$d=0;
		$atten=array();
		while (strtotime($date1) <= strtotime($date2)) {
			$date1=date('Y-m-d',strtotime("1 day",strtotime($date1)));
			if(in_array(date('w',strtotime($date1)),$day))
			{
				$date[]=$date1;
				$atten[$date1]=0;
				$d++;
			}
		}
		print_r($date);
		print_r($atten);
		echo $d.'<br>';
		echo json_encode($atten);
		
	}
	
	public function export()
	{
	    $nav['nav']=9;
	    $nav['snav']=0;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/export');
		$this->load->view('admin/footer');
	}
	
	public function settings()
	{
	    $nav['nav']=12;
	    $nav['snav']=0;
	    $data['year']=$this->AdminData->getSetting(1);
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/settings',$data);
		$this->load->view('admin/footer');
	}
	
	public function user()
	{
	    $nav['nav']=10;
	    $nav['snav']=0;
	    $data['user']=$this->db->query('select * from admin where role not IN(3)');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/user',$data);
		$this->load->view('admin/footer');
	}
	
	public function addUser()
	{
	    $nav['nav']=10;
	    $nav['snav']=0;
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/user/add');
		$this->load->view('admin/footer');
	}
		public function editUser($id)
	{
	    $nav['nav']=10;
	    $nav['snav']=0;
	    $data['user']=$this->AdminData->get($id,'admin');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/user/edit',$data);
		$this->load->view('admin/footer');
	}
	
			public function deleteUser($id)
	{
	    
	        if($this->AdminData->delete2($id,'admin'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/user/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/user/");
            }
	}
	
	public function tview()
	{
	    $nav['nav']=5;
	    $nav['snav']=51;
	    $data['teachers']=$this->db->query('select * from teachers order by name');
		$this->load->view('admin/header',$nav);
		$this->load->view('admin/teachers/view',$data);
		$this->load->view('admin/footer');
	}
}