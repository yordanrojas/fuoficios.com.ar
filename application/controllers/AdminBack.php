<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminBack extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminData');
    }
    public function index()
    {
        
    }
    
    public function teachData()
    {
        if(!empty($_POST['id']))
        {
            $yr=$this->AdminData->getSetting(1);
            $q=$this->db->query('select * from batch where tid='.$_POST['id'].' and yr='.$yr.' order by dfrm,dto');
            if($q->num_rows()>0)
            {
                foreach($q->result() as $row)
                {
                    $curr='';
                    $a=json_decode($row->attend);
                    $data='Curso sobre:- ';
                    foreach($a as $d=>$k)
                    {
                        $data.=date('d/m/Y',strtotime($d)).',&nbsp;';
                        if($d==date('Y-m-d'))
                        {
                            $curr=' (Hoy)';
                        }
                    }
                    $date1=date_create(date('Y-m-d'));
                    $date2=date_create($row->dfrm);
                    $diff=date_diff($date1,$date2);
                    $v=$diff->format("%R%a");
                    $date3=date_create($row->dto);
                    $diff2=date_diff($date1,$date3);
                    $v2=$diff2->format("%R%a");
                    ?>
                    <a class="btn <?php if($v<0&&$v2<0){ echo "btn-danger";} if($v>0&&$v2>0){ echo "btn-warning";} if($v<=0 && ($v2>0 || $v2==0)){ echo "btn-success";}?> mb-1 ml-1" href="<?php echo base_url();?>Admin/cbatchView/<?php echo $row->id;?>" data-toggle="tooltip" data-html="true" title="<?php echo $data;?>"><?php echo $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr)).$curr;?><br><?php echo date('d/m/Y',strtotime($row->dfrm));?>-<?php echo date('d/m/Y',strtotime($row->dto));?></a>
                    <?php
                }
            }
            else
            {
                echo 'Ocioso';
            }
        }else
        {
            echo 'NULL';
        }
    }
    
    public function addBatch()
    {
        if(!empty($_POST['v']) && !empty($_POST['v2']))
        {
            $data=json_decode($_POST['v']);
            $code=$data->code;
            $coid=$data->id;
            $price=$data->price;
            $title=$data->title;
            $yr=$this->AdminData->getSetting(1);
            $q=$this->db->query('select * from batch where code="'.$code.'" && sr="'.$_POST['v2'].'" && yr="'.$yr.'"');
            if($q->num_rows()==0)
            {
                $this->db->insert('batch',array('code'=>$code,'sr'=>$_POST['v2'],'yr'=>$yr,'coid'=>$coid,'title'=>$title,'tid'=>0,'count'=>0,'price'=>$price));
                echo json_encode(array('stat'=>1,'msg'=>"Grupo agregado exitosamente"));
            }else
            {
                echo json_encode(array('stat'=>0,'msg'=>"El grupo ya existe para este año."));
            }
            
        }else
        {
            echo json_encode(array('stat'=>0,'msg'=>"Parámetro faltante"));
        }
    }
    
        public function addBatchstd()
    {
        if(!empty($_POST['b']) && !empty($_POST['e']))
        {
            $data=json_decode($_POST['b']);
            $sr=$data->sr;
            $bid=$data->id;
            $yr=$data->yr;
            $d=array(
                'bid'=>$bid,
                'yr'=>$yr,
                'sr'=>$sr
                );
            if($this->AdminData->update($_POST['e'],$d,'enroll'))
            {
                $this->db->set('count', 'count+1', FALSE);
                $this->db->where('id', $bid);
                $this->db->update('batch');
                echo json_encode(array('stat'=>1,'msg'=>"kk"));
            }else
            {
                echo json_encode(array('stat'=>0,'msg'=>"Se produjo un error"));
            }
            
        }else
        {
            echo json_encode(array('stat'=>0,'msg'=>"Parámetro faltante"));
        }
    }
    
    
    public function tool()
	{
	    $this->db->update('enroll', array('ins'=>$_POST['c']), array('id' => $_POST['id']));
	}
	 public function hideCourse()
	{
	    $this->db->update('courses', array('hide'=>$_POST['c']), array('id' => $_POST['id']));
	}
	public function hideSlide()
	{
	    $this->db->update('mainslider', array('hide'=>$_POST['c']), array('id' => $_POST['id']));
	}
    public function addSlide($s)
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/slider/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/slider/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/slider/".$s);
        }    
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Imagen no encontrada',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/slider/".$s);
        }
        if($s==1)
        {
            $data=array(
                'title'=>$this->input->post('title'),
                'des'=>$this->input->post('des'),
                'img'=>$img,
                'pos'=>$this->input->post('pos'));
            if($this->AdminData->add($data,'mainslider'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Control deslizante agregado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
        }else
        {
            $data=array(
                'title'=>$this->input->post('title'),
                'img'=>$img,
                'pos'=>$this->input->post('pos'));
            if($this->AdminData->add($data,'proslider'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Control deslizante agregado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
        }
    }
    
    public function editSlide($s,$id)
    {
        $data=array();
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/slider/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/slider/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/slider/".$s);
        }    
        }
        if($s==1)
        {
            $data=array(
                'title'=>$this->input->post('title'),
                'des'=>$this->input->post('des'),
                'pos'=>$this->input->post('pos'));
            if(!empty($img))
            {
                $data['img']=$img;
            }
            if($this->AdminData->update($id,$data,'mainslider'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
        }else
        {
            $data=array(
                'title'=>$this->input->post('title'),
                'pos'=>$this->input->post('pos'));
            if(!empty($img))
            {
                $data['img']=$img;
            }
            if($this->AdminData->update($id,$data,'proslider'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/slider/".$s);
            }
        }
    }
    
    public function addPage($p)
    {
        if($p==1)
        {
            $img='';
            if (!empty($_FILES['abtimg']['name']))
            {
                $config['upload_path'] = 'images/';
                $config['allowed_types'] = '*';
                $config['file_name'] = md5(uniqid()) . $_FILES['abtimg']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('abtimg'))
                {
                    $uploadData = $this->upload->data();
                    $img = 'images/'.$uploadData['file_name'];
                    
                }
            }
            $data1=array(
                'feat1'=>$this->input->post('feat1'),
                'feat2'=>$this->input->post('feat2'),
                'feat3'=>$this->input->post('feat3'),
                'des1'=>$this->input->post('fdes1'),
                'des2'=>$this->input->post('fdes2'),
                'des3'=>$this->input->post('fdes3'));
            $data2=array('abt'=>$this->input->post('abt'));
            if(!empty($img))
            {
                $data2['img']=$img;
            }else
            {
                $data2['img']=$this->input->post('himg');
            }
            $data3=array(
                'count1'=>$this->input->post('count1'),
                'count2'=>$this->input->post('count2'),
                'count3'=>$this->input->post('count3'));
            $data4=array(
                'why'=>$this->input->post('why'),
                'vwhy'=>$this->input->post('vwhy'));
            $data5=array(
                'link1'=>$this->input->post('link1'),
                'link2'=>$this->input->post('link2'),
                'link3'=>$this->input->post('link3'),
                'link4'=>$this->input->post('link4'),
                'link5'=>$this->input->post('link5'));
            $data=array(
                'data1'=>json_encode($data1),
                'data2'=>json_encode($data2),
                'data3'=>json_encode($data3),
                'data4'=>json_encode($data4),
                'data5'=>json_encode($data5));
            if($this->AdminData->update(1,$data,'pages'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/page/".$p);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/page/".$p);
            }
        }
        elseif($p==2)
        {
            $data1=array('vission'=>$this->input->post('vision'));
            $data2=array('mission'=>$this->input->post('mission'));
            $data3=array(
                'v1'=>$this->input->post('v1'),
                'v2'=>$this->input->post('v2'),
                'v3'=>$this->input->post('v3'),
                'v4'=>$this->input->post('v4'),
                'v5'=>$this->input->post('v5'),
                'v6'=>$this->input->post('v6'),
                'vd1'=>$this->input->post('vd1'),
                'vd2'=>$this->input->post('vd2'),
                'vd3'=>$this->input->post('vd3'),
                'vd4'=>$this->input->post('vd4'),
                'vd5'=>$this->input->post('vd5'),
                'vd6'=>$this->input->post('vd6'));
            $data4=array(
                'g1'=>$this->input->post('g1'),
                'g2'=>$this->input->post('g2'),
                'g3'=>$this->input->post('g3'));
            $data5=array(
                'p1'=>$this->input->post('p1'),
                'p2'=>$this->input->post('p2'),
                'p3'=>$this->input->post('p3'),
                'p4'=>$this->input->post('p4'),
                'p5'=>$this->input->post('p5'));
            $data=array(
                'data1'=>json_encode($data1),
                'data2'=>json_encode($data2),
                'data3'=>json_encode($data3),
                'data4'=>json_encode($data4),
                'data5'=>json_encode($data5));
            if($this->AdminData->update(2,$data,'pages'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/page/".$p);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/page/".$p);
            }
        }else
        {
            $data1=array('ph1'=>$this->input->post('ph1'),
                        'ph2'=>$this->input->post('ph2'),
                        'ph3'=>$this->input->post('ph3'));
            $data2=array('ng'=>$this->input->post('ng'));
            $data3=array('mail'=>$this->input->post('mail'));
            $data=array(
                'data1'=>json_encode($data1),
                'data2'=>json_encode($data2),
                'data3'=>json_encode($data3),
                'data4'=>json_encode(array()),
                'data5'=>json_encode(array()));
            if($this->AdminData->update(3,$data,'pages'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/page/".$p);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/page/".$p);
            }
        }
    }
    
    public function addTeacher()
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/teacher/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/teacher/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/teachers/");
        }    
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Imagen no encontrada',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/teachers/");
        }
        if(empty($_POST['name']) && empty($_POST['dsg']) && empty($_POST['bio']) && empty($_POST['mob']) && empty($_POST['email']) && empty($_POST['pass']) && empty($img))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/teachers/");
        }else
        {
            $data=array(
                'name'=>$this->input->post('name'),
                'lname'=>$this->input->post('lname'),
                'dsg'=>$this->input->post('dsg'),
                'bio'=>$this->input->post('bio'),
                'mob'=>$this->input->post('mob'),
                'email'=>$this->input->post('email'),
                'pass'=>md5($this->input->post('pass')),
                'img'=>$img
                );
             if($this->AdminData->add($data,'teachers'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Agregado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/teachers/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/teachers/");
            }
        }
    }
    public function updateTeacher($id)
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/teacher/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/teacher/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/teachers/");
        }    
        }
        if(empty($_POST['name']) && empty($_POST['dsg']) && empty($_POST['bio']) && empty($_POST['mob']) && empty($_POST['email']))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/teachers/");
        }else
        {
            $data=array(
                'name'=>$this->input->post('name'),
                'lname'=>$this->input->post('lname'),
                'dsg'=>$this->input->post('dsg'),
                'bio'=>$this->input->post('bio'),
                'mob'=>$this->input->post('mob'),
                'email'=>$this->input->post('email'));
            if(!empty($_POST['pass']))
            {
                $data['pass']=md5($this->input->post('pass'));
            }
            if(!empty($img))
            {
                $data['img']=$img;
            }
            if($this->AdminData->update($id,$data,'teachers'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/teachers/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/teachers/");
            }
        }
    }
    
        public function updateProfile($id)
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/myProfile/");
        }    
        }
        if(empty($_POST['name']))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/myProfile/");
        }else
        {
            $data=array(
                'user'=>$this->input->post('name'));
            if(!empty($_POST['pass']))
            {
                $data['pass']=md5($this->input->post('pass'));
            }
            if(!empty($img))
            {
                $data['img']=$img;
            }
            if($this->AdminData->update($id,$data,'admin'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/myProfile/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/myProfile/");
            }
        }
    }
    
    
        public function addPost()
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/post/");
        }    
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Imagen no encontrada',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/post/");
        }
        if(empty($_POST['title']) && empty($_POST['des']) && empty($_POST['type']) && empty($img))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/teachers/");
        }else
        {
            $data=array(
                'title'=>$this->input->post('title'),
                'des'=>$this->input->post('des'),
                'type'=>$this->input->post('type'),
                'img'=>$img
                );
             if($this->AdminData->add($data,'news'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Agregado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/post/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/post/");
            }
        }
    }
    
        public function editPost($id)
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/post/");
        }    
        }
        if(empty($_POST['title']) && empty($_POST['des']) && empty($_POST['type']))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/post/");
        }else
        {
            $data=array(
                'title'=>$this->input->post('title'),
                'des'=>$this->input->post('des'),
                'type'=>$this->input->post('type')
                );

            if(!empty($img))
            {
                $data['img']=$img;
            }
            if($this->AdminData->update($id,$data,'news'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/post/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/post/");
            }
        }
    }
    
    
            public function addNotice()
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/notice/");
        }    
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Imagen no encontrada',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/notice/");
        }
        if(empty($_POST['title']) && empty($_POST['des']) && empty($img))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/notice/");
        }else
        {
            $data=array(
                'sub'=>$this->input->post('title'),
                'cont'=>$this->input->post('des'),
                'img'=>$img,
                'who'=>1,
                'uid'=>0,
                'type'=>0
                );
             if($this->AdminData->add($data,'notice'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Agregado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/notice/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/notice/");
            }
        }
    }
    
            public function editNotice($id)
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/notice/");
        }    
        }
        if(empty($_POST['title']) && empty($_POST['des']) )
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/notice/");
        }else
        {
            $data=array(
                'sub'=>$this->input->post('title'),
                'cont'=>$this->input->post('des')
                );

            if(!empty($img))
            {
                $data['img']=$img;
            }
            if($this->AdminData->update($id,$data,'notice'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/notice/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/notice/");
            }
        }
    }
    
    public function addCourses()
    {
        $imgs=array();
        $pat=array();
        for($i=1;$i<=5;$i++)
        {
            if (!empty($_FILES['img'.$i]['name']))
            {
            $config['upload_path'] = 'images/course/';
            $config['allowed_types'] = '*';
            $config['file_name'] = md5(uniqid()) . $_FILES['img'.$i]['name'];
            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
                if ($this->upload->do_upload('img'.$i))
                {
                    $uploadData = $this->upload->data();
                    $imgs[] = 'images/course/'.$uploadData['file_name'];
                }
            }  
        }
        for($i=1;$i<=5;$i++)
        {
            if (!empty($_FILES['pat'.$i]['name']))
            {
            $config['upload_path'] = 'images/course/';
            $config['allowed_types'] = '*';
            $config['file_name'] = md5(uniqid()) . $_FILES['pat'.$i]['name'];
            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
                if ($this->upload->do_upload('pat'.$i))
                {
                    $uploadData = $this->upload->data();
                    $pat[] = 'images/course/'.$uploadData['file_name'];
                }
            }  
        }
        $daytime=array();
        for($i=1;$i<=6;$i++)
        {
            if (in_array($i, $_POST['day']))
            {
                $daytime[]=array('day'=>$i,'from'=>$_POST['days'.$i],'to'=>$_POST['daye'.$i]);
            }
        }
        $cdata=array(
            'start'=>$this->input->post('start'),
            'end'=>$this->input->post('end'),
            'dur'=>$this->input->post('dur'),
            'durin'=>$this->input->post('durin'),
            'daytime'=>$daytime,
            'elig'=>$this->input->post('elig'),
            'cont'=>$this->input->post('cont'),
            'obj'=>$this->input->post('obj'),
            'meth'=>$this->input->post('meth'),
            'ben'=>$this->input->post('ben'),
            'teacher'=>$this->input->post('teacher'),
            'pat'=>$pat,
            'link'=>$this->input->post('link')
            );
        $data=array(
            'title'=>$this->input->post('title'),
            'stitle'=>$this->input->post('stitle'),
            'des'=>$this->input->post('dsc'),
            'type'=>$this->input->post('type'),
            'cat'=>$this->input->post('cat'),
            'area'=>$this->input->post('area'),
            'data'=>json_encode($cdata),
            'imgs'=>json_encode($imgs),
            'date'=>$this->input->post('ddate'),
            'code'=>$this->input->post('code'),
            'code2'=>$this->input->post('code2'),
            'price'=>$this->input->post('price')
            );
            
             if($this->AdminData->add($data,'courses'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Agregado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/courses/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/courses/");
            }
    }
    public function updateCourses($id)
    {
        $imgs=array();
        $pat=array();
        for($i=1;$i<=5;$i++)
        {
            if (!empty($_FILES['img'.$i]['name']))
            {
            $config['upload_path'] = 'images/course/';
            $config['allowed_types'] = '*';
            $config['file_name'] = md5(uniqid()) . $_FILES['img'.$i]['name'];
            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
                if ($this->upload->do_upload('img'.$i))
                {
                    $uploadData = $this->upload->data();
                    $imgs[] = 'images/course/'.$uploadData['file_name'];
                }
            }else
            {
                if(!empty($_POST['eimg'.$i]))
                {
                    $imgs[]=$_POST['eimg'.$i];
                }else
                {
                    $imgs[]="";
                }
            }
        }
        for($i=1;$i<=5;$i++)
        {
            if (!empty($_FILES['pat'.$i]['name']))
            {
            $config['upload_path'] = 'images/course/';
            $config['allowed_types'] = '*';
            $config['file_name'] = md5(uniqid()) . $_FILES['pat'.$i]['name'];
            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
                if ($this->upload->do_upload('pat'.$i))
                {
                    $uploadData = $this->upload->data();
                    $pat[] = 'images/course/'.$uploadData['file_name'];
                }
            }else
            {
                if(!empty($_POST['epat'.$i]))
                {
                    $pat[] = $_POST['epat'.$i];
                }
            }
        }
        $daytime=array();
        for($i=1;$i<=6;$i++)
        {
            if (in_array($i, $_POST['day']))
            {
                $daytime[]=array('day'=>$i,'from'=>$_POST['days'.$i],'to'=>$_POST['daye'.$i]);
            }
        }
        $el=array();
        foreach($this->input->post('elig') as $row)
        {
            if(!empty($row))
            {
                $el[]=$row;
            }
        }
        $con=array();
        foreach($this->input->post('cont') as $row)
        {
            if(!empty($row))
            {
                $con[]=$row;
            }
        }
        $ob=array();
        foreach($this->input->post('obj') as $row)
        {
            if(!empty($row))
            {
                $ob[]=$row;
            }
        }
        $cdata=array(
            'start'=>$this->input->post('start'),
            'end'=>$this->input->post('end'),
            'dur'=>$this->input->post('dur'),
            'durin'=>$this->input->post('durin'),
            'daytime'=>$daytime,
            'elig'=>$el,
            'cont'=>$con,
            'obj'=>$ob,
            'meth'=>$this->input->post('meth'),
            'ben'=>$this->input->post('ben'),
            'teacher'=>$this->input->post('teacher'),
            'pat'=>$pat,
            'link'=>$this->input->post('link')
            );
        $data=array(
            'title'=>$this->input->post('title'),
            'stitle'=>$this->input->post('stitle'),
            'des'=>$this->input->post('dsc'),
            'type'=>$this->input->post('type'),
            'cat'=>$this->input->post('cat'),
            'area'=>$this->input->post('area'),
            'data'=>json_encode($cdata),
            'imgs'=>json_encode($imgs),
            'date'=>$this->input->post('ddate'),
            'code'=>$this->input->post('code'),
            'code2'=>$this->input->post('code2'),
            'price'=>$this->input->post('price')
            );
            
             if($this->AdminData->update($id,$data,'courses'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/courses/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/courses/");
            }
    }
    
        function fetch_students()
    {
                $fetch_data = $this->AdminData->make_datatables();
                $data = array();
                foreach ($fetch_data as $row) {
                    $sub_array = array();
                    $sub_array[] = $row->id;
                    $sub_array[] = $row->name;
                    $sub_array[] = $row->lname;
                    $sub_array[] = $row->email;
                    $sub_array[] = $row->mob;
                    $doc=$row->doc1*$row->doc2*$row->doc3*$row->doc4*$row->doc5*$row->doc6;
                    $txt='Entregados';
                    if($doc==0)
                    {
                        $txt='Pendiente';
                    }
                    $sub_array[] = $txt;
                    $sub_array[] = '<button type="button" class="btn btn-warning btn-xs" style="font-size:1.1rem;" onclick="conView('.$row->id.')"><span class="la las la-pencil-square-o"></span></button>';
                    $data[] = $sub_array;
                }
                $output = array(
                    "draw" => intval($_POST["draw"]),
                    "recordsTotal" => $this->AdminData->get_all_data(),
                    "recordsFiltered" => $this->AdminData->get_filtered_data(),
                    "data" => $data
                );
                echo json_encode($output);
    }
        public function updateStudent($id)
    {
        if(empty($_POST['name']) && empty($_POST['mob']) && empty($_POST['pid']) && empty($_POST['dob']) && empty($_POST['age']) && empty($_POST['nat']) && empty($_POST['addr']) && empty($_POST['email']))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/viewStudent/".$id);
        }else
        {
            $ec=$this->db->query('select * from students where email="'.$_POST['email'].'" and id not IN('.$id.')');
            if($ec->num_rows()>0)
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'El correo ya existe',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/viewStudent/".$id);
                exit();
            }
            $details=array(
                'dob'=>$this->input->post('dob'),
                'age'=>$this->input->post('age'),
                'nat'=>$this->input->post('nat'),
                'addr'=>$this->input->post('addr'));
            $data=array(
                'name'=>$this->input->post('name'),
                'lname'=>$this->input->post('lname'),
                'mob'=>$this->input->post('mob'),
                'pid'=>$this->input->post('pid'),
                'details'=>json_encode($details),
                'email'=>$this->input->post('email'));

            if($this->AdminData->update($id,$data,'students'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/viewStudent/".$id);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/viewStudent/".$id);
            }
        }
    }
    public function addEnroll($id)
    {
        if(!empty($_POST['course']))
        {
            $yr=$this->AdminData->getSetting(1);
            $cid=$_POST['course'];
            $q1=$this->db->query('select * from cart where uid="'.$id.'" and cid="'.$cid.'" and stat IN(0,1,2)');
        	if(!$q1->num_rows()>0)
        	{
        	    $q=$this->db->query('SELECT * FROM `courses` where id='.$cid);
        	    if($q->num_rows()>0)
        	    {
        	        $d=array('uid'=>$id,'cid'=>$cid,'stat'=>0,'yr'=>$yr);
        	        $this->db->insert('cart', $d);
        	        $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Agregado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                    redirect(base_url()."Admin/penroll/");
        	    }else
        	    {
        	        $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'No puedes inscribirte para esta capacitación.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                    redirect(base_url()."Admin/viewStudent/".$id);
        	    }
        	}
        	else
        	{
        	     $year=0;
        	     $stat='';
        	    foreach($q1->result() as $row)
        	    {
        	        $stat=$row->stat;
        	        $year=$row->yr;
        	    }
        	    if($year!=$yr)
        	    {
        	        if($stat==0 || $stat==2 || $stat==3)
        	        {
        	            
                	    $date='';
                	    $q=$this->db->query('SELECT * FROM `courses` where id='.$cid);
                	    if($q->num_rows()>0)
                	    {
                	        $data=array('yr'=>$yr,'stat'=>0);
                            $this->db->where('uid', $id);
                            $this->db->where('cid', $cid);
                            $this->db->update('cart', $data);
                            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Agregado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                            redirect(base_url()."Admin/penroll/");
                	    }else
                	        {
                	            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'No puedes inscribirte para esta capacitación.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                                redirect(base_url()."Admin/viewStudent/".$id);
                	        }
        	        }else
        	        {
        	            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'El entrenamiento ya está seleccionado.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                        redirect(base_url()."Admin/viewStudent/".$id);
        	        }
        	    }else
        	    {
        	        $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'El entrenamiento ya está seleccionado.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                       redirect(base_url()."Admin/viewStudent/".$id);
        	    }
        	}
        }else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'El entrenamiento ya está seleccionado.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/viewStudent/".$id);
        }
    }
    public function upDoc($id,$uid)
    {
        $c=0;
        $data=array();
        $data['doc1']=0;
        $data['doc2']=0;
        $data['doc3']=0;
        $data['doc4']=0;
        $data['doc5']=0;
        $data['doc6']=0;
        $data['stat']=0;
        if(!empty($_POST['doc']))
        {
        $doc=$_POST['doc'];
        foreach($doc as $d)
        {
            
            switch ($d) {
                case 1:
                $data['doc1']=1;    
                break;
                case 2:
                $data['doc2']=1;    
                break;
                case 3:
                $data['doc3']=1;    
                break;
                case 4:
                $data['doc4']=1;    
                break;
                case 5:
                $data['doc5']=1;    
                break;
                case 6:
                $data['doc6']=1;    
                break;
            }
        }
        $c=count($doc);
        }
        if($c==6)
        {
            $data['stat']=1;
            $this->db->query('delete from notice where uid="'.$uid.'" and type=1');
        }
        if($this->AdminData->update($id,$data,'doc'))
            {
                $this->nDoc($id);
               $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            }
            else
            {
               $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            }
        
    echo "<script>window.location.href='".base_url()."Admin/pDoc'</script>";
    }
    
    public function nDoc($id)
    {
        $q=$this->db->query('select * from doc where stat=0 and id='.$id);
        if($q->num_rows()>0)
        {
            $c=1;
            foreach($q->result() as $row)
            {
                    $txt='Por favor envíe los siguientes documentos:-';
                    if($row->doc1 == 0)
                    {
                        $txt.='\n'.$c.'. DNI.';
                        $c++;
                    }
                    if($row->doc2 == 0)
                    {
                        $txt.='\n'.$c.'. Ficha  de  Inscripción.';
                        $c++;
                    }
                    if($row->doc3 == 0)
                    {
                        $txt.='\n'.$c.'. Ficha  de  Salud.';
                        $c++;
                    }
                    if($row->doc4 == 0)
                    {
                        $txt.='\n'.$c.'. Reglamento.';
                        $c++;
                    }
                    if($row->doc5 == 0)
                    {
                        $txt.='\n'.$c.'. Contrato.';
                        $c++;
                    }
                    if($row->doc6 == 0)
                    {
                        $txt.='\n'.$c.'. Compromiso.';
                        $c++;
                    }
                    $this->db->query("UPDATE `notice` SET `cont` = '".$txt."' WHERE `notice`.`uid` = '".$row->uid."' and type=1");
            }
        }
    }
    
    public function enrollReject($id)
    {
        if(!empty($id))
        {
            $data=array(
                    'ref'=>'not paid',
                    'resultid'=>'not paid',
                    'tid'=>'not paid',
                    'txt'=>'not paid',
                    'msg'=>'not paid',
                    'crid'=>$id,
                    'price'=>'',
                    'date'=>date('Y-m-d'),
                    'stat'=>0
                    );
                    $this->db->insert('trans', $data);
                $trid= $this->db->insert_id();
                $this->db->insert('trans', $data);
            $data=array('stat'=>3,'tid'=>$trid);
            if($this->AdminData->update($id,$data,'cart'))
            {
                $data2=array(
                    'ref'=>$trid,
                    'data'=>json_encode(
                        array('Tipo de transacción'=>'Efectivo',
                        'Realizado por'=>'Personal Administrativo')
                        )
                    );
                    $this->db->insert('webhook', $data2);
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/penroll");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/viewPenroll/".$id);
            }
        }else
        {
            redirect(base_url()."Admin/penroll");
        }
    }
    
    public function enrollAccept($id)
    {
        if(!empty($id))
        {
            $d1=$this->AdminData->getPenroll($id);
            if($d1->num_rows() > 0)
            {
                $uid='';
                $coid='';
                $price='';
                foreach($d1->result() as $row)
                {
                    $price=$row->price;
                    $uid=$row->uid;
                    $coid=$row->cid;
                }
                $data=array(
                    'ref'=>'cash',
                    'resultid'=>'cash',
                    'tid'=>'Efectivo',
                    'txt'=>'Efectivo',
                    'msg'=>'Efectivo',
                    'crid'=>$id,
                    'price'=>$price,
                    'date'=>date('Y-m-d'),
                    'stat'=>200
                    );
                $this->db->insert('trans', $data);
                $trid= $this->db->insert_id();
                $data1=array('tid'=>$trid);
                $this->AdminData->update($id,$data1,'cart');
                $this->enrollForCourse($trid);
                $data2=array(
                    'ref'=>$trid,
                    'data'=>json_encode(
                        array('Tipo de transacción'=>'Efectivo',
                        'Realizado por'=>'Personal Administrativo')
                        )
                    );
                    $this->db->insert('webhook', $data2);
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/penroll");
            }else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/viewPenroll/".$id);
            }
        }else
        {
           redirect(base_url()."Admin/penroll");
        }
    }
    
        public function enrollOnAccept($id)
    {
        if(!empty($id))
        {
            $d1=$this->AdminData->getOenroll($id);
            if($d1->num_rows() > 0)
            {
                $uid='';
                $coid='';
                $price='';
                $trid='';
                foreach($d1->result() as $row)
                {
                    $price=$row->price;
                    $uid=$row->uid;
                    $coid=$row->cid;
                    $trid=$row->tid;
                }
                $data=array(
                    'txt'=>'Efectivo',
                    'msg'=>'Personal Administrativ',
                    'stat'=>200
                    );
                $this->AdminData->update($trid,$data,'trans');
                $this->enrollForCourse($trid);
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/penroll");
            }else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/viewPenroll/".$id);
            }
        }else
        {
           redirect(base_url()."Admin/penroll");
        }
    }
    private function enrollForCourse($id)
	{
	    if(!empty($id))
	    {
	        $r1=$this->AdminData->get($id,'trans');
	        if($r1->num_rows()>0)
	        {
	            $status='';
	            $cid='';
    	        foreach($r1->result() as $row)
    	        {
    	            $status=$row->stat;
    	            $cid=$row->crid;
    	        }
    	        if($status==200)
    	        {
    	            $uid='';
    	            $coid='';
    	            $r2=$this->AdminData->get($cid,'cart');
    	            foreach($r2->result() as $row)
    	            {
    	                $uid=$row->uid;
    	                $coid=$row->cid;
    	                
    	            }
    	                $year=$this->AdminData->getSetting(1);
	                    $code='';
	                    $title='';
	                    $price='';
	                    $q=$this->db->query('select * from courses where id='.$coid);
	                    foreach($q->result() as $row)
	                    {
	                        $code=$row->code;
	                        $title=$row->title;
	                        $price=$row->price;
	                    }
	                    $email='';
	                    $u=$this->db->query('select * from students where id='.$uid);
	                    foreach($u->result() as $row)
	                    {
	                        $email=$row->email;
	                    }
	                    $this->sendMail($email,'Inscripción',"Inscrito exitosamente para ".$title);
	                    $i=1;
	                    $t=0;
	                    $mq=$this->db->query('select * from enroll where code="'.$code.'" and uid="'.$uid.'"');
	                    if(!$mq->num_rows()>0)
    	               {
    	                   $this->db->insert('enroll',array('uid'=>$uid,'code'=>$code,'stat'=>0));
    	                   $this->db->where('id', $cid);
    	                   $data=array('stat'=>1);
    	                   $this->db->update('cart', $data);
    	               }
    	        }
	        }
	        
	    }
	    
	}
	public function assignBatch()
	{
	    if(!empty($_POST['id']) && !empty($_POST['yr']) && !empty($_POST['code']) && !empty($_POST['sr']) && !empty($_POST['frm']) && !empty($_POST['to']) && !empty($_POST['dt']) && !empty($_POST['croom']) && !empty($_POST['proom']) && !empty($_POST['teacher']) && !empty($_POST['data']) && !empty($_POST['link']))
	    {
	        $date1=$_POST['frm'];
    		$date2=$_POST['to'];
    		$dt=json_decode($_POST['dt']);
    		$day=array();
    		foreach($dt as $r)
    		{
    			$day[]=$r->day;
    		}
    		$date=array();
    		$d=0;
    		$atten=array();
    		while (strtotime($date1) <= strtotime($date2)) {
    			if(in_array(date('w',strtotime($date1)),$day))
    			{
    				$date[]=$date1;
    				$atten[$date1]=0;
    				$d++;
    			}
    			$date1=date('Y-m-d',strtotime("1 day",strtotime($date1)));
    		}
	        $data=array('dfrm'=>$_POST['frm'],
	        'dto'=>$_POST['to'],
	        'room'=>json_encode(array('croom'=>$_POST['croom'],'proom'=>$_POST['proom'])),
	        'stat'=>1,
	        'tid'=>$_POST['teacher'],
	        'data'=>$_POST['data'],
	        'attend'=>json_encode($atten),
	        'link'=>$_POST['link']);
	        if($this->AdminData->update($_POST['id'],$data,'batch'))
            {
                $this->addAttend(json_encode($atten),$d,$_POST['id']);
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                ?>
                <script>
                    window.location.href='<?php echo base_url()?>Admin/pbatch';
                </script>
                <?php
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/pbatchView/".$_POST['id']);
            }
	        
	        
	    }else
	    {
	        $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/pbatchView/".$_POST['id']);
	    }
	}
	
	private function addAttend($attend,$d,$id)
	{
	    $q=$this->db->query('select * from enroll where bid='.$id);
	    if($q->num_rows()>0)
	    {
	        foreach($q->result() as $row)
	        {
	            $data=array('bid'=>$id,
	                        'eid'=>$row->id,
	                        'data'=>$attend,
	                        'pday'=>0,
	                        'tday'=>$d,
	                        'total'=>0);
	           $this->db->insert('atten', $data);
	        }
	    }
	}

	function export($v1,$v2)
    {
        $this->load->library("Excel");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);

        $table_columns = array("ID","Nombre","Correo Electronico","Telefono","DNI","Grupo","Curso","Pago");

        $column = 0;

        foreach($table_columns as $field)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->AdminData->exportStudent($v1,$v2);

        $excel_row = 2;

        foreach($employee_data->result() as $row)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->name.' '.$row->lname);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->email);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->mob);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pid);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr)));
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->price);
            $excel_row++;
        }

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Employee Data.xls"');
        $object_writer->save('php://output');
    }
    
    
    	function exportB($id)
    {
        $this->load->library("Excel");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);

        $table_columns = array("ID","Nombre","Correo Electronico","Telefono","DNI","Grupo","Curso","Pago");

        $column = 0;

        foreach($table_columns as $field)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->AdminData->exportStudentB($id);

        $excel_row = 2;

        foreach($employee_data->result() as $row)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->name.' '.$row->lname);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->email);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->mob);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pid);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr)));
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->price);
            $excel_row++;
        }

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Employee Data.xls"');
        $object_writer->save('php://output');
    }
    
    public function setting($id,$value)
    {
        if($id==1)
        {
            $q=$this->db->query('select * from batch where stat=0 order by sr');
            if($q->num_rows() > 0)
            {
                foreach($q->result() as $row)
                {
                    $i=0;
                    $c=1;
                    while($i<=0)
                    {
                        $q2=$this->db->query('select * from batch where sr="'.$c.'" and yr="'.$value.'" and code="'.$row->code.'"');
                        if($q2->num_rows()>0)
                        {
                            $c++;
                        }else
                        {
                            $i=2;
                        }
                    }
                    $this->db->query('UPDATE `enroll` SET `yr` = "'.$value.'" , `sr` = "'.$c.'" WHERE `enroll`.`bid` = "'.$row->id.'"');
                    $this->db->query('UPDATE `batch` SET `yr` = "'.$value.'", `sr` = "'.$c.'" WHERE `batch`.`id` = "'.$row->id.'"');
                }
            }
            $this->db->query('UPDATE `enroll` SET `yr` = "'.$value.'"  WHERE `enroll`.`bid` = 0 and stat=0');
            
        }
        $this->AdminData->update($id,array('value'=>$value),'setting');
        redirect(base_url()."Admin/settings");
    }
    
        public function attend($id)
    {
        
        if(!empty($_POST['ids']) && !empty($_POST['dates']))
        {
            $ids=json_decode($_POST['ids']);
            $dates=json_decode($_POST['dates']);
            $data=array();
            foreach($ids as $i)
            {
                $data1=array();
                $data1['id']=$i;
                $tday=0;
                $pday=0;
                $total=0;
                $data2=array();
                foreach($dates as $d)
                {
                    $tday++;
                    if(!empty($_POST['id'.$i.'-'.$d]))
                    {
                        $data2[$d]=1;
                        $pday++;
                        
                    }else
                    {
                        $data2[$d]=0;
                    }
                }
                $data1['pday']=$pday;
                $data1['tday']=$tday;
                $data1['total']=($pday/$tday)*100;
                $data1['data']=json_encode($data2);
                $data[]=$data1;
            }
            $this->db->update_batch('atten', $data, 'id');
            redirect(base_url().'Admin/attend/'.$id, 'refresh');
        }else
        {
            redirect(base_url().'Admin/cbatch', 'refresh');
        }
    }
    
    
    public function tattend($id)
    {
        $data=array();
        $dates=json_decode($_POST['dates']);
        foreach($dates as $key=>$value)
        {
            
            if(!empty($_POST['d'.$key]))
            {
                if($_POST['d'.$key]==1)
                {
                    $data[$key]=1;
                }else
                {
                    $data[$key]=0;
                }
            }else
            {
                $data[$key]=0;
            }
        }
        $d['attend']=json_encode($data);
        $this->AdminData->update($id,$d,'batch');
        redirect(base_url()."Admin/tattend/".$id);
    }
    
    
        public function saveResult($id)
    {
        $data=array(
            'eid'=>$id,
            'title'=>$_POST['title'],
            'marks'=>$_POST['marks']
            );
            if($this->AdminData->add($data,'result'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Control deslizante agregado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/result/".$id);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/result/".$id);
            }
    }
    
        public function editResult($id,$rid)
    {
        $data=array(
            'title'=>$_POST['title'],
            'marks'=>$_POST['marks']
            );
            if($this->AdminData->update($rid,$data,'result'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Control deslizante agregado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/result/".$id);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/result/".$id);
            }
    }
    
    
        public function addUser()
    {
        if(empty($_POST['username']) && empty($_POST['password']) && empty($_POST['role']))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/user/");
        }else
        {
            $q=$this->db->query("select * from admin where user='".$_POST['username']."'");
            if($q->num_rows()==0)
            {
            $data=array(
                'user'=>$this->input->post('username'),
                'pass'=>md5($this->input->post('password')),
                'role'=>$this->input->post('role'));
             if($this->AdminData->add($data,'admin'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Agregado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/user/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/user/");
            }
        }else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'El usuario ya existe!',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/addUser/");
        }
        }
    }
    
            public function saveUser($id)
    {
        if(empty($_POST['username']) && empty($_POST['password']) && empty($_POST['role']))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Admin/user/");
        }else
        {
            $q=$this->db->query("select * from admin where user='".$_POST['username']."' and id not IN('".$id."')");
            if($q->num_rows()==0)
            {
            $data=array(
                'user'=>$this->input->post('username'),
                'pass'=>md5($this->input->post('password')),
                'role'=>$this->input->post('role'));
             if($this->AdminData->update($id,$data,'admin'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Control deslizante agregado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/user/");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/user/");
            }
        }else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'El usuario ya existe!',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Admin/editUser/".$id);
        }
        }
    }
    
    public function chart2()
    {
        $data=array();
        $this->db->select('date,SUM(price) as price');
        $this->db->from('trans');
        $this->db->where('stat', 200);
        $this->db->group_by('date');
        $this->db->order_by('date');
	    $q=$this->db->get();
	    $price=0;
        foreach($q->result() as $row)
        {
            $data[$row->date]=(int)$row->price;
        }
        $data2=array();
        foreach($data as $key=>$value)
        {
            $data2[]=array($key,$value);
        }
        echo json_encode(array("price_usd"=>$data2));
    }
    
    	private function sendMail($email,$title,$cont)
	{
	            $head='<html><head>
	            <style>
                 .banner-color {
        			 background-color: rgb(0,151,100);
        			 padding: 36px 48px;
                 }
                 h1
                 {
                     font-family: "Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;
        			font-size: 30px;
        			font-weight: 300;
        			line-height: 150%;
        			margin: 0;
        			text-align: center;
                 }
        		.banner-color h4{
        			font-family: "Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;
        			font-size: 25px;
        			font-weight: 300;
        			line-height: 150%;
        			margin: 0;
        			text-align: center;
        			color: #ffffff;
        		}
                 .title-color {
                 color: #fff;
                 }
        		 .btn{
        		 padding: 8px;
            text-decoration: none;
            color: white;
            border-radius: 20px;
            background-color: rgb(0,151,100);
        		 }
              </style>
               </head>
               <body>
                  <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
                     <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                        <tbody>
                           <tr>
                              <td align="center">
                                 <center style="width:100%">
                                    <table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif;margin-top:50px;margin-bottom:50px;" width="512">
                                       <tbody>
                                          <tr>
                                             <td width="100%" style="padding:12px;text-align:center;">
            								 <h1 style="color:rgb(0,151,100);">';
            								 $body='</h1>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td align="left">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td width="100%">
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                               <tbody>
                                                                  <tr>
                                                                     <td align="center" class="banner-color">
                                                                        <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                                           <tbody>
                                                                              <tr>
                                                                                 <td width="100%" style="text-align:center;">
                                                                                 <img style="height:50px;" src="https://fuoficios.com.ar/assest/img/logo.png">
                                                                                    <h4>Ser para Hacer</h4>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding:25px;background-color:#fff;">';
                $foot=' </td>
                                                                                              </tr>
                                                                                              <tr>
                                                                                              </tr>
                                                                                              <tr>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                         </td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td colspan="2" valign="middle" id="m_-2155919752360402777credit" style="border-radius:6px;border:0;color:#8a8a8a;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:12px;line-height:150%;text-align:center;padding:24px 0">
                                        												<p style="margin:0 0 16px">- FUO</p>
                                        											</td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                             </center>
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                              </div>
                                           </body>
                                        </html>';
	            $this->load->library('email');
                $config = array();
            	$config['protocol'] = 'smtp';
            	$config['smtp_host'] = 'fuoficios.com.ar';
            	$config['smtp_user'] = 'contacto@fuoficios.com.ar';
            	$config['smtp_pass'] = 'mocona2222';
            	$config['smtp_port'] = 25;
            	$config['charset']    = 'utf-8';
            	$config['mailtype'] = 'html';
            	$config['priority'] = 1;
            	$this->email->initialize($config);
            	$this->email->set_newline("\r\n");
            	$this->email->from('contacto@fuoficios.com.ar', 'FUO');
            	$this->email->to($email);
                $this->email->subject($title);
                $this->email->message($head.$title.$body.$cont.$foot);
                //Send mail
                $this->email->send();
	}
}
