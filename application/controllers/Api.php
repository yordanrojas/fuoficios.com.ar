<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        if(empty($this->input->post('key')))
        {
            echo $this->emsg('¡Acceso denegado!');
            exit();
        }
        if($this->input->post('key')!='bbe4a07e3dfd2fc46ed020580e2ffa97')
        {
            echo $this->emsg('¡Tecla inválida!');
            exit();
        }
        $this->load->model('AdminData');
    }
    public function index()
    {  
        
    }
    
    public function login()
    {
        if(!empty($this->input->post('email')) && !empty($this->input->post('pass')))
        {
            $q=$this->AdminData->getf('email',$this->input->post('email'),'students');
            if($q->num_rows() > 0)
            {
                $pass='';
                foreach($q->result() as $row)
                {
                    $pass=$row->pass;
                }
                if($pass==md5($this->input->post('pass')))
                {
                    echo $this->smsg('Inicio de sesión correcto.',$q->result());
                    
                }else
                {
                    echo $this->emsg('¡Contraseña invalida!');
                }
            }else
            {
                echo $this->emsg('¡Email inválido!');
            }
        }
        else
        {
            echo $this->emsg('Faltan campos!');
        }
    }
    
    public function forgot()
	{
	    
	    if(!empty($this->input->post('email')))
	    {
	        $q=$this->db->query("select * from students where email='".$this->input->post('email')."'");
	        if($q->num_rows()>0)
	        {
	            $pass='';
	            foreach($q->result() as $row)
	            {
	                $pass=$row->pass;
	            }
	            
	            $this->sendMail($this->input->post('email'),'Olvidé mi contraseña Enlace','<a href="'.base_url().'Home/changePassword/'.$this->input->post('email').'"/"'.$pass.'">Change</a>');
            	echo $this->smsg('El enlace de restablecimiento de contraseña se envía a su correo.',array());
	        }
	        else
	        {
	            echo $this->emsg('¡Email inválido!');
	        }
	    }else
	    {
	        echo $this->emsg('Faltan campos!');
	    }
	}
	
	public function courses()
	{
	    $q=$this->db->query('select * from courses where hide=0');
	    $data=array();
	    if($q->num_rows()>0)
	    {
	        foreach($q->result() as $row)
	            {
	                $data2=array();
	                $data2['title']=$row->title;
	                $data2['des']=$row->des;
	                $img=json_decode($row->imgs);
	                $data2['img']=base_url().$img[0];
	                $data2['link']=base_url().'Home/course/'.$row->id;
	                $data[]=$data2;
	            }
	            echo $this->smsg('Exitoso',$data);
	    }
	    else
	    {
	        echo $this->emsg('¡datos no encontrados!');
	    }
	    
	}
	
		public function certificate()
	{
	    if(!empty($_POST['id']))
	    {
    	    $q=$this->db->query('select * from certi where uid="'.$_POST['id'].'"');
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	            {
    	                $data2=array();
    	                $data2['id']=$row->id;
    	                $data2['title']=$row->title;
    	                $data[]=$data2;
    	            }
    	            echo $this->smsg('Exitoso',$data);
    	    }
    	    else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }
    	    else
    	    {
    	        echo $this->emsg('Faltan campos!');
    	    }
	}
	
	public function courseType($c=0)
	{
	    if(!empty($_POST['type']))
	    {
    	    $q=$this->db->query('select * from courses where cat="'.$_POST['type'].'" LIMIT '.$c.',20;');
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	            {
    	                $data2=array();
    	                $data2['title']=$row->title;
    	                $data2['des']=$row->des;
    	                $img=json_decode($row->imgs);
    	                $data2['img']=base_url().$img[0];
    	                $data2['link']=base_url().'Home/course/'.$row->id;
    	                $data[]=$data2;
    	            }
    	            echo $this->smsg('Exitoso',$data);
    	    }
    	    else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }else
	    {
	        echo $this->emsg('Faltan campos!');
	        
	    }
	    
	}
	public function myCourses()
	{
	  $q=$this->AdminData->getEnCourse($_POST['id'],$this->AdminData->getSetting(1));
	  $q2=$this->AdminData->getpEnCourse($_POST['id'],$this->AdminData->getSetting(1));
	  $q3=$this->AdminData->getppEnCourse($_POST['id']);
	  $data=array();
	  if($q->num_rows()>0 || $q2->num_rows()>0 || $q3->num_rows()>0)
	    {
	        foreach($q3->result() as $row)
	            {
	                $data2=array();
	                $data2['enid']=$row->enid;
	                $data2['cid']=$row->coid;
	                $data2['link']='';
	                $data2['bid']='';
	                $data2['from']='';
	                $data2['to']='';
	                $data2['stat']=0;
	                $data2['title']=$row->title;
	                $data2['tid']='';
	                $data2['tname']='';
	                $data2['feed']=$row->feed;
	                $data2['timg']='';
	                $img=json_decode($row->imgs);
	                $data2['img']=base_url().$img[0];
	                $data[]=$data2;
	            }
	        foreach($q2->result() as $row)
	            {
	                $data2=array();
	                $data2['enid']=$row->enid;
	                $data2['cid']=$row->coid;
	                $data2['link']='';
	                $data2['bid']=$row->bid;
	                $data2['from']='';
	                $data2['to']='';
	                $data2['stat']=$row->stat;
	                $data2['title']=$row->title;
	                $data2['tid']='';
	                $data2['tname']='';
	                $data2['feed']=$row->feed;
	                $data2['timg']='';
	                $img=json_decode($row->imgs);
	                $data2['img']=base_url().$img[0];
	                $data[]=$data2;
	            }
	        foreach($q->result() as $row)
	            {
	                $data2=array();
	                $data2['enid']=$row->enid;
	                $data2['cid']=$row->coid;
	                $data2['link']=$row->link;
	                $data2['bid']=$row->bid;
	                $data2['from']=$row->dfrm;
	                $data2['to']=$row->dto;
	                $data2['stat']=$row->stat;
	                $data2['title']=$row->title;
	                $data2['tid']=$row->tid;
	                $data2['tname']=$row->tname;
	                $data2['feed']=$row->feed;
	                $data2['timg']=base_url().$row->img;
	                $img=json_decode($row->imgs);
	                $data2['img']=base_url().$img[0];
	                $data[]=$data2;
	            }
	            
	            echo $this->smsg('Exitoso',$data);
	    }
	    else
	    {
	        echo $this->emsg('¡datos no encontrados!');
	    }
	}
	public function contacts()
	{
	    $q=$this->db->query('select * from pages where id IN(1,3)');
	    $data=array();
	    if($q->num_rows()>0)
	    {
	        foreach($q->result() as $row)
	            {
	                if($row->id==1)
	                {
	                    $data['soc_link']=json_decode($row->data5);
	                }else
	                {
	                    $data['ph']=json_decode($row->data1);
	                    $data['nav']=json_decode($row->data2);
	                    $data['mail']=json_decode($row->data3);
	                }
	            }
	            echo $this->smsg('Exitoso',$data);
	    }
	    else
	    {
	        echo $this->emsg('¡datos no encontrados!');
	    }
	}
	
	public function profesor()
	{
	    if(!empty($_POST['id']))
	    {
	        $q=$this->db->query('select * from teachers where id='.$_POST['id']);
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	        {
    	            $data['id']=$row->id;
    	            $data['name']=$row->name;
    	            $data['bio']=$row->bio;
    	            $data['img']=base_url().$row->img;
    	        }
    	        echo $this->smsg('Exitoso',$data);
    	    }else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }
	    else
	    {
	        echo $this->emsg('Faltan campos!');
	        
	    }
	}
	public function syllabus()
	{
	    if(!empty($_POST['id']))
	    {
	        $q=$this->AdminData->getDeCourse($_POST['id']);
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	        {
    	            $data['title']=$row->title;
    	            $data['data']=json_decode($row->data);
    	            $img=json_decode($row->imgs);
	                $data['img']=base_url().$img[0];
    	        }
    	        echo $this->smsg('Exitoso',$data);
    	    }else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }
	    else
	    {
	        echo $this->emsg('Faltan campos!');
	        
	    }
	}
	
	public function doc()
	{
	    if(!empty($_POST['id']))
	    {
	        $q=$this->AdminData->getf('uid',$_POST['id'],'doc');
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	        {
    	            $data['1']=$row->doc1;
    	            $data['2']=$row->doc2;
    	            $data['3']=$row->doc3;
    	            $data['4']=$row->doc4;
    	            $data['5']=$row->doc5;
    	            $data['6']=$row->doc6;
    	            
    	        }
    	        echo $this->smsg('Exitoso',$data);
    	    }else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }
	    else
	    {
	        echo $this->emsg('Faltan campos!');
	        
	    }
	}
	
	public function result()
	{
	    if(!empty($_POST['eid']))
	    {
	        $q=$this->db->query('select * from result where eid="'.$_POST['eid'].'" ');
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	        {
    	            $data[]=$row;
    	            
    	        }
    	        echo $this->smsg('Exitoso',$data);
    	    }else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }
	    else
	    {
	        echo $this->emsg('Faltan campos!');
	        
	    }  
	}
	
		public function events()
	{
	        $q=$this->db->query('select * from news where type=2 ');
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	        {
    	            $data1['title']=$row->title;
    	            $data1['des']=$row->des;
    	            $data1['img']=base_url().$row->img;
    	            $data[]=$data1;
    	        }
    	        echo $this->smsg('Exitoso',$data);
    	    }else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	}
	
	
	
		public function payment()
	{
	    if(!empty($_POST['cid']) && !empty($_POST['id']))
	    {
	        $this->db->select('trans.date,trans.price,trans.tid,trans.stat');
            $this->db->from('cart');
            $this->db->join('trans', 'trans.id = cart.tid');
            $this->db->where('cart.uid',$_POST['id']);
            $this->db->where('cart.cid',$_POST['cid']);
            $q = $this->db->get();
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	        {
    	            $data['date']=$row->date;
    	            $data['price']=$row->price;
    	            $data['stat']=$row->stat;
    	            if($row->tid=='Cash')
    	            {
    	                $data['type']='Cash';
    	            }else
    	            {
    	                $data['type']='Mobbex';
    	            }
    	            
    	        }
    	        echo $this->smsg('Exitoso',$data);
    	    }else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }
	    else
	    {
	        echo $this->emsg('Faltan campos!');
	        
	    }
	}
		public function attendance()
	{
	    if(!empty($_POST['bid']) && !empty($_POST['eid']))
	    {
	        $q=$this->db->query('select * from atten where bid="'.$_POST['bid'].'" and eid="'.$_POST['eid'].'"');
    	    $data=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	        {
    	            $data['data']=json_decode($row->data);
    	            $data['per']=$row->total;
    	            
    	        }
    	        echo $this->smsg('Exitoso',$data);
    	    }else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }
	    else
	    {
	        echo $this->emsg('Faltan campos!');
	        
	    }
	}
		public function notice()
	{
	    if(!empty($_POST['id']))
	    {
	        $q=$this->db->query('select * from notice where who="1" or uid="'.$_POST['id'].'" order by id desc');
    	    $d=array();
    	    if($q->num_rows()>0)
    	    {
    	        foreach($q->result() as $row)
    	        {
    	            $data=array();
    	            $data['sub']=$row->sub;
    	            $data['cont']=$row->cont;
    	            $data['img']=base_url().$row->img;
    	            $d[]=$data;
    	        }
    	        echo $this->smsg('Exitoso',$d);
    	    }else
    	    {
    	        echo $this->emsg('¡datos no encontrados!');
    	    }
	    }
	    else
	    {
	        echo $this->emsg('Faltan campos!');
	        
	    }
	}
	
		private function sendMail($email,$title,$cont)
	{
	            $head='<html><head>
	            <style>
                 .banner-color {
        			 background-color: rgb(0,151,100);
        			 padding: 36px 48px;
                 }
        		.banner-color h1{
        			font-family: "Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;
        			font-size: 30px;
        			font-weight: 300;
        			line-height: 150%;
        			margin: 0;
        			text-align: left;
        			color: #ffffff;
        		}
                 .title-color {
                 color: #fff;
                 }
        		 .btn{
        		 padding: 8px;
            text-decoration: none;
            color: white;
            border-radius: 20px;
            background-color: rgb(0,151,100);
        		 }
              </style>
               </head>
               <body>
                  <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
                     <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                        <tbody>
                           <tr>
                              <td align="center">
                                 <center style="width:100%">
                                    <table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif;margin-top:50px;margin-bottom:50px;" width="512">
                                       <tbody>
                                          <tr>
                                             <td width="100%" style="padding:12px;text-align:center;">
            								 <img style="height:100px;" src="https://fuoficios.com.ar/assest/img/mlogo.png">
                                             </td>
                                          </tr>
                                          <tr>
                                             <td align="left">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td width="100%">
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                               <tbody>
                                                                  <tr>
                                                                     <td align="center" class="banner-color">
                                                                        <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                                           <tbody>
                                                                              <tr>
                                                                                 <td width="100%">
                                                                                    <h1>';
                $body='</h1>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding:25px;background-color:#fff;">';
                $foot=' </td>
                                                                                              </tr>
                                                                                              <tr>
                                                                                              </tr>
                                                                                              <tr>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                         </td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td colspan="2" valign="middle" id="m_-2155919752360402777credit" style="border-radius:6px;border:0;color:#8a8a8a;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:12px;line-height:150%;text-align:center;padding:24px 0">
                                        												<p style="margin:0 0 16px">- FUO</p>
                                        											</td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                             </center>
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                              </div>
                                           </body>
                                        </html>';
	            $this->load->library('email');
                $config = array();
            	$config['protocol'] = 'smtp';
            	$config['smtp_host'] = 'fuoficios.com.ar';
            	$config['smtp_user'] = 'contacto@fuoficios.com.ar';
            	$config['smtp_pass'] = 'mocona2222';
            	$config['smtp_port'] = 25;
            	$config['charset']    = 'utf-8';
            	$config['mailtype'] = 'html';
            	$config['priority'] = 1;
            	$this->email->initialize($config);
            	$this->email->set_newline("\r\n");
            	$this->email->from('contacto@fuoficios.com.ar', 'FUO');
            	$this->email->to($email);
            	$this->email->subject($title);
            	$this->email->message($head.$title.$body.$cont.$foot);
            	//Send mail
            	$this->email->send();
	}
	
    public function emsg($msg)
    {
        return json_encode(array('status'=>0,'msg'=>$msg));
    }
    
    public function smsg($msg,$data)
    {
        return json_encode(array('status'=>1,'msg'=>$msg,'data'=>$data));
    }
    
}