<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminData');
        $this->load->model('TeacherData');
    }

	public function index($e=0)
	{
	    if(!empty($this->session->adminuser) && !empty($this->session->adminpass))
        {
            session_destroy();
            redirect(base_url().'Admin', 'refresh');
        }
        else
        {
		    $this->load->view('admin/login',array('e'=>$e));
        }
	}

	public function validate()
	{
	    if(!empty($this->input->post('username')) && !empty($this->input->post('userpass')))
        {
            $data=$this->AdminData->valid($this->input->post('username'),md5($this->input->post('userpass')));
            if($data->num_rows()==1)
            {
                $user='';
                $pass='';
                $img='';
                $role='';
                foreach($data->result() as $row)
                {
                    $user=$row->user;
                    $pass=$row->pass;
                    $img=$row->img;
                    $role=$row->role;
                }
                if($user!=$this->input->post('username') && $pass!=md5($this->input->post('userpass')))
                {
                    session_destroy();
                    redirect(base_url().'Auth/index/1', 'refresh');
                }else
                {
                    $this->session->set_userdata('adminuser', $this->input->post('username'));
                    $this->session->set_userdata('adminpass', md5($this->input->post('userpass')));
                    $this->session->set_userdata('adminimg', $img);
                    $this->session->set_userdata('adminrole', $role);
                    redirect(base_url().'Admin', 'refresh');
                }
                
            }else
            {
                session_destroy();
                redirect(base_url().'Auth/index/1', 'refresh'); 
            }
        }else
        {
            session_destroy();
            redirect(base_url().'Auth', 'refresh'); 
        }
	}
	
		public function teacher($e=0)
	{
	    if(!empty($this->session->tuser) && !empty($this->session->tpass))
        {
            session_destroy();
            redirect(base_url().'Teacher', 'refresh');
        }else
        {
		    $this->load->view('teacher/login',array('e'=>$e));
        }
	}
	
	public function validateTeacher()
	{
	    if(!empty($this->input->post('username')) && !empty($this->input->post('userpass')))
        {
            $data=$this->TeacherData->valid($this->input->post('username'),md5($this->input->post('userpass')));
            if($data->num_rows()==1)
            {
                $user='';
                $pass='';
                $img='';
                foreach($data->result() as $row)
                {
                    $user=$row->email;
                    $pass=$row->pass;
                    $img=$row->img;
                }
                if($user!=$this->input->post('username') && $pass!=md5($this->input->post('userpass')))
                {
                    session_destroy();
                    redirect(base_url().'Auth/teacher/1', 'refresh');
                }else
                {
                    $this->session->set_userdata('tuser', $this->input->post('username'));
                    $this->session->set_userdata('tpass', md5($this->input->post('userpass')));
                    $this->session->set_userdata('timg', $img);
                    redirect(base_url().'Teacher', 'refresh');
                }
                
            }else
            {
                session_destroy();
                redirect(base_url().'Auth/teacher/1', 'refresh'); 
            }
        }else
        {
            session_destroy();
            redirect(base_url().'Auth/teacher', 'refresh'); 
        }
	}
}