<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminData');
    }

	public function index()
	{

	}
	
	
	
	public function checkAttend()
	{
	    $yr=$this->AdminData->getSetting(1);
	    $this->db->select('enroll.id,batch.title,batch.coid,enroll.uid');
        $this->db->from('enroll');
        $this->db->join('batch', 'batch.id = enroll.bid');
        $this->db->join('atten', 'atten.eid = enroll.id');
        $this->db->where('enroll.cer', 0);
        $this->db->where('enroll.yr', $yr);
        $this->db->where('atten.total >=79.5');
        $query = $this->db->get();
        foreach($query->result() as $row)
        {
            $data=array('uid'=>$row->uid,
                        'title'=>$row->title,
                        'coid'=>$row->coid,
                        'eid'=>$row->id,
                        'date'=>date('Y-m-d'));
            if($this->db->insert('certi', $data))
            {
                $this->db->query("UPDATE `enroll` SET `cer` = '1' WHERE `enroll`.`id` =".$row->id);
            }
        }
	}
	
	public function checkFeed()
	{
	    $yr=$this->AdminData->getSetting(1);
	    $q=$this->db->query('select * from batch where feed=0 and stat=1 and yr='.$yr);
	    foreach($q->result() as $row)
	    {
	        $date1=date_create(date('Y-m-d'));
            $date2=date_create($row->dto);
            $diff=date_diff($date1,$date2);
            $v=$diff->format("%R%a");
            if($v<6)
            {
                
                $this->db->query("UPDATE `batch` SET `feed` = '1' WHERE `batch`.`id` =".$row->id);
                $this->db->query("UPDATE `enroll` SET `feed` = '1' WHERE `bid` =".$row->id);
            }
	    }
	}
	

	
	
	
	
}