<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('UserData');
        if(!empty($this->session->userdata("fuouser")) && !empty($this->session->userdata("fuopass")))
        {
            $q=$this->db->query("select* from students where email='".$this->session->userdata("fuouser")."'");
            if($q->num_rows()>0)
            {
                $pass='';
                foreach($q->result() as $row)
                {
                    $pass=$row->pass;
                }
                if($pass!=$this->session->userdata("fuopass"))
                {
                    $this->session->set_userdata("fuouser","");
                    $this->session->set_userdata("fuopass","");
                }
            }else
            {
                $this->session->set_userdata("fuouser","");
                $this->session->set_userdata("fuopass","");
            }
        }
        else
        {
            $this->session->set_userdata("fuouser","");
            $this->session->set_userdata("fuopass","");
        }
    }
    
    public function test()
    {
        $this->load->view('test');
    }
	public function index()
	{
	    $data['tslider']=$this->UserData->slider('mainslider');
	    $data['bslider']=$this->UserData->slider('proslider');
	    $data['data']=$this->UserData->get(1,'pages');
	    $data['courses']=$this->UserData->selectCourse();
	    $data['form']=$this->UserData->select('courses');
	    $foot['data']=$this->UserData->get(1,'pages');
	    $nav['nav']=1;
		$this->load->view('header',$nav);
		$this->load->view('main',$data);
		$this->load->view('footer',$foot);
	}
	
	public function contact()
	{
	    $nav['nav']=6;
	    $foot['data']=$this->UserData->get(1,'pages');
	    $data['form']=$this->UserData->select('courses');
	    $data['data']=$this->UserData->get(3,'pages');
	    $this->load->view('header',$nav);
		$this->load->view('contact',$data);
		$this->load->view('footer',$foot);
	}
	
		public function info()
	{
	    $nav['nav']=2;
	    $foot['data']=$this->UserData->get(1,'pages');
	    $data['news']=$this->db->query('select * from news order by id desc');
	    $this->load->view('header',$nav);
		$this->load->view('info',$data);
		$this->load->view('footer',$foot);
	}
	
	public function viewInfo($id)
	{
	    $nav['nav']=2;
	    $foot['data']=$this->UserData->get(1,'pages');
	    $data['news']=$this->UserData->get($id,'news');
	    $this->load->view('header',$nav);
		$this->load->view('viewInfo',$data);
		$this->load->view('footer',$foot);
	}
	
	public function institution()
	{
	    $nav['nav']=2;
	    $foot['data']=$this->UserData->get(1,'pages');
	    $data['data1']=$this->UserData->get(1,'pages');
	    $data['data2']=$this->UserData->get(2,'pages');
	    $this->load->view('header',$nav);
		$this->load->view('institution',$data);
		$this->load->view('footer',$foot);
	}
	
	public function courses()
	{
	    $this->load->library("pagination");
	    $config = array();
	    if ($this->input->get('area')) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config["base_url"] = base_url() . "Home/courses/";
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $config["total_rows"] = $this->UserData->get_count($this->input->get('area'));
        $config["per_page"] = 6;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
    
    
    
        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
    
    
        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["links"] = $this->pagination->create_links();

        $data['courses'] = $this->UserData->get_courses($this->input->get('area'),$config["per_page"], $page);
	    $nav['nav']=3;
	    $foot['data']=$this->UserData->get(1,'pages');
	    $this->load->view('header',$nav);
		$this->load->view('courses',$data);
		$this->load->view('footer',$foot);
	}
	
	public function course($id='')
	{
	    if(!empty($id))
	    {
	        $foot['data']=$this->UserData->get(1,'pages');
    	    $nav['nav']=3;
    	    $data['data']=$this->UserData->get($id,'courses');
    	    $this->load->view('header',$nav);
    		$this->load->view('course',$data);
    		$this->load->view('footer',$foot);
	    }else
	    {
	        redirect(base_url().'Home/courses');
	    }
	}
	
	public function user($id=0,$cid=0)
	{
	    if($id!=0)
	    {
	        $this->session->set_userdata("fuouser","");
            $this->session->set_userdata("fuopass","");
            session_destroy();
	    }
	    if($cid!=0)
	    {
	        $this->session->set_userdata("cart",$cid);
	    }
	    if(!empty($this->session->userdata("fuouser")) && !empty($this->session->userdata("fuopass")))
        {
            header(base_url().'Home/myaccount'); 
            exit(); 
        }elseif(!empty($this->session->userdata("tuser")) && !empty($this->session->userdata("tpass")))
        {
            header( "Location: ".base_url()."Teacher" );
            exit(); 
        }else
        {
            $nav['nav']=5;
    	    $foot['data']=$this->UserData->get(1,'pages');
    	    $this->load->view('header',$nav);
    		$this->load->view('user');
    		$this->load->view('footer',$foot);
        }
	}
	
	
	public function login()
	{
	    if(!empty($this->session->userdata("fuouser")) && !empty($this->session->userdata("fuopass")))
        {
            header(base_url().'Home/myaccount'); 
            exit(); 
        }elseif(!empty($this->session->userdata("tuser")) && !empty($this->session->userdata("tpass")))
        {
            header( "Location: ".base_url()."Teacher" );
            exit(); 
        }else
        {
            $nav['nav']=5;
    	    $foot['data']=$this->UserData->get(1,'pages');
    	    $this->load->view('header',$nav);
    		$this->load->view('login');
    		$this->load->view('footer',$foot);
        }
	}
	
	public function register()
	{
	    
	    if(!empty($this->session->userdata("fuouser")) && !empty($this->session->userdata("fuopass")))
        {
            header(base_url().'Home/myaccount'); 
            exit(); 
        }elseif(!empty($this->session->userdata("tuser")) && !empty($this->session->userdata("tpass")))
        {
            header( "Location: ".base_url()."Teacher" );
            exit(); 
        }else
        {
	    $nav['nav']=5;
	    $foot['data']=$this->UserData->get(1,'pages');
	    $data['form']=$this->UserData->select('courses');
	    $this->load->view('header',$nav);
		$this->load->view('register',$data);
		$this->load->view('footer',$foot);
        }
	}
	
	public function registeradmin()
	{
	    $nav['nav']=5;
	    $foot['data']=$this->UserData->get(1,'pages');
	    $data['form']=$this->UserData->select('courses');
	    $this->load->view('header',$nav);
		$this->load->view('register2',$data);
		$this->load->view('footer',$foot);
	}
	
	public function saveForm()
	{
	   $data=array('name'=>$this->input->post('name'),
        	        'mob'=>$this->input->post('mob'),
        	        'email'=>$this->input->post('email'),
        	        'course'=>$this->input->post('course'),
        	        'refer'=>$this->input->post('refer'),
        	        'msg'=>$this->input->post('msg'));
        	        $d=json_decode($this->input->post('course'));
        	        $msg='Nombre:-'.$this->input->post('name');
        	        $msg=$msg.'<br>Correo:-'.$this->input->post('email');
        	        $msg=$msg.'<br>Teléfono:-'.$this->input->post('mob');
        	        $msg=$msg.'<br>Curso:-'.$d->title;
        	        $msg=$msg.'<br>Cómo nos conociste?:-'.$this->input->post('refer');
        	        $msg=$msg.'<br>Mensaje:-'.$this->input->post('msg');
        $this->load->library('email');
        $config = array();
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'fuoficios.com.ar';
        $config['smtp_user'] = 'web@fuoficios.com.ar';
        $config['smtp_pass'] = 'mocona123';
        $config['smtp_port'] = 25;
        $config['charset']    = 'utf-8';
        $config['mailtype'] = 'html';
        $config['priority'] = 1;
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('web@fuoficios.com.ar', 'FUO');
        $this->email->to('contacto@fuoficios.com.ar');
        $this->email->subject('Consulta web');
        $this->email->message($msg);
        //Send mail
        $this->email->send();
        
	   if($this->UserData->add($data,'contact'))
        {
            echo 1;
        }else
        {
            echo 0;
        }
	}
	
	public function registerStudent()
	{
	    $details=array('dob'=>$this->input->post('dob'),
	                    'age'=>$this->input->post('age'),
	                    'pid'=>$this->input->post('pid'),
	                    'nat'=>$this->input->post('nat'),
	                    'addr'=>$this->input->post('addr'));
	    $other=array('refer'=>$this->input->post('refer'),
	                    'empstat'=>$this->input->post('empstat'),
	                    'emp'=>$this->input->post('emp'),
	                    'edu'=>$this->input->post('edu'));
        $data=array('name'=>$this->input->post('first'),
                        'lname'=>$this->input->post('last'),
            	        'mob'=>$this->input->post('mob'),
            	        'email'=>$this->input->post('email'),
            	        'pid'=>$this->input->post('pid'),
            	        'details'=>json_encode($details),
            	        'other'=>json_encode($other),
            	        'pass'=>md5($this->input->post('pass')));
        if($this->db->insert('students',$data))
        {
            $uid=$this->db->insert_id();
            $data=array('uid'=>$uid);
            $this->db->insert('doc',$data);
            $txt='Por favor envíe los siguientes documentos:-\n 1. DNI. \n 2. Ficha  de  Inscripción. \n 3. Ficha  de  Salud. \n 4. Reglamento. \n 5. Contrato. \n 6. Compromiso.';
            $data2=array('sub'=>'Envío de documentos.',
                                'cont'=>$txt,
                                'img'=>'images/doc.png',
                                'who'=>2,
                                'uid'=>$uid,
                                'type'=>1);
            $this->db->insert('notice', $data2);   
            $this->session->set_userdata("smsg", "<p class='text-success'>Registrado correctamente.</p>");
            $this->session->set_userdata("fuouser",$this->input->post('email'));
	       $this->session->set_userdata("fuopass",md5($this->input->post('pass')));
	       $this->sendMail($this->input->post('email'),'Registro exitoso','Gracias por registrarse, inscríbase en los cursos y descargue nuestra aplicación para obtener más detalles sobre el curso');
            redirect(base_url()."Home/myaccount/");
        }
        else
        {
            $this->session->set_userdata("emsg", "<p class='text-danger'>¿Se produjo un error? Inténtalo de nuevo.</p>");
            redirect(base_url()."Home/register/");
        }
	}
	
		public function registerStudent2()
	{
	    $details=array('dob'=>$this->input->post('dob'),
	                    'age'=>$this->input->post('age'),
	                    'pid'=>$this->input->post('pid'),
	                    'nat'=>$this->input->post('nat'),
	                    'addr'=>$this->input->post('addr'));
	    $other=array('refer'=>$this->input->post('refer'),
	                    'empstat'=>$this->input->post('empstat'),
	                    'emp'=>$this->input->post('emp'),
	                    'edu'=>$this->input->post('edu'));
        $data=array('name'=>$this->input->post('first'),
                        'lname'=>$this->input->post('last'),
            	        'mob'=>$this->input->post('mob'),
            	        'email'=>$this->input->post('email'),
            	        'pid'=>$this->input->post('pid'),
            	        'details'=>json_encode($details),
            	        'other'=>json_encode($other),
            	        'pass'=>md5($this->input->post('pass')));
        if($this->db->insert('students',$data))
        {
            $uid=$this->db->insert_id();
            $data=array('uid'=>$uid);
            $this->db->insert('doc',$data);
            $txt='Por favor envíe los siguientes documentos:-\n 1. DNI. \n 2. Ficha  de  Inscripción. \n 3. Ficha  de  Salud. \n 4. Reglamento. \n 5. Contrato. \n 6. Compromiso.';
            $data2=array('sub'=>'Envío de documentos.',
                                'cont'=>$txt,
                                'img'=>'images/doc.png',
                                'who'=>2,
                                'uid'=>$uid,
                                'type'=>1);
            $this->db->insert('notice', $data2);
            $this->session->set_userdata("smsg", "<p class='text-success'>Registrado correctamente.</p>");
	       $this->sendMail($this->input->post('email'),'Registro Exitoso','Ingrese a ya registrado y complete el pago de curso seleccionado');
            redirect(base_url()."Home/register/");
        }
        else
        {
            $this->session->set_userdata("emsg", "<p class='text-danger'>¿Se produjo un error? Inténtalo de nuevo.</p>");
            redirect(base_url()."Home/register/");
        }
	}
	
	public function sendOtp($c)
	{
	    if($c==0)
	    {
	    if(!empty($_POST['email']))
	    {
	    $q=$this->db->query("select * from students where email='".$_POST['email']."'");
	    if($q->num_rows()>0)
	    {
	        echo json_encode(array('status'=>0,'data'=>'el Email ya existe!'));
	    }
	    else
	    {
	    $this->load->library('email');
	    $num=rand(0000,9999);
        $this->sendMail($this->input->post('email'),'Verificación de correo','Para la verificación de correo, el pin de validación es: '.$num);
    	echo json_encode(array('status'=>1,'data'=>$num));
	    }
	    }else
	    {
	        echo json_encode(array('status'=>0,'data'=>'falta el correo electrónico'));
	    }
	    }else
	    {
	        $q=$this->db->query("select * from students where pid='".$_POST['pid']."'");
	    if($q->num_rows()>0)
	    {
	        echo "Esta identificación ya existe";
	    }else
	    {
	        echo 1;
	    }
	    
	    }
	}
	
	public function loginData()
	{
	    if(!empty($_POST['email']) && !empty($_POST['pass']))
	    {
	        $this->load->model('TeacherData');
	        $q=$this->db->query("select * from students where email='".$_POST['email']."' AND pass='".md5($_POST['pass'])."'");
	        $data=$this->TeacherData->valid($this->input->post('email'),md5($this->input->post('pass')));
	        if($q->num_rows()>0)
	        {
	            $pass='';
	            foreach($q->result() as $row)
	            {
	                $pass=$row->pass;
	            }
	            if($pass==md5($_POST['pass']))
	            {
	                $this->session->set_userdata("fuouser", $_POST['email']);
	                $this->session->set_userdata("fuopass", md5($_POST['pass']));
	                echo json_encode(array('status'=>1,'data'=>'Inicia sesión correctamente'));
	            }else
	            {
	                echo json_encode(array('status'=>0,'data'=>'Contraseña inválidos!'));
	            }
	        }elseif($data->num_rows()==1)
            {
                $user='';
                $pass='';
                $img='';
                foreach($data->result() as $row)
                {
                    $user=$row->email;
                    $pass=$row->pass;
                    $img=$row->img;
                }
                if($user!=$this->input->post('email') && $pass!=md5($this->input->post('pass')))
                {
                    echo json_encode(array('status'=>0,'data'=>'Contraseña inválidos!'));
                }else
                {
                    $this->session->set_userdata('tuser', $this->input->post('email'));
                    $this->session->set_userdata('tpass', md5($this->input->post('pass')));
                    $this->session->set_userdata('timg', $img);
                    echo json_encode(array('status'=>2,'data'=>'Inicia sesión correctamente'));
                }
                
            }
	        else
	        {
	            echo json_encode(array('status'=>0,'data'=>'Correo electrónico y contraseña inválidos!'));
	        }
	    }else
	    {
	        echo json_encode(array('status'=>0,'data'=>'faltan campos'));
	    }
	}
	public function forgotData()
	{
	    if(!empty($_POST['email']))
	    {
	        $q=$this->db->query("select * from students where email='".$_POST['email']."'");
	        if($q->num_rows()>0)
	        {
	            $pass='';
	            foreach($q->result() as $row)
	            {
	                $pass=$row->pass;
	            }
            	$this->sendMail($this->input->post('email'),'Restablecer la contraseña',"Haga clic aquí: <a href='".base_url()."Home/changePassword/".$_POST['email']."/".$pass."'>cambio</a>");
            	echo json_encode(array('status'=>1,'data'=>'Restablecer enlace se envía en su correo electrónico.'));
	        }
	        else
	        {
	            echo json_encode(array('status'=>0,'data'=>'Correo electrónico inválidos!'));
	        }
	    }else
	    {
	        echo json_encode(array('status'=>0,'data'=>'faltan campos'));
	    }
	}
	public function changePassword($email='',$key='')
	{
	    $data=array();
	    if(!empty($email) && !empty($key))
	    {
	        $q=$this->db->query("select * from students where email='".$email."' and pass='".$key."'");
	        if($q->num_rows()>0)
	        {
	            $data['stat']=0;
	            $data['data']=$email;
	        }
	        else
	        {
	            $data['stat']=1;
	            $data['data']='Usuario invalido';
	        }
	    }
	    else
	    {
	        $data['stat']=1;
	        $data['data']='¡Acceso denegado!';
	    }
	    $foot['data']=$this->UserData->get(1,'pages');
    	$nav['nav']=0;
    	$this->load->view('header',$nav);
    	$this->load->view('forgot',$data);
    	$this->load->view('footer',$foot);
	}
	public function updatePass()
	{
	    if(!empty($_POST['email']) && !empty($_POST['pass']))
	    {
	        if($this->db->query("UPDATE `students` SET `pass` = '".md5($_POST['pass'])."' WHERE `students`.`email` = '".$_POST['email']."'"))
	        {
	           echo 1; 
	        }else
	        {
	            echo 0;
	        }
	    }
	    else
	    {
	        echo 0;
	    }
	}
	public function myaccount($cid=0)
	{
	    if(!empty($_GET['remove']))
	    {
	        $this->UserData->delete($_GET['remove'],'cart');
	    }
	    if(!empty($this->session->userdata("fuouser")) && !empty($this->session->userdata("fuopass")))
        {
	    $foot['data']=$this->UserData->get(1,'pages');
    	$nav['nav']=5;
    	$data=array();
    	$r=$this->UserData->getf('email',$this->session->userdata("fuouser"),'students');
    	$id=0;
    	$yr=$this->UserData->getSetting(1);
    	foreach($r->result() as $row)
    	{
    	    $id=$row->id;
    	}
    	if(!empty($this->session->userdata("cart")))
        {
            $cid=$this->session->userdata("cart");
            $this->session->set_userdata("cart","");
        }
    	if($cid!=0)
    	{
    	    $q1=$this->db->query('select * from cart where uid="'.$id.'" and cid="'.$cid.'"  and stat IN(0,1,2)');
        	if(!$q1->num_rows()>0)
        	{
        	    $date='';
        	    $q=$this->db->query('SELECT * FROM `courses` where id='.$cid.' and hide=0');
        	    if($q->num_rows()>0)
        	    {
            	    foreach($q->result() as $row)
            	    {
            	        $date=$row->date;
            	    }
            	    $date1=date_create($date);
                        $date2=date_create(date("Y-m-d"));
                        $diff=date_diff($date1,$date2);
                        $de=$diff->format("%R%a");
                        if($de<=0)
                        {
                    	    $d=array('uid'=>$id,'cid'=>$cid,'stat'=>0,'yr'=>$yr);
                    	    $this->db->insert('cart', $d);
                        }
                        else
                        {
                            $data['msg']='No puedes inscribirte para esta capacitación.';
                        }
        	    }
        	}
        	else
        	{
        	     $year=0;
        	     $stat='';
        	    foreach($q1->result() as $row)
        	    {
        	        $stat=$row->stat;
        	        $year=$row->yr;
        	    }
        	    if($year!=$yr)
        	    {
        	        if($stat==0 || $stat==2 || $stat==3)
        	        {
        	            
                	    $date='';
                	    $q=$this->db->query('SELECT * FROM `courses` where id='.$cid.' and hide=0');
                	    if($q->num_rows()>0)
                	    {
                    	    foreach($q->result() as $row)
                    	    {
                    	        $date=$row->date;
                    	    }
                    	    $date1=date_create($date);
                                $date2=date_create(date("Y-m-d"));
                                $diff=date_diff($date1,$date2);
                                $de=$diff->format("%R%a");
                                if($de<=0)
                                {
                            	    $data=array('yr'=>$yr,'stat'=>0);
                                    $this->db->where('uid', $id);
                                    $this->db->where('cid', $cid);
                                    $this->db->update('cart', $data);
                                }
                                else
                                {
                                    $data['msg']='No puedes inscribirte para esta capacitación.';
                                }
                	    }else
                	        {
                	            $data['msg']='El entrenamiento ya está seleccionado.';
                	        }
        	        }else
        	        {
        	            $data['msg']='El entrenamiento ya está seleccionado.';
        	        }
        	    }else
        	    {
        	        $data['msg']='El entrenamiento ya está seleccionado.';
        	    }
        	}
    	}
    	$data['yr']=$yr;
    	$data['cart']=$this->db->query('select cart.id as crid , courses.* FROM cart INNER JOIN courses ON cart.cid=courses.id where cart.uid="'.$id.'" and cart.stat="0" and cart.yr="'.$yr.'"');
    	$data['pen']=$this->db->query('select cart.id as crid , courses.* FROM cart INNER JOIN courses ON cart.cid=courses.id where cart.uid="'.$id.'" and cart.stat="2" and cart.yr="'.$yr.'"');
    	$data['enroll']=$this->UserData->getEnCourse($id);
    	$this->load->view('header',$nav);
    	$this->load->view('myaccount',$data);
    	$this->load->view('footer',$foot);
        }elseif(!empty($this->session->userdata("tuser")) && !empty($this->session->userdata("tpass")))
        {
            header( "Location: ".base_url()."Teacher" );
            exit(); 
        }else
        {
            header( "Location: ".base_url()."Home/user" );
        }
	    
	    
	}
	public function payment($id)
	{
	    
	        $id=$id;
    	    $price=0;
    	    $imgs=array();
    	    $title='';
    	    $date='';
    	    $q=$this->db->query('select cart.id as crid , courses.* FROM cart INNER JOIN courses ON cart.cid=courses.id where cart.id="'.$id.'" and cart.stat="0"');
    	    if(!$q->num_rows()==1)
    	    {
    	        header("Location: ".base_url().'Home/myaccount');
	            exit();
    	    }else
    	    {
        	    foreach($q->result() as $row)
        	    {
        	        $date=$row->date;
        	        $price=$row->price;
        	        $imgs=json_decode($row->imgs);
        	        $title=$row->title;
        	    }
        	    $date1=date_create($date);
                $date2=date_create(date("Y-m-d"));
                $diff=date_diff($date1,$date2);
                $de=$diff->format("%R%a");
                if($de<=0)
                {
                    if($price==0)
                    {
                        $data=array(
                            'ref'=>'cash',
                            'resultid'=>'cash',
                            'tid'=>'Efectivo',
                            'txt'=>'Efectivo',
                            'msg'=>'Efectivo',
                            'crid'=>$id,
                            'price'=>$price,
                            'date'=>date('Y-m-d'),
                            'stat'=>200
                            );
                        $this->db->insert('trans', $data);
                        $trid= $this->db->insert_id();
                        $data1=array('tid'=>$trid);
                        $this->UserData->update($id,$data1,'cart');
                        $this->db->insert('webhook',array('ref'=>$id,'data'=>json_encode(array('error'=>"code5"))));
                        $this->enrollForCourse($trid);
                        $data2=array(
                            'ref'=>$trid,
                            'data'=>json_encode(
                                array('Tipo de transacción'=>'Efectivo',
                                'Realizado por'=>'fuo Web')
                                )
                            );
                            $this->db->insert('webhook', $data2);
                            header("Location: ".base_url().'Home/myaccount?pay=1');
                            exit();
                    }else
                    {
        	    $img=base_url().$imgs[0];
        	    if(empty($title) && empty($img) && empty($price) && !$price>0)
        	    {
        	        echo json_encode(array('status'=>0,'msg'=>'Product error'));
        	        exit();
        	    }else
        	    {
        	        $trid=$this->UserData->genTrans($id);
        	        if($trid!=0)
        	        {
            	        $curl = curl_init();
                        curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api.mobbex.com/p/checkout",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "{\n\t\"total\":".$price.",\n\t\"currency\": \"ARS\",\n\t\"reference\": \"fuocart".$id."_".$trid."\",\n\t\"description\": \"Descripción de la Venta\",\n\t\"items\": [{\n\t\t\"image\": \"".$img."\",\n\t\t\"quantity\": 1,\n\t\t\"description\": \"".$title."\",\n\t\t\"total\": ".$price."\n\t}],\n\t\"options\": {\n\t\t\"domain\": \"fuoficios.com.ar\"\t\n\t},\n\t\"return_url\": \"https://fuoficios.com.ar/Home/returnUrl\",\n\t\"webhook\": \"https://fuoficios.com.ar/Home/webhook/".$trid."\"\n}",
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json",
                            "Postman-Token: 69e0bbb8-171d-4607-8835-f8de064cfb27",
                            "cache-control: no-cache",
                            "x-access-token: c7749039-24e2-48e0-a7e3-6e396ac117c9",
                            "x-api-key: tGVG906sdS6wCU5lZxjn1GXuBGvte5k9bJOA7dYg",
                            "x-lang: es"
                          ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);
                        
                        if ($err)
                        {
                            header("Location: ".base_url().'Home/myaccount?pay=0');
                            exit();
                        }else
                        {
                            //echo $response;
                           // exit();
                            $result=json_decode($response);
                            if($result->result ==1)
                            {
                                $data=$result->data;
                                $data1['trid']=$trid;
                                $data1['id']='v2/'.$data->id;
                                $data1['title']=$title;
                                $data1['price']=$price;
                                $data1['img']=$img;
                                $this->UserData->updTrans($data->id,"fuocart".$id."_".$trid,$price,$trid);
                                $this->UserData->updCartTrans($trid,$id);
                                $this->load->view('checkout',$data1);
                            }else
                            {
                                header("Location: ".base_url().'Home/myaccount?pay=0');
                                exit();
                            }
                        }
        	        }else
                        {
                            header("Location: ".base_url().'Home/myaccount?pay=0');
                            exit();
                        }
    	        }
                }
    	    }
    	    else
                        {
                            header("Location: ".base_url().'Home/myaccount?pay=0');
                            exit();
                        }
	    }
	}
	public function webhook($id)
	{
	    $this->db->insert('webhook',array('ref'=>$id,'data'=>json_encode($_POST)));
	    if(!empty($_POST))
	    {
	        if($_POST['type']=="checkout")
	        {
	            $data1=$_POST['data'];
	            if($data1['result']==true)
	            {
	                $data2=$data1['payment'];
	                $ref=$data2['reference'];
	                $data3=$data2['status'];
	                if($data3['code']=='200')
	                {
	                    $this->db->insert('webhook',array('ref'=>$id,'data'=>json_encode(array('error'=>"code1"))));
	                    $this->db->insert('webhook',array('data'=>json_encode(array('ref'=>$ref,'code'=>$data3['code']))));
	                    $data = array('resultid'=>$data2['id'],'txt'=>$data3['text'],'msg'=>$data3['message'],'stat'=>$data3['code'],'date'=>date('Y-m-d'));
	                    $this->db->where('id', $id);
	                    $this->db->update('trans', $data);
	                    $this->enrollForCourse($id);
	                }elseif($data3['code']=='2' || $data3['code']=='3')
	                {
	                    $this->db->insert('webhook',array('ref'=>$id,'data'=>json_encode(array('error'=>"code2"))));
	                    $this->db->insert('webhook',array('data'=>json_encode(array('ref'=>$ref,'code'=>$data3['code']))));
	                    $data = array('resultid'=>$data2['id'],'txt'=>$data3['text'],'msg'=>$data3['message'],'stat'=>$data3['code'],'date'=>date('Y-m-d'));
	                    $this->db->where('id', $id);
	                    $this->db->update('trans', $data);
	                    $this->pendingForCourse($id);
	                    
	                }else
	                {
	                    $this->db->insert('webhook',array('ref'=>$id,'data'=>json_encode(array('error'=>"code3"))));
	                    $this->db->insert('webhook',array('data'=>json_encode(array('ref'=>$ref,'code'=>$data3['code']))));
	                    $data = array('resultid'=>$data2['id'],'txt'=>$data3['text'],'msg'=>$data3['message'],'stat'=>$data3['code'],'date'=>date('Y-m-d'));
	                    $this->db->where('id', $id);
	                    $this->db->update('trans', $data);
	                    $this->FailedForCourse($id);
	                }
	            }else
	            {
	                $data = array('txt'=>'result error','msg'=>'payment error','stat'=>'1000','date'=>date('Y-m-d'));
	                $this->db->where('id', $id);
	                $this->db->update('trans', $data);
	                $this->FailedForCourse($id);
	            }
	            
	        }
	        else
	            {
	                $data = array('txt'=>'checkout error','msg'=>'payment error','stat'=>'2000','date'=>date('Y-m-d'));
	                $this->db->where('id', $id);
	                $this->db->update('trans', $data);
	                $this->FailedForCourse($id);
	            }
	    }
	    else
	            {
	                $data = array('txt'=>'post error','msg'=>'payment error','stat'=>'3000','date'=>date('Y-m-d'));
	                $this->db->where('id', $id);
	                $this->db->update('trans', $data);
	                $this->FailedForCourse($id);
	            }
	}
	
	public function returnUrl()
	{
	    echo "<script>window.close();</script>";
	}
	
	
	public function enrollForCourse($id)
	{
	    if(!empty($id))
	    {
	        $r1=$this->UserData->get($id,'trans');
	        if($r1->num_rows()>0)
	        {
	            $status='';
	            $cid='';
    	        foreach($r1->result() as $row)
    	        {
    	            $status=$row->stat;
    	            $cid=$row->crid;
    	        }
    	        if($status==200)
    	        {
    	            $uid='';
    	            $coid='';
    	            $price='';
    	            $r2=$this->UserData->get($cid,'cart');
    	            foreach($r2->result() as $row)
    	            {
    	                $uid=$row->uid;
    	                $coid=$row->cid;
    	                
    	            }
    	                $year=$this->UserData->getSetting(1);
	                    $code='';
	                    $title='';
	                    $q=$this->db->query('select * from courses where id='.$coid);
	                    foreach($q->result() as $row)
	                    {
	                        $code=$row->code;
	                        $title=$row->title;
	                        $price=$row->price;
	                    }
	                    $email='';
	                    $u=$this->db->query('select * from students where id='.$uid);
	                    foreach($u->result() as $row)
	                    {
	                        $email=$row->email;
	                    }
	                    $this->sendMail($email,'Inscripción',"Inscrito exitosamente para ".$title);
	                    $i=1;
	                    $t=0;
	                    $mq=$this->db->query('select * from enroll where code="'.$code.'" and uid="'.$uid.'"');
	                    if(!$mq->num_rows()>0)
    	               {
    	                   $this->db->insert('enroll',array('uid'=>$uid,'code'=>$code,'stat'=>0));
    	                   $this->db->where('id', $cid);
    	                   $data=array('stat'=>1);
    	                   $this->db->update('cart', $data);
    	               }
    	        }
	        }
	        
	    }
	    
	}
	
	private function pendingForCourse($id)
	{
	    if(!empty($id))
	    {
	        $r1=$this->UserData->get($id,'trans');
	        if($r1->num_rows()>0)
	        {
	            $status='';
	            $cid='';
    	        foreach($r1->result() as $row)
    	        {
    	            $status=$row->stat;
    	            $cid=$row->crid;
    	        }
    	        if($status==2 || $status==3)
    	        {
    	            $this->db->where('id', $cid);
    	            $data=array('stat'=>2);
    	            $this->db->update('cart', $data);
    	        }
	        }
	        
	    }
	    
	}
	
	private function FailedForCourse($id)
	{
	    if(!empty($id))
	    {
	        $r1=$this->UserData->get($id,'trans');
	        if($r1->num_rows()>0)
	        {
	            $status='';
	            $cid='';
    	        foreach($r1->result() as $row)
    	        {
    	            $status=$row->stat;
    	            $cid=$row->crid;
    	        }
    	        if($status!=200 || $status!=2 || $status!=3)
    	        {
    	            $this->db->where('id', $cid);
    	            $data=array('stat'=>3);
    	            $this->db->update('cart', $data);
    	        }
	        }
	        
	    }
	    
	}
	
	
	private function sendMail($email,$title,$cont)
	{
	            $head='<html><head>
	            <style>
                 .banner-color {
        			 background-color: rgb(0,151,100);
        			 padding: 36px 48px;
                 }
                 h1
                 {
                     font-family: "Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;
        			font-size: 30px;
        			font-weight: 300;
        			line-height: 150%;
        			margin: 0;
        			text-align: center;
                 }
        		.banner-color h4{
        			font-family: "Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;
        			font-size: 25px;
        			font-weight: 300;
        			line-height: 150%;
        			margin: 0;
        			text-align: center;
        			color: #ffffff;
        		}
                 .title-color {
                 color: #fff;
                 }
        		 .btn{
        		 padding: 8px;
            text-decoration: none;
            color: white;
            border-radius: 20px;
            background-color: rgb(0,151,100);
        		 }
              </style>
               </head>
               <body>
                  <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
                     <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                        <tbody>
                           <tr>
                              <td align="center">
                                 <center style="width:100%">
                                    <table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif;margin-top:50px;margin-bottom:50px;" width="512">
                                       <tbody>
                                          <tr>
                                             <td width="100%" style="padding:12px;text-align:center;">
            								 <h1 style="color:rgb(0,151,100);">';
            								 $body='</h1>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td align="left">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td width="100%">
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                               <tbody>
                                                                  <tr>
                                                                     <td align="center" class="banner-color">
                                                                        <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                                           <tbody>
                                                                              <tr>
                                                                                 <td width="100%" style="text-align:center;">
                                                                                 <img style="height:50px;" src="https://fuoficios.com.ar/assest/img/logo.png">
                                                                                    <h4>Ser para Hacer</h4>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding:25px;background-color:#fff;">';
                $foot=' </td>
                                                                                              </tr>
                                                                                              <tr>
                                                                                              </tr>
                                                                                              <tr>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                         </td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td colspan="2" valign="middle" id="m_-2155919752360402777credit" style="border-radius:6px;border:0;color:#8a8a8a;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:12px;line-height:150%;text-align:center;padding:24px 0">
                                        												<p style="margin:0 0 16px">- FUO</p>
                                        											</td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                             </center>
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                              </div>
                                           </body>
                                        </html>';
	            $this->load->library('email');
                $config = array();
            	$config['protocol'] = 'smtp';
            	$config['smtp_host'] = 'fuoficios.com.ar';
            	$config['smtp_user'] = 'contacto@fuoficios.com.ar';
            	$config['smtp_pass'] = 'mocona2222';
            	$config['smtp_port'] = 25;
            	$config['charset']    = 'utf-8';
            	$config['mailtype'] = 'html';
            	$config['priority'] = 1;
            	$this->email->initialize($config);
            	$this->email->set_newline("\r\n");
            	$this->email->from('contacto@fuoficios.com.ar', 'FUO');
            	$this->email->to($email);
                $this->email->subject($title);
                $this->email->message($head.$title.$body.$cont.$foot);
                //Send mail
                $this->email->send();
	}
	
	public function feedback($id)
	{
	    $this->load->view('feedback',array('id'=>$id));
	}
	
	public function updatefeed()
	{
	    $this->db->query("UPDATE `enroll` SET `feed` = '0' WHERE `id` =".$_POST['id']);
	}
	public function certificate($id)
	{
	    $this->db->select('students.name,students.lname,students.pid,batch.title,batch.data,certi.date');
        $this->db->from('certi');
        $this->db->join('students', 'students.id = certi.uid');
        $this->db->join('enroll', 'enroll.id = certi.eid');
        $this->db->join('batch', 'batch.id = enroll.bid');
        $this->db->where('certi.id', $id);
        $q = $this->db->get();
        if($q->num_rows() >0)
        {
            $name='';
            $dni='';
            $title='';
            $d='';
            $di='';
            $date='';
            $data='';
            foreach($q->result() as $row)
            {
                $name=$row->name;
                $name.=' '.$row->lname;
                $dni=$row->pid;
                $title=$row->title;
                $data=json_decode($row->data);
                $date=$row->date;
            }
        
	    $html='<html>
<head>
<style>
@import url("https://fonts.googleapis.com/css2?family=Lato:wght@900&display=swap");

body{
font-family: sans-serif;
}
.center
{
	text-align:center;
}
.top{
padding-top:420px;
}
h6,h1,h2{
margin:10px;
}
.font-n
{
	font-size:2.2rem;
	font-weight:500;
}
.font-h{
font-family: "Lato", sans-serif;
font-size:3.7rem;
	font-weight:1000;
}
.font-h2{
font-family: "Lato", sans-serif;
font-size:3rem;
	font-weight:1000;
}
img
{
height:250px;
top:830px;
position:absolute;
}
.img-l{
left:190px;
}
.img-c{
top:800px;
left:39%;
height:380px;
}
.img-r{
top:850px;
height:200px;
right:430px;
}
p{
position:absolute;
text-align:center;
}
.block
{
top:1000px;
border-top:1px solid #000;
width:200px;
}
.block-l{
left:200px;
}
.block-r{
right:200px;
}
.block-c
{
top:1090px;
width:250px;
left:43%;
}
</style>
</head>
<body style="margin:0px;">
<div style="background-image: url('.base_url().'images/certi/certi.png);height: 1220px;width: 100%;background-position: center;background-size: cover;background-repeat:no-repeat;">

<h6 class="center top font-n">La Fundación Universitaria de Oficios certifica que:</h6>
<h1 class="center font-h">'.$name.'</h1>
<h6 class="center font-n">D.N.I No. <b>'.$dni.'</b></h6>
<h6 class="center font-n">Ha aprobado el Curso Teórico - Práctico:</h6>
<h1 class="center font-h2">'.$title.'</h1>
<h6 class="center font-n">Con una carga horaria de '.$data->dur.' '.$data->durin.'</h6>
<img class="img-l" src="'.base_url().'images/certi/sign1.png">
<img class="img-c" src="'.base_url().'images/certi/fuostamp.png">
<img class="img-r" src="'.base_url().'images/certi/sign2.jpeg">
<p class="block block-l"><b>Mery Lunge</b><br>Presidenta<br>Fundación Universitaria de Oficios</p>
<p class="block-c">Córdoba, '.date('d',strtotime($date)).' de '.date('F',strtotime($date)).' de '.date('Y',strtotime($date)).'</p>
<p class="block block-r"><b>Madda Davila</b><br>Directora Académica<br>Fundación Universitaria de Oficios</p>
</div>
</body>
</html>';
	    $this->load->library('pdf');
	    $this->pdf->set_option('isRemoteEnabled', true);
	    $this->pdf->set_paper(array(0,0,1387,1035));
	    $this->pdf->loadHtml($html);
        $this->pdf->render();
        $this->pdf->stream("n.pdf", array("Attachment"=>0));
	}
	}
}