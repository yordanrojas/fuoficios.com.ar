<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends CI_Controller {
    var $mtid='';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('TeacherData');
        if(!empty($this->session->userdata('tuser')) && !empty($this->session->userdata('tpass')))
        {
            $data=$this->TeacherData->valid($this->session->userdata('tuser'),$this->session->userdata('tpass'));
            if($data->num_rows()==1)
            {
                $user='';
                $pass='';
                foreach($data->result() as $row)
                {
                    $user=$row->email;
                    $pass=$row->pass;
                    $this->mtid=$row->id;
                }
                if($user!=$this->session->userdata('tuser') && $pass!=$this->session->userdata('tpass'))
                {
                    session_destroy();
                    redirect(base_url().'Home/login', 'refresh');
                }
            }else
            {
                session_destroy();
                redirect(base_url().'Home/login', 'refresh');
            }
            
        }else
        {
            session_destroy();
            redirect(base_url().'Home/login', 'refresh');
        }
    }

	public function index()
	{
	    $nav['nav']=1;
		$this->load->view('teacher/header',$nav);
		$this->load->view('teacher/main');
		$this->load->view('teacher/footer');
	}
	
	public function logout()
	{
	    session_destroy();
        redirect(base_url().'Home/login', 'refresh');
	}
		public function batch()
	{
	    $nav['nav']=2;
    	$id=$this->mtid;
    	$yr=$this->TeacherData->getSetting(1);
    	$data['batch']=$this->db->query('select title,yr,sr,code,id from batch where tid="'.$id.'" and yr="'.$yr.'"');
    	echo $id;
		$this->load->view('teacher/header',$nav);
		$this->load->view('teacher/batch',$data);
		$this->load->view('teacher/footer');
	}
	
	public function viewBtach($id)
	{
	    if(!empty($id))
	    {
	        $nav['nav']=2;
	        $data['batch']=$this->TeacherData->get($id,'batch');
	        $this->load->view('teacher/header',$nav);
    		$this->load->view('teacher/viewBatch',$data);
    		$this->load->view('teacher/footer');
	        
	    }else
	    {
	        redirect(base_url().'Teacher/batch', 'refresh');
	    }
	}
	
		public function attend($id)
	{
	    if(!empty($id))
	    {
	        $nav['nav']=2;
	        $data['bid']=$id;
	        $data['batch']=$this->db->query('select attend from batch where id='.$id);
	        $this->db->select('atten.data,students.name,students.lname,atten.pday,atten.tday,atten.total,atten.id');
            $this->db->from('atten');
            $this->db->join('batch', 'batch.id = atten.bid');
            $this->db->join('enroll', 'enroll.id = atten.eid');
            $this->db->join('students', 'students.id = enroll.uid');
            $this->db->where('atten.bid', $id);
            $data['s']= $this->db->get();
	        $this->load->view('teacher/header',$nav);
    		$this->load->view('teacher/attend',$data);
    		$this->load->view('teacher/footer');
	        
	    }else
	    {
	        redirect(base_url().'Teacher/batch', 'refresh');
	    }
	}
	
	public function students($id)
	{
	    if(!empty($id))
	    {
	        $nav['nav']=2;
	        $data['id']=$id;
	        $this->db->select('students.name,students.lname,students.email,students.mob,enroll.id,enroll.uid,enroll.stat');
            $this->db->from('enroll');
            $this->db->join('students', 'students.id = enroll.uid');
            $this->db->where('enroll.bid', $id);
            $data['students']= $this->db->get();
	        $this->load->view('teacher/header',$nav);
    		$this->load->view('teacher/students',$data);
    		$this->load->view('teacher/footer');
	        
	    }else
	    {
	        redirect(base_url().'Teacher/batch', 'refresh');
	    }   
	}
	
	public function tool()
	{
	    $this->db->update('enroll', array('stat'=>$_POST['c']), array('id' => $_POST['id']));
	    if($_POST['c']==1)
	    {
	        $data=array('sub'=>'Herramientas.',
                            'cont'=>'Por favor envíe la herramienta dada para el grupo :- '.$row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr)),
                            'img'=>'images/tool.png',
                            'who'=>2,
                            'uid'=>$_POST['uid'],
                            'type'=>2);
            $this->db->insert('notice', $data); 
	    }else
	    {
	        $this->db->query('delete from notice where uid="'.$_POST['uid'].'" and type=2');
	    }
	}
	
	public function result($id)
	{
	    if(!empty($id))
	    {
	        $nav['nav']=2;
	        $data['id']=$id;
            $data['result']= $this->TeacherData->getf('eid',$id,'result');
	        $this->load->view('teacher/header',$nav);
    		$this->load->view('teacher/result',$data);
    		$this->load->view('teacher/footer');
	        
	    }else
	    {
	        redirect(base_url().'Teacher/batch', 'refresh');
	    }   
	}
	
	public function addResult($id=0)
	{
	        $data=array();
	        $nav['nav']=2;
	        if(!empty($id))
	        {
	            $data['id']=$id;
	        }
	        if(!empty($rid))
	        {
	            $data['result']=$this->TeacherData->get($rid,'result');
	        }
	        $this->load->view('teacher/header',$nav);
    		$this->load->view('teacher/addResult',$data);
    		$this->load->view('teacher/footer');
	}
	
		public function viewResult($id,$rid)
	{
	        $data=array();
	        $nav['nav']=2;
	        if(!empty($id))
	        {
	            $data['id']=$id;
	        }
	        if(!empty($rid))
	        {
	            $data['result']=$this->TeacherData->get($rid,'result');
	        }
	        $this->load->view('teacher/header',$nav);
    		$this->load->view('teacher/viewResult',$data);
    		$this->load->view('teacher/footer');
	}
	
		public function deleteResult($id,$rid)
	{
	    
	        if($this->TeacherData->delete2($rid,'result'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Borrado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/result/".$id);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/result/".$id);
            }
	}
	
			public function myProfile()
	{
	    $nav['nav']=2;
	    $data['teacher']=$this->TeacherData->getf('email',$this->session->userdata("tuser"),'teachers');
	    $data['id']=$this->mtid;
		$this->load->view('teacher/header',$nav);
		$this->load->view('teacher/myProfile',$data);
		$this->load->view('teacher/footer');
	}
	
	
	public function cal()
     {
        $data = array();
        $d=date('Y-m-d', strtotime($_POST['v']." months", strtotime($_POST['d'])));
    	$id=$this->mtid;
    	$yr=$this->TeacherData->getSetting(1);
    	$r2=$this->db->query('select id,attend,code,sr,yr from batch where tid='.$id);
    	if($r2->num_rows() > 0)
    	{
    	    foreach($r2->result() as $row)
    	    {
    	        foreach(json_decode($row->attend) as $day=>$v)
    	        {
    	            $cdy1=date('Y',strtotime($d));
    	            $cdy2=date('Y',strtotime($day));
    	            $cdm1=date('m',strtotime($d));
    	            $cdm2=date('m',strtotime($day));
    	          if($cdy1==$cdy2 && $cdm1==$cdm2)
    	          {
    	              if(empty($data[intval(date('d',strtotime($day)))]))
    	              {
    	                $data[intval(date('d',strtotime($day)))]='<a href="'.base_url().'Teacher/viewBtach/'.$row->id.'">'.$row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr)).'</a>';
    	              }else
    	              {
    	                  $data[intval(date('d',strtotime($day)))]=$data[intval(date('d',strtotime($day)))].'<br><a href="'.base_url().'Teacher/viewBtach/'.$row->id.'">'.$row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr)).'</a>';
    	              }
    	          }
    	        }
    	    }
    	}
         $prefs['template'] = '

        {table_open}<table border="0" cellpadding="0" cellspacing="0" class="table">{/table_open}

        {heading_row_start}<tr>{/heading_row_start}

        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}"><center><h4 class="text-white">{heading}<h4></center></th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

        {heading_row_end}</tr>{/heading_row_end}

        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td>{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}

        {cal_row_start}<tr>{/cal_row_start}
        {cal_cell_start}<td>{/cal_cell_start}
        {cal_cell_start_today}<td>{/cal_cell_start_today}
        {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

        {cal_cell_content}<div class="highlight">{day}<br>{content}</div>{/cal_cell_content}
        {cal_cell_content_today}<div class="highlight2">{day}<br>{content}</div>{/cal_cell_content_today}

        {cal_cell_no_content}{day}{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="highlight2">{day}</div>{/cal_cell_no_content_today}

        {cal_cell_blank}&nbsp;{/cal_cell_blank}

        {cal_cell_other}{day}{/cal_cel_other}

        {cal_cell_end}</td>{/cal_cell_end}
        {cal_cell_end_today}</td>{/cal_cell_end_today}
        {cal_cell_end_other}</td>{/cal_cell_end_other}
        {cal_row_end}</tr>{/cal_row_end}

        {table_close}</table>{/table_close}
        ';
              $this->load->library('calendar', $prefs);
        
        echo $this->calendar->generate(date('Y',strtotime($d)), date('m',strtotime($d)), $data);
     }
}