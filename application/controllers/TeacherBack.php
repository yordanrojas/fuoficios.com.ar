<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeacherBack extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('TeacherData');
    }
    public function index()
    {
        
    }
    
    public function attend($id)
    {
        
        if(!empty($_POST['ids']) && !empty($_POST['dates']))
        {
            $ids=json_decode($_POST['ids']);
            $dates=json_decode($_POST['dates']);
            $data=array();
            foreach($ids as $i)
            {
                $data1=array();
                $data1['id']=$i;
                $tday=0;
                $pday=0;
                $total=0;
                $data2=array();
                foreach($dates as $d)
                {
                    $tday++;
                    if(!empty($_POST['id'.$i.'-'.$d]))
                    {
                        $data2[$d]=1;
                        $pday++;
                        
                    }else
                    {
                        $data2[$d]=0;
                    }
                }
                $data1['pday']=$pday;
                $data1['tday']=$tday;
                $data1['total']=($pday/$tday)*100;
                $data1['data']=json_encode($data2);
                $data[]=$data1;
            }
            $this->db->update_batch('atten', $data, 'id');
            redirect(base_url().'Teacher/attend/'.$id, 'refresh');
        }else
        {
            redirect(base_url().'Teacher/batch', 'refresh');
        }
    }
    
    public function saveResult($id)
    {
        $data=array(
            'eid'=>$id,
            'title'=>$_POST['title'],
            'marks'=>$_POST['marks']
            );
            if($this->TeacherData->add($data,'result'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Control deslizante agregado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/result/".$id);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/result/".$id);
            }
    }
    
        public function editResult($id,$rid)
    {
        $data=array(
            'title'=>$_POST['title'],
            'marks'=>$_POST['marks']
            );
            if($this->TeacherData->update($rid,$data,'result'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Control deslizante agregado con éxito',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/result/".$id);
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/result/".$id);
            }
    }
    
            public function updateProfile($id)
    {
        $img='';
        if (!empty($_FILES['img']['name']))
        {
        $config['upload_path'] = 'images/teacher/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid()) . $_FILES['img']['name'];
        //Load upload library and initialize configuration
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('img'))
        {
            $uploadData = $this->upload->data();
            $img = 'images/teacher/'.$uploadData['file_name'];
        }
        else
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Teacher/myProfile");
        }    
        }
        if(empty($_POST['email']))
        {
            $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Faltan algunos campos.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
            redirect(base_url()."Teacher/myProfile");
        }else
        {
            $cq=$this->db->query('select * from teachers where email ="'.$_POST['email'].'" and id not IN('.$id.')');
            if($cq->num_rows() > 0)
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'El correo es usado por otro profesor.',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/myProfile");
            }else
            {
            $data=array(
                'email'=>$this->input->post('email'));
            if(!empty($_POST['pass']))
            {
                $data['pass']=md5($this->input->post('pass'));
            }
            if(!empty($img))
            {
                $data['img']=$img;
            }
            if($this->TeacherData->update($id,$data,'teachers'))
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Success',message: 'Actualizado exitosamente',},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/myProfile");
            }
            else
            {
                $this->session->set_userdata("adminmsg", "<script>$.notify({icon: 'la la-bell',title: 'Error',message: 'Se produjo un error',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});</script>");
                redirect(base_url()."Teacher/myProfile");
            }
        }
        }
    }
}