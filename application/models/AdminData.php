<?php


class AdminData extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function select($table)
    {
        //data is retrive from this query
        $query = $this->db->get($table);
        return $query;
    }
    public function get($id,$table)
    {
        //data is retrive from this query
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query;
    }
     public function getf($field,$id,$table)
    {
        //data is retrive from this query
        $this->db->where($field, $id);
        $query = $this->db->get($table);
        return $query;
    }
    public function selectdes($table)
    {
        //data is retrive from this query
        $query = $this->db->query("Select * from ".$table." order by id desc ");
        return $query;
    }
    public function valid($user,$pass)
    {
        //data is retrive from this query
        $query = $this->db->query('SELECT * FROM `admin` WHERE user="'.$user.'" AND pass="'.$pass.'" ');
        return $query;
    }
    public function add($data ,$table){
        if($this->db->insert($table, $data)){
            return true;
        }else{
            return false;
        }
    }
    public function  delete($id,$table){
        $this->db->where('id', $id);
        $r=$this->db->get($table);
        foreach ($r->result() as $ra) {
                try {
                    unlink("./".$ra->img);
                }
                catch(Exception $e) {
                }
        }
        $this->db->where('id', $id);
        if($this->db->delete($table)){
            return true;
        }else{
            return false;
        }

    }
     public function  delete2($id,$table){
        $this->db->where('id', $id);
        if($this->db->delete($table)){
            return true;
        }else{
            return false;
        }

    }
    
    public function  deletef($f,$id,$table){
        $this->db->where($f, $id);
        if($this->db->delete($table)){
            return true;
        }else{
            return false;
        }

    }

    public function update($id,$data,$table)
    {
        $this->db->where('id', $id);

        if($this->db->update($table, $data)){
            return true;
        }else{
            return false;
        }
    }



    public function updatePass($usern,$data)
    {
        $this->db->where('username', $usern);

        if($this->db->update('admin', $data)){
            return true;
        }else{
            return false;
        }
    }
    public function get_count() {
        return $this->db->count_all('gallery');
    }

    public function get_authors($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get('gallery');

        return $query->result();
    }
        public function getSetting($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('setting');
        foreach($query->result() as $row)
        {
            return $row->value;
        }
    }
    public function selectCurrStudent($y)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,students.tool,enroll.id as eid,enroll.ins,enroll.code,enroll.yr,enroll.sr,doc.doc1,doc.doc2,doc.doc3,doc.doc4,doc.doc5,doc.doc6');
        $this->db->from('students');
        $this->db->join('enroll', 'enroll.uid = students.id');
        $this->db->join('doc', 'doc.uid = students.id');
        $this->db->where('enroll.yr', $y);
        $this->db->order_by('enroll.id', 'DESC');
        $query = $this->db->get();
        return $query;
    }

    public function selectPenroll($y)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,courses.title,cart.id');
        $this->db->from('cart');
        $this->db->join('courses', 'cart.cid = courses.id');
        $this->db->join('students', 'cart.uid = students.id');
        $this->db->where('cart.stat', 0);
        $this->db->where('cart.yr', $y);
        $this->db->order_by('cart.id', 'DESC');
        $query = $this->db->get();
        return $query;
    }
    public function getPenroll($id)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,courses.title,courses.price,cart.id,cart.uid,cart.cid');
        $this->db->from('cart');
        $this->db->join('courses', 'cart.cid = courses.id');
        $this->db->join('students', 'cart.uid = students.id');
        $this->db->where('cart.stat', 0);
        $this->db->where('cart.id', $id);
        $query = $this->db->get();
        return $query;
    }
    public function selectOenroll($y)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,courses.title,cart.id,trans.date');
        $this->db->from('cart');
        $this->db->join('courses', 'cart.cid = courses.id');
        $this->db->join('students', 'cart.uid = students.id');
        $this->db->join('trans', 'cart.tid = trans.id');
        $this->db->where('cart.stat', 2);
        $this->db->where('cart.yr', $y);
        $this->db->order_by('cart.id', 'DESC');
        $query = $this->db->get();
        return $query;
    }
        public function getOenroll($id)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,courses.title,courses.price,cart.id,cart.tid,cart.uid,cart.cid,webhook.data');
        $this->db->from('cart');
        $this->db->join('courses', 'cart.cid = courses.id');
        $this->db->join('students', 'cart.uid = students.id');
        $this->db->join('trans', 'cart.tid = trans.id');
        $this->db->join('webhook', 'trans.id = webhook.ref');
        $this->db->where('cart.stat', 2);
        $this->db->where('cart.id', $id);
        $query = $this->db->get();
        return $query;
    }
    public function selectCenroll($y)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,courses.title,cart.id');
        $this->db->from('cart');
        $this->db->join('courses', 'cart.cid = courses.id');
        $this->db->join('students', 'cart.uid = students.id');
        $this->db->where('cart.stat', 1);
        $this->db->where('cart.yr', $y);
        $this->db->order_by('cart.id', 'DESC');
        $query = $this->db->get();
        return $query;
    }
       public function getCenroll($id)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,courses.title,courses.price,cart.id,cart.tid,cart.uid,cart.cid,webhook.data');
        $this->db->from('cart');
        $this->db->join('courses', 'cart.cid = courses.id');
        $this->db->join('students', 'cart.uid = students.id');
        $this->db->join('trans', 'cart.tid = trans.id');
        $this->db->join('webhook', 'trans.id = webhook.ref');
        $this->db->where('cart.stat', 1);
        $this->db->where('cart.id', $id);
        $query = $this->db->get();
        return $query;
    }
    
    public function selectAenroll($y)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,courses.title,cart.id');
        $this->db->from('cart');
        $this->db->join('courses', 'cart.cid = courses.id');
        $this->db->join('students', 'cart.uid = students.id');
        $this->db->where('cart.stat', 3);
        $this->db->where('cart.yr', $y);
        $this->db->order_by('cart.id', 'DESC');
        $query = $this->db->get();
        return $query;
    }
           public function getAenroll($id)
    {
        $this->db->select('students.id as sid,students.name,students.lname,students.email,courses.title,courses.price,cart.id,cart.tid,cart.uid,cart.cid,webhook.data');
        $this->db->from('cart');
        $this->db->join('courses', 'cart.cid = courses.id');
        $this->db->join('students', 'cart.uid = students.id');
        $this->db->join('trans', 'cart.tid = trans.id');
        $this->db->join('webhook', 'trans.id = webhook.ref');
        $this->db->where('cart.stat', 3);
        $this->db->where('cart.id', $id);
        $query = $this->db->get();
        return $query;
    }
    public function getEnCourse($id,$y)
    {
        $this->db->select('enroll.id as enid,enroll.feed,batch.coid,batch.link,batch.id as bid,batch.dfrm,batch.dto,batch.stat,batch.title,batch.data,teachers.id as tid,teachers.name as tname,teachers.img,courses.imgs');
        $this->db->from('enroll');
        $this->db->join('batch', 'batch.id = enroll.bid');
        $this->db->join('teachers', 'teachers.id = batch.tid');
        $this->db->join('courses', 'batch.coid = courses.id');
        $this->db->where('enroll.yr', $y);
        $this->db->where('enroll.uid', $id);
        $this->db->order_by('batch.id', 'asc');
        $query = $this->db->get();
        return $query;
    }
    public function getpEnCourse($id,$y)
    {
        $this->db->select('enroll.id as enid,enroll.feed,batch.coid,batch.link,batch.id as bid,batch.dfrm,batch.dto,batch.stat,batch.title,batch.data,courses.imgs');
        $this->db->from('enroll');
        $this->db->join('batch', 'batch.id = enroll.bid');
        $this->db->join('courses', 'batch.coid = courses.id');
        $this->db->where('enroll.yr', $y);
        $this->db->where('enroll.uid', $id);
        $this->db->where('batch.stat', 0);
        $query = $this->db->get();
        return $query;
    }
    
    public function getppEnCourse($id)
    {
        $this->db->select('enroll.id as enid,enroll.feed,courses.id as coid,courses.title,courses.data,courses.imgs');
        $this->db->from('enroll');
        $this->db->join('courses', 'enroll.code = courses.code');
        $this->db->where('enroll.uid', $id);
        $this->db->where('enroll.bid', 0);
        $query = $this->db->get();
        return $query;
    }
    
    
    
    public function stdEnroll($id)
    {
        $this->db->select('enroll.id as enid,atten.total,enroll.feed,batch.coid,enroll.code,enroll.yr,enroll.sr,batch.link,batch.id as bid,batch.dfrm,batch.dto,batch.stat,batch.title,batch.data,teachers.dsg,teachers.name as tname');
        $this->db->from('enroll');
        $this->db->join('batch', 'batch.id = enroll.bid');
        $this->db->join('teachers', 'teachers.id = batch.tid');
        $this->db->join('atten', 'atten.eid = enroll.id');
        $this->db->where('enroll.uid', $id);
        $this->db->where('batch.stat', 1);
        $this->db->order_by('batch.yr', 'desc');
        $query = $this->db->get();
        return $query;
    }
    
    public function getDeCourse($id)
    {
        $this->db->select('batch.title,batch.data,courses.imgs');
        $this->db->from('batch');
        $this->db->join('courses', 'courses.id = batch.coid');
        $this->db->where('batch.id', $id);
        $query = $this->db->get();
        return $query;
    }
    public function selectPbatch($yr)
    {
        $this->db->where('stat','0');
        $query = $this->db->get('batch');
        return $query;
    }
    
    public function selectPbatchstd()
    {
        $this->db->select('students.name,students.lname,students.email,students.mob,enroll.code,enroll.id');
        $this->db->from('enroll');
        $this->db->join('students', 'students.id = enroll.uid');
        $this->db->where('bid','0');
        $query = $this->db->get();
        return $query;
    }
    
    public function selectPbatchstdi($id)
    {
        $this->db->select('students.name,students.lname,students.email,students.mob');
        $this->db->from('enroll');
        $this->db->join('students', 'students.id = enroll.uid');
        $this->db->where('bid',$id);
        $query = $this->db->get();
        return $query;
    }
    
    public function selectCbatch($yr)
    {
        $this->db->where('stat','1');
        $query = $this->db->get('batch');
        return $query;
    }
    
    var $table = "students";
    var $select_column = array("students.id", "students.name", "students.lname", "students.email", "students.mob", "doc.doc1", "doc.doc2", "doc.doc3", "doc.doc4", "doc.doc5", "doc.doc6");
    var $order_column = array("id","name", "email", "mob", null,null);
    function make_query()
    {
        $this->db->select($this->select_column);
        $this->db->from($this->table);
        $this->db->join('doc', 'doc.uid = students.id');
        if(isset($_POST["search"]["value"]))
        {
            $this->db->like("students.name", $_POST["search"]["value"]);
            $this->db->or_like("students.lname", $_POST["search"]["value"]);
            $this->db->or_like("students.id", $_POST["search"]["value"]);
            $this->db->or_like("students.email", $_POST["search"]["value"]);
            $this->db->or_like("students.mob", $_POST["search"]["value"]);
        }
        if(isset($_POST["order"]))
        {
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else
        {
            $this->db->order_by('id', 'DESC');
        }
    }
    function make_datatables(){
        $this->make_query();
        if($_POST["length"] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    function get_filtered_data(){
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function get_all_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join('doc', 'doc.uid = students.id');
        return $this->db->count_all_results();
    }
    
    function getDoc()
    {
        $this->db->select("students.id as uid,students.name,students.lname,students.email,doc.id,doc.doc1,doc.doc2,doc.doc3,doc.doc4,doc.doc5,doc.doc6");
        $this->db->from('students');
        $this->db->join('doc', 'doc.uid = students.id');
        $this->db->where('doc.stat','0');
        $query = $this->db->get();
        return $query;
    }
    function getPbatch($id)
    {
        $this->db->select("batch.*,courses.data");
        $this->db->from('batch');
        $this->db->join('courses', 'courses.id = batch.coid');
        $this->db->where('batch.id',$id);
        $query = $this->db->get();
        return $query;
    }
    function getCbatch($id)
    {
        $this->db->select("batch.*,teachers.name,teachers.lname,teachers.dsg");
        $this->db->from('batch');
        $this->db->join('teachers', 'teachers.id = batch.tid');
        $this->db->where('batch.id',$id);
        $query = $this->db->get();
        return $query;
    }
    
    function exportStudent($v1,$v2)
    {
        $this->db->select("enroll.code,enroll.yr,enroll.sr,enroll.id,students.name,students.lname,students.email,students.mob,students.pid,batch.title,batch.price");
        $this->db->from('enroll');
        $this->db->join('students','students.id = enroll.uid');
        $this->db->join('batch', 'batch.id = enroll.bid');
        $this->db->where('(batch.dfrm BETWEEN "'.$v1.'" AND "'.$v2.'" ) OR (batch.dto BETWEEN "'.$v1.'" AND "'.$v2.'" )');
        $query = $this->db->get();
        return $query;
    }
    function exportStudentB($id)
    {
        $this->db->select("enroll.code,enroll.yr,enroll.sr,enroll.id,students.name,students.lname,students.email,students.mob,students.pid,batch.title,batch.price");
        $this->db->from('enroll');
        $this->db->join('students','students.id = enroll.uid');
        $this->db->join('batch', 'batch.id = enroll.bid');
        $this->db->where('enroll.bid',$id);
        $query = $this->db->get();
        return $query;
    }
}