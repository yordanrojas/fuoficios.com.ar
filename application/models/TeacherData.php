<?php


class TeacherData extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function select($table)
    {
        //data is retrive from this query
        $query = $this->db->get($table);
        return $query;
    }
    public function get($id,$table)
    {
        //data is retrive from this query
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query;
    }
     public function getf($field,$id,$table)
    {
        //data is retrive from this query
        $this->db->where($field, $id);
        $query = $this->db->get($table);
        return $query;
    }
    public function selectdes($table)
    {
        //data is retrive from this query
        $query = $this->db->query("Select * from ".$table." order by id desc ");
        return $query;
    }
    public function valid($user,$pass)
    {
        //data is retrive from this query
        $query = $this->db->query('SELECT * FROM `teachers` WHERE email="'.$user.'" AND pass="'.$pass.'" ');
        return $query;
    }
    public function add($data ,$table){
        if($this->db->insert($table, $data)){
            return true;
        }else{
            return false;
        }
    }
    public function  delete($id,$table){
        $this->db->where('id', $id);
        $r=$this->db->get($table);
        foreach ($r->result() as $ra) {
                try {
                    unlink("./".$ra->img);
                }
                catch(Exception $e) {
                }
        }
        $this->db->where('id', $id);
        if($this->db->delete($table)){
            return true;
        }else{
            return false;
        }

    }
     public function  delete2($id,$table){
        $this->db->where('id', $id);
        if($this->db->delete($table)){
            return true;
        }else{
            return false;
        }

    }
        public function getSetting($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('setting');
        foreach($query->result() as $row)
        {
            return $row->value;
        }
    }
        public function update($id,$data,$table)
    {
        $this->db->where('id', $id);

        if($this->db->update($table, $data)){
            return true;
        }else{
            return false;
        }
    }
}