<?php


class UserData extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function slider($table)
    {
        $this->db->order_by("pos", "asc");
        $query = $this->db->get($table);
        return $query;
    }
    
    
    public function select($table)
    {
        $query = $this->db->get($table);
        return $query;
    }
    
    public function getNav()
    {
        $this->db->select('DISTINCT(area)');
        return $this->db->get('courses');
    }
    
    public function selectCourse()
    {
        $this->db->limit(5);
        $this->db->order_by("code2");
        $this->db->where('hide', 0);
        $query = $this->db->get('courses');
        return $query;
    }
    public function get($id,$table)
    {
        //data is retrive from this query
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query;
    }
    
     public function getf($field,$id,$table)
    {
        //data is retrive from this query
        $this->db->where($field, $id);
        $query = $this->db->get($table);
        return $query;
    }
    
    public function get_count($d=0) {
        if(empty($d))
        {
            return $this->db->count_all('courses');
        }else
        {
            return $this->db->where('area',$d)->from("courses")->count_all_results();
        }
    }

    public function get_courses($d=0,$limit, $start) {
        if(empty($d))
        {
            $this->db->limit($limit, $start);
            $this->db->order_by("code2");
            $this->db->where('hide', 0);
            $query = $this->db->get('courses');
            return $query->result();
        }else
        {
            $this->db->limit($limit, $start);
            $this->db->order_by("code2");
            $this->db->where('hide', 0);
            $this->db->where('area', $d);
            $query = $this->db->get('courses');
            return $query->result();
        }
    }
    public function add($data ,$table){
        if($this->db->insert($table, $data)){
            return true;
        }else{
            return false;
        }
    }
    public function  delete($id,$table){
        $this->db->where('id', $id);
        if($this->db->delete($table)){
            return true;
        }else{
            return false;
        }

    }
        public function update($id,$data,$table)
    {
        $this->db->where('id', $id);

        if($this->db->update($table, $data)){
            return true;
        }else{
            return false;
        }
    }
    
    public function genTrans($id){
        $data=array(
            'tid'=>NULL,
            'txt'=>NULL,
            'msg'=>NULL,
            'crid'=>$id,
            'stat'=>0
            );
        if($this->db->insert('trans', $data)){
            return $this->db->insert_id();
        }else{
            return 0;
        }
    }
    
    public function updTrans($trid,$ref,$price,$id)
    {
        $this->db->where('id', $id);
        $data=array('tid'=>$trid,
                    'ref'=>$ref,
                    'price'=>$price);
        $this->db->update('trans', $data);
    }
    public function updCartTrans($tid,$id)
    {
        $this->db->where('id', $id);
        $data=array('tid'=>$tid);
        $this->db->update('cart', $data);
    }
    public function updTransID($trid,$id)
    {
        $this->db->where('id', $id);
        $data=array('tid'=>$trid);
        $this->db->update('cary', $data);
    }
    public function getCid($trid)
    {
        $this->db->where('id', $trid);
        $query = $this->db->get('trans');
        foreach($query->result() as $row)
        {
            return $row->cid;
        }
    }
    public function getSetting($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('setting');
        foreach($query->result() as $row)
        {
            return $row->value;
        }
    }
        public function getEnCourse($id)
    {
        $this->db->select('enroll.id as enid,courses.id as cid,courses.title,courses.type,courses.imgs');
        $this->db->from('enroll');
        $this->db->join('courses', 'enroll.code = courses.code');
        $this->db->where('enroll.uid', $id);
        $this->db->order_by('enroll.id', 'desc');
        $query = $this->db->get();
        return $query;
    }
}
?>