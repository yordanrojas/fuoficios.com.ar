<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/pbatch">Atrás</a>
<h4 class="page-title">Añadir Grupos</h4>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Grupos</div>
        </div>
    	<div class="card-body row">
    	    <div class="col-sm-1"></div>
    	    <div class="col-sm-5">
    	        <select class="form-control" name="code" id="code" required autofocus>
    	            <?php
    	            foreach($batch->result() as $row)
    	            {
    	            ?>
    	            <option value='<?php echo json_encode($row);?>'><?php echo $row->code;?>(<?php echo $row->title;?>)</option>
    	            <?php
    	            }
    	            ?>
    	        </select>
    	        </div>
    	   <div class="col-sm-3">
    	   <select class="form-control" name="sr" id="sr" required autofocus>
    	            <option value="1">1</option>
    	            <option value="2">2</option>
    	            <option value="3">3</option>
    	            <option value="4">4</option>
    	            <option value="5">5</option>
    	            <option value="6">6</option>
    	            <option value="7">7</option>
    	            <option value="8">8</option>
    	            <option value="8">9</option>
    	            <option value="10">10</option>
    	   </select>
    	        </div>
    	    <div class="col-sm-2"><button class="btn btn-success" onclick="exportStudent()">Enviar</button></div>
    	    <div class="col-sm-1"></div>
    	</div>
    </div>
    <div class="shnot"></div>
    <script>
        
        function exportStudent()
        {
            var v=$('#code').find(":selected").val();
            var v2=$('#sr').find(":selected").val();
            if(v!='' && v2!='')
            {
                $.post("<?php echo base_url();?>AdminBack/addBatch/",
                  {
                  v:v,
                  v2:v2
                  },
                  function(data, status){
                     if(status=='success')
                     {
                     
                         var d=JSON.parse(data);
                         if(d.stat==1)
                         {
                            $.notify({icon: 'la la-bell',title: 'Success',message: d.msg,},{type: 'success',placement: {from: 'bottom',align: 'right'},time: 1000,});
                            window.location.href="<?php echo base_url()?>Admin/pBatch";
                         }else
                         {
                         $.notify({icon: 'la la-bell',title: 'Error',message: d.msg,},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});
                         }
                     }else
                     {
                     $.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});
                     }
                  });
                
            }
        }
    </script>