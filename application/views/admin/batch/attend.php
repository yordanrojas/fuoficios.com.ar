<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/cbatchView/<?php echo $id;?>">Atrás</a>
<?php
$ids=array();
$attend=array();
  foreach($batch->result() as $row)
  {
      $attend=json_decode($row->attend);
  }
  
?>
<style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: inherit;
    left: inherit;
}
</style>
<h4 class="page-title">Asistencia</h4>
<form method="POST" action="<?php echo base_url();?>AdminBack/attend/<?php echo $id;?>">
    	    <div class="table-responsive">
              <table class="table table-hover">
                  <thead>
                      <tr>
                      <th>name</th>
                      <?php
                      $att=array();
                      foreach($attend as $row=>$key)
                      {
                         echo "<th>".date('d/m/Y',strtotime($row))."</th>";
                         $att[]=$row;
                      }
                      ?>
                      <th>Dias</th>
                      <th>Porcentaje</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach($s->result() as $row)
                      {
                          $ids[]=$row->id;
                          echo "<tr>";
                          echo "<td style='width:200px;'>".$row->name.'&nbsp;'.$row->lname."</td>";
                          $data=json_decode($row->data);
                          foreach($data as $k=>$r)
                          {
                              ?><td class='text-center' style='width:200px;'><input type='checkbox' name='<?php echo 'id'.$row->id.'-'.$k;?>' value='1' <?php if($r==1){ echo 'checked';}?>></td><?php
                          }
                          echo "<td style='width:200px;'>".$row->pday."/".$row->tday."</td>";
                          echo "<td style='width:200px;'>".intval($row->total)."</td>";
                          echo "</tr>";
                      }
                      ?>
                  </tbody>
              </table>
            </div>
            <input type="hidden" name="ids" value='<?php echo json_encode($ids);?>'></input>
            <input type="hidden" name="dates" value='<?php echo json_encode($att);?>'></input>
            <input type="submit" value="Enviar" class="btn btn-success"></input>
            
</form>
