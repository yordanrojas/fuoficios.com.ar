<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/Cbatch">Atrás</a>
<h4 class="page-title">Grupos</h4>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Datos</div>
        </div>
    	<div class="card-body row">
    	    <?php
    	    $id='';
    	    $data='';
    	    foreach($batch->result() as $row)
    	    {
    	        $id=$row->id;
    	        $data=json_decode($row->data);
    	        ?>
    	        <input type="hidden" name="id" value="<?php echo $id;?>"/>
    	        <input type="hidden" name="yr" value="<?php echo $row->yr;?>"/>
    	        <input type="hidden" name="sr" value="<?php echo $row->sr;?>"/>
    	        <input type="hidden" name="code" value="<?php echo $row->code;?>"/>
    	        <input type="hidden" name="frm" value="<?php echo date('d/m/Y',strtotime($data->start));?>"/>
    	        <input type="hidden" name="to" value="<?php echo date('d/m/Y',strtotime($data->start));?>"/>
    	        <input type="hidden" name="data" value='<?php echo json_encode($data);?>'/>
    	        <input type="hidden" name="dt" value='<?php echo json_encode($data->daytime);?>'/>
    	    <div class="col-sm-12">
    	        Curso:-<?php echo $row->title;?>
    	    </div>
    	    <div class="col-sm-6">
    	        Grupo:-<?php echo $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr));?>
    	        </div>
    	        <div class="col-sm-6">
    	        Estudiantes:-<?php echo $row->count;?>
    	        </div>
    	        <div class="col-sm-12">
    	        Enlace de grupo de whatsApp:- <a href='<?php echo $row->link;?>'>Enlace</a>
    	        </div>
    	        <?php
    	        $room=json_decode($row->room);
    	        ?>
    	        <div class="col-sm-6">
    	            Salón de clases:- <?php echo $room->croom;?>
    	            </div>
    	           <div class="col-sm-6">
    	            Laboratorio practico:- <?php echo $room->proom;?>
    	            </div>
    	       <div class="col-sm-6">
    	            Profesor:- <?php echo $row->name.'&nbsp;'.$row->lname;;?> (<?php echo $row->dsg;?>)&nbsp;<a href="<?php echo base_url();?>Admin/tattend/<?php echo $id;?>">Asistencia</a>
    	            </div>
    	    
    		<div class="col-sm-12">
    		   Desde:-<?php echo date('d/m/Y',strtotime($row->dfrm));?><br>
    		   A:-<?php echo date('d/m/Y',strtotime($row->dto));?>
    		</div>
    		<?php }?>
    		<div class="col-sm-12">
    		   <?php
    		   foreach($data->daytime as $row)
    		   {
    		       switch($row->day)
                    {
                        case 1:
                        echo "Lunes:- ";
                        break;
                        case 2:
                        echo "Martes:- ";
                        break;
                        case 3:
                        echo "Miércoles:- ";
                        break;
                        case 4:
                        echo "Jueves:- ";
                        break;
                        case 5:
                        echo "Viernes:- ";
                        break;
                        case 6:
                        echo "Sábado:- ";
                        break;
                    }
    		       echo $row->from.'-'.$row->to.'<br>';
    		   }
    		   ?>
    		</div>
    	</div>
    	<div class="card-action">
    	    <button class="btn btn-success" onclick="window.location.href='<?php echo base_url();?>Admin/attend/<?php echo $id;?>'">Asistencia</button>&nbsp;&nbsp;<button class="btn btn-success" onclick="window.location.href='<?php echo base_url();?>Admin/bstudents/<?php echo $id;?>'">Estudiantes</button>&nbsp;&nbsp;<button class="btn btn-success" onclick="window.location.href='<?php echo base_url();?>AdminBack/exportB/<?php echo $id;?>'">Exportar</button>
    	</div>
    </div>
