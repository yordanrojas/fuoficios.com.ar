<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/pbatch">Atrás</a>
<h4 class="page-title">Grupo pendiente</h4>



<form method="post" action="<?php echo base_url();?>AdminBack/assignBatch">
    <div class="card">
        <div class="card-header">
            <div class="card-title">Detalles</div>
        </div>
    	<div class="card-body row">
    	    <?php
    	    $id='';
    	    $data='';
    	    foreach($batch->result() as $row)
    	    {
    	        $id=$row->id;
    	        $data=json_decode($row->data);
    	        ?>
    	        <input type="hidden" name="id" value="<?php echo $id;?>"/>
    	        <input type="hidden" name="yr" value="<?php echo $row->yr;?>"/>
    	        <input type="hidden" name="sr" value="<?php echo $row->sr;?>"/>
    	        <input type="hidden" name="code" value="<?php echo $row->code;?>"/>
    	        <input type="hidden" name="frm" value="<?php echo $data->start;?>"/>
    	        <input type="hidden" name="to" value="<?php echo $data->end;?>"/>
    	        <input type="hidden" name="data" value='<?php echo json_encode($data);?>'/>
    	        <input type="hidden" name="dt" value='<?php echo json_encode($data->daytime);?>'/>
    	    <div class="col-sm-12">
    	        Curso:-<?php echo $row->title;?>
    	    </div>
    	    <div class="col-sm-6">
    	        Grupo:-<?php echo $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr));?>
    	        </div>
    	        <div class="col-sm-6">
    	        Estudiantes:-<?php echo $row->count;?>
    	        </div>
    	    <?php }?>
    	    <div class="form-group col-sm-12">
    			<label for="mob">Enlace de grupo de chat</label>
    			<input type="text" class="form-control" name="link" required >
    		</div>
    	    <div class="form-group col-sm-12">
    			<label for="mob">Aula</label>
    			<select class="form-control" name="croom" required >
    			    <option value="Aula 1">Aula 1</option>
    			    <option value="Aula 2">Aula 2</option>
    			    <option value="Aula 3">Aula 3</option>
    			    <option value="Aula 4">Aula 4</option>
    			    <option value="Aula 5">Aula 5</option>
    			</select>
    		</div>
    		<div class="form-group col-sm-12">
    			<label for="mob">Tallere</label>
    			<select class="form-control" name="proom" required >
    			    <option value="Tallere 1">Tallere 1</option>
    			    <option value="Tallere 2">Tallere 2</option>
    			    <option value="Tallere 3">Tallere 3</option>
    			    <option value="Tallere 4">Tallere 4</option>
    			</select>
    		</div>
    		<div class="form-group col-sm-12">
    			<label for="mob">Profesor</label>
    			<select class="form-control" name="teacher" required >
    			    <option></option>
    			    <?php
    			    foreach($teacher->result() as $row)
    			    {
    			        echo '<option value="'.$row->id.'">'.$row->name.'&nbsp;'.$row->lname.'('.$row->dsg.')</option>';
    			    }
    			    ?>
    			</select>
    		</div>
    		<div class="col-sm-12">
    		   Desde:-<?php echo date('d/m/Y',strtotime($data->start));?><br>
    		   A:-<?php echo date('d/m/Y',strtotime($data->end));?>
    		</div>
    		<div class="col-sm-12">
    		   <?php
    		   foreach($data->daytime as $row)
    		   {
    		       switch($row->day)
                    {
                        case 1:
                        echo "Lunes:- ";
                        break;
                        case 2:
                        echo "Martes:- ";
                        break;
                        case 3:
                        echo "Miércoles:- ";
                        break;
                        case 4:
                        echo "Jueves:- ";
                        break;
                        case 5:
                        echo "Viernes:- ";
                        break;
                        case 6:
                        echo "Sábado:- ";
                        break;
                    }
    		       echo $row->from.'-'.$row->to.'<br>';
    		   }
    		   ?>
    		</div>
    	</div>
    	
    	<div class="card-action">
    	    <button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>

<div class="card">
        <div class="card-header">
            <div class="card-title">Studiantes</div>
        </div>
    	<div class="card-body">
    	    <table id="example" class="table">
    	        <thead>
    	            <tr>
    	                <th>Tu Nombre</th>
                        <th>Correo</th>
                        <th>telefono</th>
    	            </tr>
    	        </thead>
    	        <tbody>
    	            <?php
    	            foreach($students->result() as $s)
    	            {
    	                ?>
    	                <tr>
    	                    <td><?php echo $s->name.' '.$s->lname;?></td>
    	                    <td><?php echo $s->email;?></td>
    	                    <td><?php echo $s->mob;?></td>
    	                </tr>
    	                <?php
    	            }
    	            ?>
    	        </tbody>
    	    </table>
    	</div>
</div>
    <script>
        
        function conAccept(i)
                {
                    var check = confirm("Are you sure?");
                    if(check== true)
                    {
                        window.location.href='<?php echo base_url()."AdminBack/enrollAccept/";?>'+i;
                    }
                }
    </script>