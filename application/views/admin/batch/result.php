        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/addResult/<?php echo $id;?>">Añadir</a>
        <h4 class="page-title">Hoja de papel</h4>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Sr</th>
                        <th>Examen</th>
                        <th>Marcas</th>
                        <th style="width:70px;">Borrar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($result->num_rows()>0)
                    {
                        $i=1;
                        foreach($result->result() as $row)
                        {
                            ?>
                            <tr>
                                <td onclick="window.location.href='<?php echo base_url()."Admin/viewResult/".$id."/".$row->id;?>'"><?php echo $i;?></td>
                                 <td onclick="window.location.href='<?php echo base_url()."Admin/viewResult/".$id."/".$row->id;?>'"><?php echo $row->title;?></td>
                                 <td onclick="window.location.href='<?php echo base_url()."Admin/viewResult/".$id."/".$row->id;?>'"><?php echo $row->marks;?></td>
                                 <td class="text-center" style="font-size:1.1rem;"  onclick="conDelete(<?php echo $row->id;?>)"><span class="la la-bitbucket"></span></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
            <div class="modal" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Esta seguro?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="conDelete2()">Aceptar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
            <script>
            var curr=0;
            function conDelete(i)
                {
                    $('#myModal').modal('toggle');
                    curr=i;
                }
                function conDelete2()
                {
                        window.location.href='<?php echo base_url()."Admin/deleteResult/".$id."/";?>'+curr;
                }
            </script>