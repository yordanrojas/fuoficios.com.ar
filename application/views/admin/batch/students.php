        <style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: inherit;
    left: inherit;
}
</style>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/cbatchView/<?php echo $id;?>">Atrás</a>
        <h4 class="page-title">Estudiantes</h4>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>Herramienta</th>
                        <th>Hoja de papel</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($students->num_rows()>0)
                    {
                        $i=1;
                        foreach($students->result() as $row)
                        {
                            ?>
                            <tr>
                                <td><?php echo $row->id;?></td>
                                 <td><?php echo $row->name.'&nbsp;'.$row->lname;?></td>
                                 <td><?php echo $row->email;?></td>
                                 <td><?php echo $row->mob;?></td>
                                 <td style="text-align:center;"><input type="checkbox" value="1" onclick="checkTool(this,<?php echo $row->id;?>)" <?php if($row->stat==1){ echo "checked";}?>></td>
                                 <td style="text-align:center;"><a href="<?php echo base_url();?>Admin/result/<?php echo $row->id;?>" class="btn btn-success" style="padding:5px;">Editar</a></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
            
            <script>
                function checkTool(t,id)
                {
                    var c=0;
                    if($(t).prop("checked") == true){
                        c=1;
                    }
                $.post("<?php echo base_url();?>Admin/tool/",
                  {
                    id: id,
                    c: c
                  },
                  function(data, status){
                  });
                }
            </script>