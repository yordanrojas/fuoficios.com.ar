<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/cbatchView/<?php echo $id;?>">Atrás</a>
<?php
$attend=array();
$name='';
$yr='';
$sr='';
$code='';
  foreach($batch->result() as $row)
  {
      $attend=json_decode($row->attend);
  }
  
    foreach($s->result() as $row)
  {
      $name=$row->name;
      $sr=$row->sr;
      $yr=$row->yr;
      $code=$row->code;
  }
  
?>
<style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: inherit;
    left: inherit;
}
</style>
<h4 class="page-title">Asistencia de <?php echo $name;?> (<?php echo $code.sprintf("%02d",$sr).date('y',strtotime('01-01-'.$yr));?>)</h4>
<form method="POST" action="<?php echo base_url();?>AdminBack/tattend/<?php echo $id;?>">
    <input type="hidden" name="dates" value='<?php echo json_encode($attend);?>'></input>
    	    <div class="table-responsive">
              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Date</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach($attend as $key=>$row)
                      {
                      ?><tr><td><?php echo date('d/m/Y',strtotime($key));?></td><td><input type='checkbox' value='1' name="d<?php echo $key?>" class='form-control' <?php if($row==1){ echo "checked";}?>/></td></tr>
                      <?php
                      }
                      ?>
                  </tbody>
              </table>
            </div>
            <input type="submit" value="Enviar" class="btn btn-success"></input>
</form>