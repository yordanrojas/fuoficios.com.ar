        <h4 class="page-title">Grupos</h4>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Grupo</th>
                        <th>Estudiantes</th>
                        <th style="width:70px;">Ver</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($batch->num_rows()>0)
                    {
                        foreach($batch->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr));?></td>
                                 <td><?php echo $row->count;?></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/cbatchView/".$row->id;?>'"><span class="la las la-pencil-square-o"></span></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>