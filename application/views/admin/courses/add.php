<?php 
$option='';
if(!empty($teachers))
{
    foreach($teachers->result() as $row)
    {
        $option.="<option value='".json_encode($row)."'>".$row->name."(".$row->dsg.")</option>";
    }
}
?>
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/courses">Atrás</a>
<h4 class="page-title">Añadir curso</h4>
<form method="post" action="<?php echo base_url()?>AdminBack/addCourses/" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Propuestas Académicas</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="title">Título</label>
    			<input type="text" class="form-control" id="title" name="title" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="stitle">Subtítulo</label>
    			<input type="text" class="form-control" id="stitle" name="stitle" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="dsc">Descripción</label>
    			<textarea class="form-control" id="dsc" name="dsc" rows="5" required autofocus></textarea>
    		</div>
    		<div class="row">
    		        <div class="col-sm-4">
    		            <div class="form-group">
    		                <label for="cat">Categoría</label>
    		                <select class="form-control" name="cat" id="cat" required autofocus>
    		                    <option value="Curso">Curso</option>
    		                    <option value="Capacitacion">Capacitacion</option>
    		                    <option value="Workshop">Workshop</option>
    		                    <option value="Diplomatura">Diplomatura</option>
    		                    <option value="Seminario">Seminario</option>
    		                    <option value="Eventos">Eventos</option>
    		                </select>
    		            </div>
    		        </div>
    		        <div class="col-sm-4">
    		            <div class="form-group">
    		                <label for="type">Modalidad </label>
    		                <select class="form-control" name="type" id="type" required autofocus>
    		                    <option value="Semipresencial">Semipresencial</option>
    		                    <option value="Presencial">Presencial</option>
    		                    <option value="Online">Online</option>
    		                </select>
    		            </div>
    		        </div>
    		        <div class="col-sm-4">
    		            <div class="form-group">
                			<label for="stitle">Área</label>
                			<input type="text" class="form-control" id="area" name="area" required autofocus>
                		</div>
    		        </div>
    		</div>
    		<div class="row">
    		    <div class="col-sm-6">
    		        <div class="form-group">
    		            <label for="start">Inicio-Final</label>
    		            <div class="row">
    		                <div class="col-sm-6"><input type="date" class="form-control" id="start" name="start"required autofocus></div>
    		                <div class="col-sm-6"><input type="date" class="form-control" id="end" name="end" required autofocus></div>
    		            </div>
    		        </div>
    		    </div>
    		    <div class="col-sm-6">
    		        <div class="form-group">
    		            <label for="dur">Duración</label>
    		            <div class="row">
    		                <div class="col-sm-6"><input type="number" class="form-control" id="dur" name="dur" required autofocus></div>
    		                <div class="col-sm-6">
    		                    <select  class="form-control" id="durin" name="durin" required autofocus>
    		                        <option value="Horas">Horas</option>
    		                        <option value="Encuentros">Encuentros</option>
    		                        <option value="Meses">Meses</option>
    		                    </select>
    		                </div>
    		            </div>
    		        </div>
    		    </div>
			</div>
    		<div class="form-group">
    			<label>Días y hora</label>
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="1" name="day[]">
								<span class="form-check-sign">Lunes</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days1" name="days1"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye1" name="daye1"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="2" name="day[]">
								<span class="form-check-sign">Martes</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days2" name="days2"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye2" name="daye2"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="3" name="day[]">
								<span class="form-check-sign">Miércoles</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days3" name="days3"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye3" name="daye3"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="4" name="day[]">
								<span class="form-check-sign">Jueves</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days4" name="days4"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye4" name="daye4"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="5" name="day[]">
								<span class="form-check-sign">Viernes</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days5" name="days5"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye5" name="daye5"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="6" name="day[]">
								<span class="form-check-sign">Sábado</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days6" name="days6"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye6" name="daye6"></div>
    			</div>
    		</div>
    		<div class="form-group" id="elig">
    			<label for="img">Dirigido <a class="btn btn-success ml-3" style="padding:3px;padding-left:8px;padding-right:8px;" href="#elig" onclick="addElig()">+</a></label>
    			<input type="text" class="form-control" id="eli" name="elig[]" >
    		</div>
    		<div class="form-group" id="cont">
    			<label for="img">Contenidos <a class="btn btn-success ml-3" style="padding:3px;padding-left:8px;padding-right:8px;" href="#elig" onclick="addcont()">+</a></label>
    			<input type="text" class="form-control" id="con" name="cont[]">
    		</div>
    		<div class="form-group" id="obj">
    			<label for="img">Objetivo <a class="btn btn-success ml-3" style="padding:3px;padding-left:8px;padding-right:8px;" href="#obj" onclick="addobj()">+</a></label>
    			<input type="text" class="form-control" id="ob" name="obj[]">
    		</div>
    		<div class="form-group">
    			<label for="meth">Metodología</label>
    			<textarea class="form-control" id="meth" name="meth" rows="3"></textarea>
    		</div>
    		<div class="form-group">
    			<label for="ben">Beneficios</label>
    			<textarea class="form-control" id="ben" name="ben" rows="3"></textarea>
    		</div>
    		<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen del curso</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img1" required autofocus>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen de contenido</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img2">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen objetiva</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img3">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen Metodológica</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img4">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen de beneficios</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img5">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="pat1">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="pat2">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="pat3">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="pat4">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="pat5">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Enlace</label>
				<div class="col-md-9 p-0">
					<input type="text" class="form-control" name="link">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]" required autofocus>
					    <option value="0">----</option>
					    <?php echo $option;?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]">
					    <option value="0">----</option>
					    <?php echo $option;?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]">
					    <option value="0">----</option>
					    <?php echo $option;?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]">
					    <option value="0">----</option>
					    <?php echo $option;?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]">
					    <option value="0">----</option>
					    <?php echo $option;?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Ultima fecha para aplicar</label>
				<div class="col-md-3 p-0">
					<input type="date"  class="form-control" name="ddate" rquired>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Código del curso</label>
				<div class="col-md-3 p-0">
					<input type="text"  class="form-control" name="code" placeholder="CSF" required autofocus>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Posición</label>
				<div class="col-md-3 p-0">
					<input type="text"  class="form-control" name="code2" placeholder="3" required autofocus>
				</div>
			</div>

			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Precio</label>
				<div class="col-md-3 p-0">
					<input type="number"  class="form-control" name="price" required autofocus>
				</div>
			</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>

<script>
    
     function fileValidation(){
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>
<script>
    function addElig()
    {
        $('#elig').append('<input type="text" class="form-control" name="elig[]" placeholder="Nombre" style="margin-top:5px;">');
    }
    function addcont()
    {
        $('#cont').append('<input type="text" class="form-control" name="cont[]" placeholder="Nombre" style="margin-top:5px;">');
    }
    function addobj()
    {
        $('#obj').append('<input type="text" class="form-control" name="obj[]" placeholder="Nombre" style="margin-top:5px;">');
    }
</script>