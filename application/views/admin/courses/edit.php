<?php
if($data->num_rows()>0)
{
    $id='';
$title='';
$stitle='';
$des='';
$cat='';
$type='';
$d='';
$img='';
$date='';
$code='';
$code2='';
$price='';
$area='';
foreach($data->result() as $row)
{
    $id=$row->id;
    $title=$row->title;
    $stitle=$row->stitle;
    $des=$row->des;
    $cat=$row->cat;
    $type=$row->type;
    $d=$row->data;
    $img=$row->imgs;
    $date=$row->date;
    $code=$row->code;
    $code2=$row->code2;
    $price=$row->price;
    $area=$row->area;
}
$img=json_decode($img);
$d=json_decode($d);
$teach=$d->teacher;
$option=array();
if(!empty($teachers))
{
    for($i=0;$i<5;$i++)
    {
        $tid=0;
        $opt='';
        if(!empty($teach[$i]))
        {
            $temp=json_decode($teach[$i]);
            $tid=$temp->id;
        }
    foreach($teachers->result() as $row)
    {
        $select='';
        if($row->id==$tid)
        {
            $select='selected';
        }
        $opt.="<option value='".json_encode($row)."' ".$select.">".$row->name."(".$row->dsg.")</option>";
    }
    $option[]=$opt;
    }
}
?>
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/courses">Atrás</a>
<h4 class="page-title">Editar curso</h4>
<form method="post" action="<?php echo base_url()?>AdminBack/updateCourses/<?php echo $id;?>" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Propuestas Académicas</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="title">Título</label>
    			<input type="text" class="form-control" id="title" name="title" value="<?php echo $title;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="stitle">Subtítulo</label>
    			<input type="text" class="form-control" id="stitle" name="stitle" value="<?php echo $stitle;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="dsc">Descripción</label>
    			<textarea class="form-control" id="dsc" name="dsc" rows="5" required autofocus><?php echo $des;?></textarea>
    		</div>
    		<div class="row">
    		        <div class="col-sm-4">
    		            <div class="form-group">
    		                <label for="cat">Categoría</label>
    		                <select class="form-control" name="cat" id="cat" required autofocus>
    		                    <option value="Curso" <?php if($cat=="Curso"){ echo "selected";} ?>>Curso</option>
    		                    <option value="Capacitacion" <?php if($cat=="Capacitacion"){ echo "selected";} ?>>Capacitacion</option>
    		                    <option value="Workshop" <?php if($cat=="Workshop"){ echo "selected";} ?>>Workshop</option>
    		                    <option value="Diplomatura" <?php if($cat=="Diplomatura"){ echo "selected";} ?>>Diplomatura</option>
    		                    <option value="Seminario" <?php if($cat=="Seminario"){ echo "selected";} ?>>Seminario</option>
    		                    <option value="Eventos" <?php if($cat=="Eventos"){ echo "selected";} ?>>Eventos</option>
    		                </select>
    		            </div>
    		        </div>
    		        <div class="col-sm-4">
    		            <div class="form-group">
    		                <label for="type">Modalidad </label>
    		                <select class="form-control" name="type" id="type" required autofocus>
    		                    <option value="Semipresencial" <?php if($type=="Semipresencial"){ echo "selected";} ?>>Semipresencial</option>
    		                    <option value="Presencial" <?php if($type=="Presencial"){ echo "selected";} ?>>Presencial</option>
    		                    <option value="Online" <?php if($type=="Online"){ echo "selected";} ?>>Online</option>
    		                </select>
    		            </div>
    		        </div>
    		        <div class="col-sm-4">
    		            <div class="form-group">
                			<label for="stitle">Área</label>
                			<input type="text" class="form-control" id="area" name="area" value="<?php echo $area;?>" required autofocus>
                		</div>
    		        </div>
    		</div>
    		<div class="row">
    		    <div class="col-sm-6">
    		        <div class="form-group">
    		            <label for="start">Inicio-Final</label>
    		            <div class="row">
    		                <div class="col-sm-6"><input type="date" class="form-control" id="start" name="start"  value="<?php echo $d->start;?>" required autofocus></div>
    		                <div class="col-sm-6"><input type="date" class="form-control" id="end" name="end"   value="<?php echo $d->end;?>" required autofocus></div>
    		            </div>
    		        </div>
    		    </div>
    		    <div class="col-sm-6">
    		        <div class="form-group">
    		            <label for="dur">Duración</label>
    		            <div class="row">
    		                <div class="col-sm-6"><input type="number" class="form-control" id="dur" name="dur" value="<?php echo $d->dur;?>" required autofocus></div>
    		                <div class="col-sm-6">
    		                    <select  class="form-control" id="durin" name="durin" required autofocus>
    		                        <option value="Horas" <?php if($d->durin=="Horas"){ echo "selected";}?>>Horas</option>
    		                        <option value="Encuentros" <?php if($d->durin=="Encuentros"){ echo "selected";}?>>Encuentros</option>
    		                        <option value="Meses" <?php if($d->durin=="Meses"){ echo "selected";}?>>Meses</option>
    		                    </select>
    		                </div>
    		            </div>
    		        </div>
    		    </div>
			</div>
    		<div class="form-group">
    			<label>Días y hora</label>
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="1" name="day[]"  id="day1">
								<span class="form-check-sign">Lunes</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days1" name="days1"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye1" name="daye1"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="2" name="day[]"  id="day2">
								<span class="form-check-sign">Martes</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days2" name="days2"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye2" name="daye2"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="3" name="day[]"  id="day3">
								<span class="form-check-sign">Miércoles</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days3" name="days3"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye3" name="daye3"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="4" name="day[]"  id="day4">
								<span class="form-check-sign">Jueves</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days4" name="days4"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye4" name="daye4"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="5" name="day[]"  id="day5">
								<span class="form-check-sign">Viernes</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days5" name="days5"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye5" name="daye5"></div>
    			</div>
    			
    			<div class="row">
    			    <div class="col-sm-6">
    			        <div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="6" name="day[]"  id="day6">
								<span class="form-check-sign">Sábado</span>
							</label>
						</div>
					</div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="days6" name="days6"></div>
    			    <div class="col-sm-3"><input type="time" class="form-control" id="daye6" name="daye6"></div>
    			</div>
    		</div>
    		<div class="form-group" id="elig">
    			<label for="img">Dirigido <a class="btn btn-success ml-3" style="padding:3px;padding-left:8px;padding-right:8px;" href="#elig" onclick="addElig()">+</a></label>
    			<?php
    			foreach($d->elig as $r)
    			{
    			    echo '<input type="text" class="form-control" name="elig[]" value="'.$r.'" style="margin-top:5px;">';
                }
    			?>
    			<input type="text" class="form-control" id="eli" name="elig[]" style="margin-top:5px;">
    		</div>
    		<div class="form-group" id="cont">
    			<label for="img">Contenidos <a class="btn btn-success ml-3" style="padding:3px;padding-left:8px;padding-right:8px;" href="#elig" onclick="addcont()">+</a></label>
    			<?php
    			foreach($d->cont as $r)
    			{
    			    echo '<input type="text" class="form-control" name="cont[]" value="'.$r.'" style="margin-top:5px;">';
                }
    			?>
    			<input type="text" class="form-control" id="con" name="cont[]" style="margin-top:5px;">
    		</div>
    		<div class="form-group" id="obj">
    			<label for="img">Objetivo <a class="btn btn-success ml-3" style="padding:3px;padding-left:8px;padding-right:8px;" href="#obj" onclick="addobj()">+</a></label>
    			<?php
    			foreach($d->obj as $r)
    			{
    			    echo '<input type="text" class="form-control" name="obj[]" value="'.$r.'" style="margin-top:5px;">';
                }
    			?>
    			<input type="text" class="form-control" id="ob" name="obj[]" style="margin-top:5px;">
    		</div>
    		<div class="form-group">
    			<label for="meth">Metodología</label>
    			<textarea class="form-control" id="meth" name="meth" rows="3"><?php echo $d->meth;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="ben">Beneficios</label>
    			<textarea class="form-control" id="ben" name="ben" rows="3"><?php echo $d->ben;?></textarea>
    		</div>
    		<?php
    		    $i=1;
    			foreach($img as $r)
    			{
    			    echo '<input type="hidden" class="form-control" name="eimg'.$i.'" value="'.$r.'">';
    			    $i++;
                }
                $i=1;
                $epat=array();
                foreach($d->pat as $r)
    			{
    			    $epat[$i]=$r;
    			    echo '<input type="hidden" class="form-control" name="epat'.$i.'" value="'.$r.'">';
    			    $i++;
                }
    			?>
    		<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen del curso</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img1"  >
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen de contenido</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img2">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen objetiva</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img3">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen Metodológica</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img4">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Imagen de beneficios</label>
				<div class="col-md-9 p-0">
					<input type="file" class="form-control-file input-full" name="img5">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0 pat1">
				    <?php
				    if(!empty($epat[1]))
				    {
				        ?>
				        <img src="<?php echo base_url().$epat[1];?>" style="height:150px;"/>
				        <button class="btn btn-danger" onclick="addPat(1)">Eliminar</button>
				        <?php
				    }else
				    {
				      ?>
				      <input type="file" class="form-control-file input-full" name="pat1">
				      <?php  
				    }
				    ?>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0 pat2">
				    <?php
				    if(!empty($epat[2]))
				    {
				        ?>
				        <img src="<?php echo base_url().$epat[2];?>" style="height:150px;"/>
				        <button class="btn btn-danger" onclick="addPat(2)">Eliminar</button>
				        <?php
				    }else
				    {
				      ?>
				      <input type="file" class="form-control-file input-full" name="pat2">
				      <?php  
				    }
				    ?>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0 pat3">
					<?php
				    if(!empty($epat[3]))
				    {
				        ?>
				        <img src="<?php echo base_url().$epat[3];?>" style="height:150px;"/>
				        <button class="btn btn-danger" onclick="addPat(3)">Eliminar</button>
				        <?php
				    }else
				    {
				      ?>
				      <input type="file" class="form-control-file input-full" name="pat3">
				      <?php  
				    }
				    ?>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0 pat4">
					<?php
				    if(!empty($epat[4]))
				    {
				        ?>
				        <img src="<?php echo base_url().$epat[4];?>" style="height:150px;"/>
				        <button class="btn btn-danger" onclick="addPat(4)">Eliminar</button>
				        <?php
				    }else
				    {
				      ?>
				      <input type="file" class="form-control-file input-full" name="pat4">
				      <?php  
				    }
				    ?>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Nuestros compañeros</label>
				<div class="col-md-9 p-0 pat5">
					<?php
				    if(!empty($epat[5]))
				    {
				        ?>
				        <img src="<?php echo base_url().$epat[5];?>" style="height:150px;"/>
				        <button class="btn btn-danger" onclick="addPat(5)">Eliminar</button>
				        <?php
				    }else
				    {
				      ?>
				      <input type="file" class="form-control-file input-full" name="pat5">
				      <?php  
				    }
				    ?>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Enlace</label>
				<div class="col-md-9 p-0">
					<input type="text" class="form-control" name="link" value="<?php echo $d->link;?>">
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]" required autofocus>
					    <option value="0">----</option>
					    <?php echo $option[0];?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]">
					    <option value="0">----</option>
					    <?php echo $option[1];?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]">
					    <option value="0">----</option>
					    <?php echo $option[2];?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]">
					    <option value="0">----</option>
					    <?php echo $option[3];?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Profesor</label>
				<div class="col-md-6 p-0">
					<select class="form-control input-full" name="teacher[]">
					    <option value="0">----</option>
					    <?php echo $option[4];?>
					</select>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Ultima fecha para aplicar</label>
				<div class="col-md-3 p-0">
					<input type="date"  class="form-control" name="ddate" value="<?php echo $date;?>" required>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Código del curso</label>
				<div class="col-md-3 p-0">
					<input type="text"  class="form-control" name="code" value="<?php echo $code;?>" placeholder="CSF" required autofocus>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Posición</label>
				<div class="col-md-3 p-0">
					<input type="text"  class="form-control" name="code2" value="<?php echo $code2;?>" placeholder="3" required autofocus>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="inlineinput" class="col-md-3 col-form-label">Precio</label>
				<div class="col-md-3 p-0">
					<input type="number"  class="form-control" name="price" value="<?php echo $price;?>" required autofocus>
				</div>
			</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>

<script>
    
     function fileValidation(){
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>
<script>
    function addElig()
    {
        $('#elig').append('<input type="text" class="form-control" name="elig[]" style="margin-top:5px;">');
    }
    function addcont()
    {
        $('#cont').append('<input type="text" class="form-control" name="cont[]" style="margin-top:5px;">');
    }
    function addobj()
    {
        $('#obj').append('<input type="text" class="form-control" name="obj[]" style="margin-top:5px;">');
    }
    function addPat(n)
    {
        $('.pat'+n).html('<input type="file" class="form-control-file input-full" name="pat'+n+'">');
        $('input[name=epat'+n+']').val('');
    }
<?php
    foreach($d->daytime as $r)
    {
        switch($r->day)
        {
            case 1:
            echo "$('#day1').prop('checked', true);";
            echo "$('#days1').val('".$r->from."');$('#daye1').val('".$r->to."');";
            break;
            case 2:
            echo "$('#day2').prop('checked', true);";
            echo "$('#days2').val('".$r->from."');$('#daye2').val('".$r->to."');";
            break;
            case 3:
            echo "$('#day3').prop('checked', true);";
            echo "$('#days3').val('".$r->from."');$('#daye3').val('".$r->to."');";
            break;
            case 4:
            echo "$('#day4').prop('checked', true);";
            echo "$('#days4').val('".$r->from."');$('#daye4').val('".$r->to."');";
            break;
            case 5:
            echo "$('#day5').prop('checked', true);";
            echo "$('#days5').val('".$r->from."');$('#daye5').val('".$r->to."');";
            break;
            case 6:
            echo "$('#day6').prop('checked', true);";
            echo "$('#days6').val('".$r->from."');$('#daye6').val('".$r->to."');";
            break;
        }
    }
}
echo "</script>";
?>