        <h4 class="page-title"><?php if($type==1){ echo "Pendiente";}
                                if($type==2){ echo "En espera";}
                                if($type==3){ echo "Confirmado";}
                                if($type==4){ echo "Fallido";}?></h4>
        <?php
        if($type==1 || $type==3 || $type==4){
        ?>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tu Nombre</th>
                        <th>Tu Apellido</th>
                        <th>Correo</th>
                        <th>Curso</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($enroll->num_rows()>0)
                    {
                        foreach($enroll->result() as $row)
                        {
                            ?>
                            <tr onclick="window.location.href='<?php if($type==1){ echo base_url()."Admin/viewPenroll/".$row->id;}
                                if($type==2){ echo base_url()."Admin/viewOenroll/".$row->id;}
                                if($type==3){ echo base_url()."Admin/viewCenroll/".$row->id;}
                                if($type==4){ echo base_url()."Admin/viewAenroll/".$row->id;}?>'">
                                <td><?php echo $row->sid;?></td>
                                 <td><?php echo $row->name;?></td>
                                 <td><?php echo $row->lname;?></td>
                                 <td><?php echo $row->email;?></td>
                                 <td><?php echo $row->title;?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <?php 
            
        }
                    if($type==2)
                    {
        ?>
        <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Curso</th>
                        <th>Fecha</th>
                        <th>Dia</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($enroll->num_rows()>0)
                    {
                        foreach($enroll->result() as $row)
                        {
                            ?>
                            <tr onclick="window.location.href='<?php if($type==1){ echo base_url()."Admin/viewPenroll/".$row->id;}
                                if($type==2){ echo base_url()."Admin/viewOenroll/".$row->id;}
                                if($type==3){ echo base_url()."Admin/viewCenroll/".$row->id;}
                                if($type==4){ echo base_url()."Admin/viewAenroll/".$row->id;}?>'">
                                <td><?php echo $row->sid;?></td>
                                 <td><?php echo $row->name;?></td>
                                 <td><?php echo $row->email;?></td>
                                 <td><?php echo $row->title;?></td>
                                 <td><?php echo date('d/m/Y',strtotime($row->date));?></td>
                                 <td><?php
                                 $date1=date_create($row->date);
                                $date2=date_create(date('Y-m-d'));
                                $diff=date_diff($date1,$date2);
                                echo $diff->format("%a");
                                 ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <?php
            
                    }
        
            ?>
            
            