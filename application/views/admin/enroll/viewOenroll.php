<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/penroll">Atrás</a>
<h4 class="page-title">Inscripción en Espera</h4>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Detalles</div>
        </div>
    	<div class="card-body row">
    	    <?php
    	    $id='';
    	    foreach($enroll->result() as $row)
    	    {
    	        $id=$row->id;
    	        ?>
    	        <div class="col-sm-12">ID: <?php echo $row->sid;?> &nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>Admin/viewStudent/<?php echo $row->sid;?>">Ver</a></div>
    	        <div class="col-sm-6">Nombre: <?php echo $row->name.' '.$row->lname;?></div>
    	        <div class="col-sm-6">Correo: <?php echo $row->email;?></div>
    	        <br>
    	        <div class="col-sm-12">Curso: <?php echo $row->title;?></div>
    	        <div class="col-sm-12">Precio: $<?php echo $row->price;?></div>
    	        <?php
    	        echo '<div class="col-sm-12">'.jsonToTable(json_decode($row->data)).'</div>';
    	    }
    	    ?>
    	</div>
    	<div class="card-action">
    		<a class="btn btn-success" onclick="conDelete()" href="#">Efectivo</a> &nbsp;&nbsp; <a class="btn btn-danger" href="<?php echo base_url()?>AdminBack/enrollReject/<?php echo $id;?>">Rechazar</a>
    	</div>
    </div>
                <div class="modal" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Esta seguro?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="conAccept(<?php echo $id;?>)">Aceptar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
    <script>
        function conDelete()
                {
                    $('#myModal').modal('toggle');
                }
        function conAccept(i)
                {
                   
                        window.location.href='<?php echo base_url()."AdminBack/enrollOnAccept/";?>'+i;
                }
    </script>
    <?php
    function jsonToTable ($data)
{
    $table = '
    <table class="json-table" width="100%">
    ';
    foreach ($data as $key => $value) {
        $table .= '
        <tr valign="top">
        ';
        if ( ! is_numeric($key)) {
            $table .= '
            <td>
                <strong>'.getKey($key) .':</strong>
            </td>
            <td>
            ';
        } else {
            $table .= '
            <td colspan="2">
            ';
        }
        if (is_object($value) || is_array($value)) {
            $table .= jsonToTable($value);
        } else {
            $table .= getValue($value);
        }
        $table .= '
            </td>
        </tr>
        ';
    }
    $table .= '
    </table>
    ';
    return $table;
}


function getKey($key)
{
    switch ($key) {
    case "type":
        return "Tipo";
        break;
    case "data":
        return "Dato";
        break;
    case "result":
        return "Resultado";
        break;
    case "view":
        return "Ver";
        break;
    case "options":
        return "Opciones";
        break;
    
    case "label":
        return "Descripción";
        break;
    
    case "payment":
        return "Pago";
        break;
    
    case "description":
        return "Descripción";
        break;
    
    case "operation":
        return "Operación";
        break;
    
    case "status":
        return "Estado";
        break;
    
    case "value":
        return "Valor";
        break;
    
    case "code":
        return "Código";
        break;
    
    case "message":
        return "Mensaje";
        break;
    
    case "currency":
        return "Tipo de moneda";
        break;
    
    case "created":
        return "Creado";
        break;
    
    case "updated":
        return "Actualizado";
        break;
    
    case "reference":
        return "Referencia";
        break;
    
    case "source":
        return "Origen";
        break;
    
    case "name":
    return "Nombre";
    break;
    case "number":
    return "Numero";
    break;
    case "installment":
    return "Cuotas";
    break;
    case "email":
    return "Correo";
    break;
    case "amount":
    return "Cantidad";
    break;
    case "count":
    return "Numero";
    break;
    default:
        return $key;
        
    }
}
function getValue($key)
{
    switch ($key) {
    case "checkout":
        return "En línea";
        break;
    default:
        return $key;
        
    }
}
    ?>