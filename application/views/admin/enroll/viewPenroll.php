<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/penroll">Atrás</a>
<h4 class="page-title">Inscripción Pendiente</h4>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Detalles</div>
        </div>
    	<div class="card-body row">
    	    <?php
    	    $id='';
    	    foreach($enroll->result() as $row)
    	    {
    	        $id=$row->id;
    	        ?>
    	        <div class="col-sm-12">ID: <?php echo $row->sid;?> &nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>Admin/viewStudent/<?php echo $row->sid;?>">Ver</a></div>
    	        <div class="col-sm-6">Nombre: <?php echo $row->name.' '.$row->lname;?></div>
    	        <div class="col-sm-6">Correo: <?php echo $row->email;?></div>
    	        <br>
    	        <div class="col-sm-12">Curso: <?php echo $row->title;?></div>
    	        <div class="col-sm-12">Precio: $<?php echo $row->price;?></div>
    	        <?php
    	    }
    	    ?>
    	</div>
    	<div class="card-action">
    		<a class="btn btn-success" onclick="conDelete()" href="#">Efectivo</a> &nbsp;&nbsp; <a class="btn btn-danger" href="<?php echo base_url()?>AdminBack/enrollReject/<?php echo $id;?>">Rechazar</a>
    	</div>
    </div>
                <div class="modal" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Esta seguro?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="conAccept(<?php echo $id;?>)">Aceptar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
    <script>
        function conDelete()
                {
                    $('#myModal').modal('toggle');
                }
        function conAccept(i)
                {
                   
                        window.location.href='<?php echo base_url()."AdminBack/enrollAccept/";?>'+i;
                }
    </script>