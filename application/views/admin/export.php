<h4 class="page-title">Reporte de datos por estudiantes</h4>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Datos</div>
        </div>
    	<div class="card-body row">
    	    <div class="col-sm-1"></div>
    	    <div class="col-sm-2">Fecha:</div>
    	    <div class="col-sm-3">
    	        <input type="date" class="form-control" name="studentfrm" id="stdfrm">
    	        </div>
    	   <div class="col-sm-3">
    	        <input type="date" class="form-control" name="studentto" id="stdto">
    	        </div>
    	    <div class="col-sm-2"><button class="btn btn-success" onclick="exportStudent()">Enviar</button></div>
    	    <div class="col-sm-1"></div>
    	</div>
    </div>
    <script>
        
        function exportStudent()
        {
            var v=$("#stdfrm").val();
            var v2=$("#stdto").val();
            if(v!='' && v2!='')
            {
                var d1=new Date(v);
                var d2=new Date(v2);
                if(d1.getTime() <= d2.getTime())
                {
                    window.location.href='<?php echo base_url();?>AdminBack/export/'+v+'/'+v2;
                }
                
            }
        }
    </script>