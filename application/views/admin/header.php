<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Dashboard</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<link rel="stylesheet" href="<?php echo base_url();?>backend/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
	<link rel="stylesheet" href="<?php echo base_url();?>backend/assets/css/ready.css">
	<link rel="stylesheet" href="<?php echo base_url();?>backend/assets/css/demo.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
	<script src="<?php echo base_url();?>backend/assets/js/core/jquery.3.2.1.min.js"></script>
	<style>
	.main-panel {
	    background: #ffffff;
	    
	}
	    .page-item.active .page-link {
	        color: #fff;
	        background-color: #31dc89;
	        border-color: #31dc89;
	    }
	    .sidebar .nav .nav-item.active a:before {
	        background: #f18c6f;
	        
	    }
	    .sidebar .nav .nav-item.active a i,.sidebar .nav .nav-item.active a {
	        color: #f18c6f;
	        
	    }
	    .sidebar .nav .nav-item a:hover:before {
	        background: #f18c6f;
	        
	    }
	    .sidebar .nav .nav-item a:hover
	    {
	        color: #f18c6f;
	    }
	</style>
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<div class="logo-header">
				<a href="index.html" class="logo">
					Tablero
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<button class="topbar-toggler more"><i class="la la-ellipsis-v"></i></button>
			</div>
			<nav class="navbar navbar-header navbar-expand-lg">
				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="<?php echo base_url();?>Admin/inbox" id="navbarDropdown">
								<i class="la la-bell"></i>
							</a>
						</li>
						<li class="nav-item dropdown">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false"> <img src="<?php if(!empty($this->session->userData('adminprofile'))){ echo base_url().$this->session->userData('adminprofile');}else{ echo base_url().'backend/assets/img/profile.jpg';}?>" alt="user-img" width="36" class="img-circle"><span ><?php echo ucfirst($this->session->userData('adminuser'));?></span></span> </a>
							<ul class="dropdown-menu dropdown-user">
								<li>
									<div class="user-box">
										<div class="u-img"><img src="<?php if(!empty($this->session->userData('adminprofile'))){ echo base_url().$this->session->userData('adminprofile');}else{ echo base_url().'backend/assets/img/profile.jpg';}?>" alt="user"></div>
										<div class="u-text">
											<h4><?php echo ucfirst($this->session->userData('adminuser'));?></h4>
										</div>
									</li>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="<?php echo base_url()?>Admin/myProfile"><i class="ti-user"></i> Mi perfil</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="<?php echo base_url()?>Admin/logout"><i class="fa fa-power-off"></i> Cerrar sesión</a>
								</ul>
								<!-- /.dropdown-user -->
							</li>
						</ul>
					</div>
				</nav>
			</div>
			<?php
			if($this->session->userData('adminrole')==3)
			{
			?>
			<div class="sidebar">
				<div class="scrollbar-inner sidebar-wrapper">
					<ul class="nav">
						<li class="nav-item <?php if($nav==1){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/">
								<i class="la la-dashboard"></i>
								<p>Sistema de Control Administrativo</p>
							</a>
						</li>
						<li class="nav-item dropdown <?php if($nav==2){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-file"></i><p class="w-100">Diapositivas</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/slider/1" <?php if($snav==21){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Control deslizante principal</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/slider/2" <?php if($snav==22){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Control deslizante de alianzas</a>
                            </div>
                        </li>
						<li class="nav-item dropdown <?php if($nav==3){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-file"></i><p class="w-100">Páginas</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/page/1" <?php if($snav==31){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Inicio</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/page/2" <?php if($snav==32){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Institución</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/page/3" <?php if($snav==33){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Contacto</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/post" <?php if($snav==34){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Noticias y Eventos</a>
                            </div>
                        </li>
                        <li class="nav-item <?php if($nav==4){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/courses">
								<i class="la la-dashboard"></i>
								<p>Cursos</p>
							</a>
						</li>

						<li class="nav-item dropdown <?php if($nav==5){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-user"></i><p class="w-100">Profesores</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/tview" <?php if($snav==51){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Tabla de tiempos</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/teachers" <?php if($snav==52){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Profesores</a>
                              
                            </div>
                        </li>
						<li class="nav-item dropdown <?php if($nav==6){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-user"></i><p class="w-100">Estudiantes</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/students" <?php if($snav==61){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Todos</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/student" <?php if($snav==62){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Actual</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/pDoc" <?php if($snav==63){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Documentos pendientes</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown <?php if($nav==7){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-file"></i><p class="w-100">Inscripción</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/penroll" <?php if($snav==71){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Pendiente por pagar</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/oenroll" <?php if($snav==72){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>En espera por mobbex</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/cenroll" <?php if($snav==73){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Pago Confirmado</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/aenroll" <?php if($snav==74){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Pago Fallido</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown <?php if($nav==8){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-group"></i><p class="w-100">Grupos</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/pbatch" <?php if($snav==81){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Pendiente</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/cbatch" <?php if($snav==82){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Actual</a>
                            </div>
                        </li>
                        <li class="nav-item <?php if($nav==9){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/export">
								<i class="la la-file-excel-o"></i>
								<p>Exportar datos</p>
							</a>
						</li>
						<li class="nav-item <?php if($nav==10){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/user">
								<i class="la la-users"></i>
								<p>Los usuarios</p>
							</a>
						</li>
						<li class="nav-item <?php if($nav==11){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/notice">
								<i class="la la-comment"></i>
								<p>Aviso</p>
							</a>
						</li>
						<li class="nav-item <?php if($nav==12){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/settings">
								<i class="la la-cog"></i>
								<p>Configuraciones</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<?php
			}
			?>
			
			<?php
			if($this->session->userData('adminrole')==1)
			{
			?>
			<div class="sidebar">
				<div class="scrollbar-inner sidebar-wrapper">
					<ul class="nav">
						<li class="nav-item <?php if($nav==1){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/">
								<i class="la la-dashboard"></i>
								<p>Sistema de Control Web</p>
							</a>
						</li>
						<li class="nav-item dropdown <?php if($nav==2){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-file"></i><p class="w-100">Diapositivas</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/slider/1" <?php if($snav==21){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Control deslizante principal</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/slider/2" <?php if($snav==22){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Control deslizante de alianzas</a>
                            </div>
                        </li>
						<li class="nav-item dropdown <?php if($nav==3){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-file"></i><p class="w-100">Páginas</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/page/1" <?php if($snav==31){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Inicio</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/page/2" <?php if($snav==32){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Institución</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/page/3" <?php if($snav==33){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Contacto</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/post" <?php if($snav==34){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Noticias y Eventos</a>
                            </div>
                        </li>
                        <li class="nav-item <?php if($nav==4){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/courses">
								<i class="la la-dashboard"></i>
								<p>Cursos</p>
							</a>
						</li>
						
					</ul>
				</div>
			</div>
			<?php
			}
			?>
			
			<?php
			if($this->session->userData('adminrole')==2)
			{
			?>
			<div class="sidebar">
				<div class="scrollbar-inner sidebar-wrapper">
					<ul class="nav">
						<li class="nav-item <?php if($nav==1){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/">
								<i class="la la-dashboard"></i>
								<p>Sistema de Control Inscripción</p>
							</a>
						</li>
						<li class="nav-item dropdown <?php if($nav==6){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-user"></i><p class="w-100">Estudiantes</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/students" <?php if($snav==61){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Todos</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/student" <?php if($snav==62){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Actual</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/pDoc" <?php if($snav==63){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Documentos pendientes</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown <?php if($nav==7){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-file"></i><p class="w-100">Inscripción</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/penroll" <?php if($snav==71){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Pendiente por pagar</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/oenroll" <?php if($snav==72){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>En espera por mobbex</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/cenroll" <?php if($snav==73){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Pago Confirmado</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/aenroll" <?php if($snav==74){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Pago Fallido</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown <?php if($nav==8){ echo 'active';}?>">
						    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-group"></i><p class="w-100">Grupos</p></a>
                            <div class="dropdown-menu w-100">
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/pbatch" <?php if($snav==81){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Pendiente</a>
                              <a class="dropdown-item" href="<?php echo base_url()?>Admin/cbatch" <?php if($snav==82){ echo 'style="color:#f18c6f;"';}else{ echo 'style="color:#83848a;"';}?>>Actual</a>
                            </div>
                        </li>
                        <li class="nav-item <?php if($nav==9){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/export">
								<i class="la la-file-excel-o"></i>
								<p>Exportar datos</p>
							</a>
						</li>
		
						<li class="nav-item <?php if($nav==11){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Admin/notice">
								<i class="la la-comment"></i>
								<p>Aviso</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<?php
			}
			?>
			<div class="main-panel">
				<div class="content">
					<div class="container-fluid">
					