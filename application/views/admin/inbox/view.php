 <?php
 $name='';
 $mob='';
 $email='';
 $course=array();
 $refer='';
 $msg='';
 foreach($inbox->result() as $row)
 {
     $name=$row->name;
     $mob=$row->mob;
     $email=$row->email;
     $course=json_decode($row->course);
     $refer=$row->refer;
     $msg=$row->msg;
 }
?>
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/inbox">Atrás</a>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Investigación</div>
        </div>
    	<div class="card-body">
    	   <div class="col-xs-12">Nombre:-<?php echo $name;?></div>
    	   <div class="col-xs-12">Teléfono:-<?php echo $mob;?></div>
    	   <div class="col-xs-12">Correo:-<?php echo $email;?></div>
    	   <div class="col-xs-12">Formación:-<?php if(!empty($course->title)){ echo $course->title;}?></div>
    	   <div class="col-xs-12">Refiere:-<?php echo $refer;?></div>
    	   <div class="col-xs-12">Mensaje:-<?php echo $msg;?></div>
    	</div>
    </div>
