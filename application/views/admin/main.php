<script>
window.onload = function () {
        CanvasJS.addCultureInfo("es", 
    {      
      decimalSeparator: ",",// Observe ToolTip Number Format
      digitGroupSeparator: ".", // Observe axisY labels                   
      shortMonths: ["Enero", "Feb", "Mar", "Abr", "Mayo", "Jun", "Jul", "Agosto", "Sep", "Oct", "Nov", "Dic"]
      
    });
    
    
//Better to construct options first and then pass it as a parameter
var options = {
	title: {
		text: "Numero de estudiantes por año"              
	},
	data: [              
	{
		// Change type to "doughnut", "line", "splineArea", etc.
		type: "column",
		dataPoints: [
        		    <?php
        foreach($d1->result() as $row)
        {
            $a=array();
            foreach($row as $value)
            {
               $a[]=$value; 
            }
            echo '{ label: "'.$a[0].'", y: '.$a[1].'},';
        }
        ?>
		]
	}
	]
};

$("#chartContainer").CanvasJSChart(options);

var options1 = {
	title: {
		text: "Numero de estudiantes por mes (<?php echo $yr;?>)"              
	},
	data: [              
	{
		// Change type to "doughnut", "line", "splineArea", etc.
		type: "column",
		dataPoints: [
        		    <?php
        foreach($d2->result() as $row)
        {
            $lable='';
            if($row->date==1)
            {
                $lable='Enero';
            }elseif($row->date==2)
            {
                $lable='Feb';
            }elseif($row->date==3)
            {
                $lable='Mar';
            }elseif($row->date==4)
            {
                $lable='Abr';
            }elseif($row->date==5)
            {
                $lable='Mayo';
            }elseif($row->date==6)
            {
                $lable='Jun';
            }elseif($row->date==7)
            {
                $lable='Jul';
            }elseif($row->date==8)
            {
                $lable='Agosto';
            }elseif($row->date==9)
            {
                $lable='Sep';
            }elseif($row->date==10)
            {
                $lable='Oct';
            }elseif($row->date==11)
            {
                $lable='Nov';
            }elseif($row->date==12)
            {
                $lable='Dic';
            }
            echo '{ label: "'.$lable.'", y: '.$row->c.'},';
        }
        ?>
		]
	}
	]
};
$("#chartContainer3").CanvasJSChart(options1);

var dataPoints = [];
 
var chart2 = new CanvasJS.Chart("chartContainer2", {
    culture:  "es",
	animationEnabled: true,
	theme: "light2",
	zoomEnabled: true,
	title: {
		text: "Ingresos por año"
	},
	axisY: {
		title: "",
		titleFontSize: 24,
		prefix: "$"
	},
	data: [{
		type: "line",
		yValueFormatString: "$#,##0.00",
		dataPoints: dataPoints
	}]
});
 
function addData(data) {
	var dps = data.price_usd;
	for (var i = 0; i < dps.length; i++) {
	    n=new Date(dps[i][0]);
	    console.log(n.getTime());
		dataPoints.push({
			x: new Date(n),
			y: dps[i][1]
		});
		console.log(new Date(dps[i][0]));
	}
	chart2.render();
}
 
$.getJSON("https://fuoficios.com.ar/AdminBack/chart2", addData);

<?php
$d4total=0;
foreach($d3->result() as $row)
{
    $d4total+=$row->c;
}
$dataPoints=array();

foreach($d3->result() as $row)
{
    $d4arr['label']=$row->code;
    $d4arr['total']=$d4total;
    $d4arr['s']=$row->c;
    $d4arr['y']=($row->c/$d4total)*100;
    $dataPoints[]=$d4arr;
}
?>

var chart4 = new CanvasJS.Chart("chartContainer4", {
	animationEnabled: true,
	exportEnabled: true,
	title:{
		text: "Promedio de curso por alumno [<?php echo $yr;?>]"
	},
	subtitles: [{
		text: ""
	}],
	data: [{
		type: "pie",
		showInLegend: "true",
		legendText: "{label}",
		indexLabelFontSize: 16,
		indexLabel: "{label}({s}/{total}) - #percent%",
		yValueFormatString: "#,##0",
		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
	}]
});
chart4.render();


<?php
$dataPoints1 = array();
$dataPoints2 = array();
$dataPoints3 = array();
foreach($d4->result() as $row)
{
    $dataPoints1[] = array("label"=>$row->code,"y"=>$row->t-$row->c);
    $dataPoints2[] = array("label"=>$row->code,"y"=>$row->c);
    $dataPoints3[] = array("label"=>$row->code,"y"=>$row->t);
}

	
?>
var chart5 = new CanvasJS.Chart("chartContainer5", {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: "Tabla de aprobación y reprobación de estudiantes para [<?php echo $yr;?>]"
	},
	legend:{
		cursor: "pointer",
		verticalAlign: "center",
		horizontalAlign: "right",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Total de estudiantes",
		indexLabel: "{y}",
		yValueFormatString: "#0.##",
		showInLegend: true,
		dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
	},{
		type: "column",
		name: "Estudiantes aprobados",
		indexLabel: "{y}",
		yValueFormatString: "#0.##",
		showInLegend: true,
		dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
	},{
		type: "column",
		name: "Estudiantes reprobados o pendientes",
		indexLabel: "{y}",
		yValueFormatString: "#0.##",
		showInLegend: true,
		dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
	}]
});
chart5.render();
 
function toggleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else{
		e.dataSeries.visible = true;
	}
	chart5.render();
}
 

}
</script>


<div id="chartContainer" style="height: 300px; width: 100%;"></div>
<br>
<br>
<div id="chartContainer3" style="height: 300px; width: 100%;"></div>
<br>
<br>
<div class="col-sm-12"><div id="chartContainer2" style="height: 300px; width: 100%;"></div></div>
<br>
<br>
<div class="col-sm-12"><div id="chartContainer4" style="height: 300px; width: 100%;"></div></div>
<br>
<br>
<div class="col-sm-12"><div id="chartContainer5" style="height: 300px; width: 100%;"></div></div>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>