        <h4 class="page-title">Aviso</h4>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/addNotice/">Añadir</a>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Sr</th>
                        <th>Título</th>
                        <th style="width:70px;">Editar</th>
                        <th style="width:70px;">Borrar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($post->num_rows()>0)
                    {
                        foreach($post->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->id;?></td>
                                 <td><?php echo $row->sub;?></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/editNotice/".$row->id;?>'"><span class="la las la-pencil-square-o"></span></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/deleteNotice/".$row->id;?>'"><span class="la la-bitbucket"></span></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>