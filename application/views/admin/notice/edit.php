<?php
$id='';
$sub='';
$cont='';
foreach($post->result() as $row)
{
    $id=$row->id;
    $sub=$row->sub;
    $cont=$row->cont;
}

?>
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/notice">Atrás</a>
<h4 class="page-title">Añadir aviso</h4>
<form method="post" action="<?php echo base_url()?>AdminBack/editNotice/<?php echo $id;?>" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Aviso</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Título</label>
    			<input type="text" class="form-control" id="title" name="title" value="<?php echo $sub;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="bio">Descripción</label>
    			<textarea class="form-control" id="des" name="des" rows="10" required autofocus><?php echo $cont;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="img">Imagen</label>
    			<input type="file" class="form-control-file" id="img" name="img" onchange="return fileValidation()">
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>

<script>
    
     function fileValidation(){
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>