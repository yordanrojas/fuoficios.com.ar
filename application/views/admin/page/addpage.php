<h4 class="page-title">Páginas</h4>
<?php
if($p==1)
{
    $feat1='';
    $feat2='';
    $feat3='';
    $fdes1='';
    $fdes2='';
    $fdes3='';
    $abt='';
    $img='';
    $count1='';
    $count2='';
    $count3='';
    $why='';
    $vwhy='';
    $link1='';
    $link2='';
    $link3='';
    $link4='';
    $link5='';
    $data1='';
    $data2='';
    $data3='';
    $data4='';
    $data5='';
    foreach($page->result() as $row)
    {
        $data1=$row->data1;
        $data2=$row->data2;
        $data3=$row->data3;
        $data4=$row->data4;
        $data5=$row->data5;
    }
    $data=json_decode($data1);
    $feat1=$data->feat1;
    $feat2=$data->feat2;
    $feat3=$data->feat3;
    $fdes1=$data->des1;
    $fdes2=$data->des2;
    $fdes3=$data->des3;
    $data=json_decode($data2);
    $abt=$data->abt;
    $img=$data->img;
    $data=json_decode($data3);
    $count1=$data->count1;
    $count2=$data->count2;
    $count3=$data->count3;
    $data=json_decode($data4);
    $why=$data->why;
    $vwhy=$data->vwhy;
    $data=json_decode($data5);
    $link1=$data->link1;
    $link2=$data->link2;
    $link3=$data->link3;
    $link4=$data->link4;
    $link5=$data->link5;
    ?>
<form method="post" action="<?php echo base_url()?>AdminBack/addPage/1" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Caracteristicas</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    		    <input type="hidden" class="form-control" id="himg" name="himg" value="<?php echo $img;?>">
    			<label for="title">Característica 1</label>
    			<input type="text" class="form-control" id="feat1" name="feat1" placeholder="Característica" value="<?php echo $feat1;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="des">Descripción</label>
    			<textarea class="form-control" id="fdes1" name="fdes1" required autofocus><?php echo $fdes1;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="title">Característica 2</label>
    			<input type="text" class="form-control" id="feat2" name="feat2" placeholder="Característica" value="<?php echo $feat2;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="des">Descripción</label>
    			<textarea class="form-control" id="fdes2" name="fdes2" required autofocus><?php echo $fdes2;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="title">Característica 3</label>
    			<input type="text" class="form-control" id="feat3" name="feat3" placeholder="Característica" value="<?php echo $feat3;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="des">Descripción</label>
    			<textarea class="form-control" id="fdes3" name="fdes3" required autofocus><?php echo $fdes3;?></textarea>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">¿Quiénes Somos?</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="abt">Descripción</label>
    			<textarea class="form-control" id="abt" name="abt" required autofocus><?php echo $abt;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="img">Imagen</label>
    			<input type="file" class="form-control-file" id="abtimg" name="abtimg" onchange="return fileValidation('abtimg')"  >
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">Mostrador</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="des">Estudiantes Graduados</label>
    			<input type="number" class="form-control" id="count1" name="count1" value="<?php echo $count1;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="des">Profesores Expertos</label>
    			<input type="number" class="form-control" id="count2" name="count2" value="<?php echo $count2;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="des">Nuestros Cursos</label>
    			<input type="number" class="form-control" id="count3" name="count3" value="<?php echo $count3;?>" required autofocus>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">¿Por qué FUO?</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="abt">Descripción</label>
    			<textarea class="form-control" id="why" name="why" required autofocus><?php echo $why;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="abt">ID de video de Youtube</label>
    			<input type="text" class="form-control" id="vwhy" name="vwhy" value="<?php echo $vwhy;?>" required autofocus>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">Medios de comunicación social</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="abt">Twitter</label>
    			<input type="text" class="form-control" id="link1" name="link1" value="<?php echo $link1;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="abt">Facebook</label>
    			<input type="text" class="form-control" id="link2" name="link2" value="<?php echo $link2;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="abt">Youtube</label>
    			<input type="text" class="form-control" id="link3" name="link3" value="<?php echo $link3;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="abt">Instagram</label>
    			<input type="text" class="form-control" id="link4" name="link4" value="<?php echo $link4;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="abt">What"s app</label>
    			<input type="text" class="form-control" id="link5" name="link5" value="<?php echo $link5;?>" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
    
    
    
    
    <?php
}elseif($p==2)
{
    $vision='';
    $mission='';
    $v1='';
    $v2='';
    $v3='';
    $v4='';
    $v5='';
    $v6='';
    $vd1='';
    $vd2='';
    $vd3='';
    $vd4='';
    $vd5='';
    $vd6='';
    $g1='';
    $g2='';
    $g3='';
    $p1='';
    $p2='';
    $p3='';
    $p4='';
    $p5='';
    $data1='';
    $data2='';
    $data3='';
    $data4='';
    $data5='';
    foreach($page->result() as $row)
    {
        $data1=$row->data1;
        $data2=$row->data2;
        $data3=$row->data3;
        $data4=$row->data4;
        $data5=$row->data5;
    }
    $data=json_decode($data1);
    $vision=$data->vission;
    $data=json_decode($data2);
    $mission=$data->mission;
    $data=json_decode($data3);
    $v1=$data->v1;
    $v2=$data->v2;
    $v3=$data->v3;
    $v4=$data->v4;
    $v5=$data->v5;
    $v6=$data->v6;
    $vd1=$data->vd1;
    $vd2=$data->vd2;
    $vd3=$data->vd3;
    $vd4=$data->vd4;
    $vd5=$data->vd5;
    $vd6=$data->vd6;
    $data=json_decode($data4);
    $g1=$data->g1;
    $g2=$data->g2;
    $g3=$data->g3;
    $data=json_decode($data5);
    $p1=$data->p1;
    $p2=$data->p2;
    $p3=$data->p3;
    $p4=$data->p4;
    $p5=$data->p5;
    ?>
<form method="post" action="<?php echo base_url()?>AdminBack/addPage/2" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Visión</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="vision">Descripción</label>
    			<textarea class="form-control" id="vision" name="vision" required autofocus><?php echo $vision;?></textarea>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">Misión</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="mission">Descripción</label>
    			<textarea class="form-control" id="mission" name="mission" required autofocus><?php echo $mission;?></textarea>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">Valores</div>
        </div>
    	<div class="card-body">
    	    <div class="form-group">
    			<label for="v1">Valores 1</label>
    			<input type="text" class="form-control" id="v1" name="v1" placeholder="Valores" value="<?php echo $v1;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="vd1">Descripción</label>
    			<textarea class="form-control" id="vd1" name="vd1" required autofocus><?php echo $vd1;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="v2">Valores 2</label>
    			<input type="text" class="form-control" id="v2" name="v2" placeholder="Valores" value="<?php echo $v2;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="vd2">Descripción</label>
    			<textarea class="form-control" id="vd2" name="vd2" required autofocus><?php echo $vd2;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="v3">Valores 3</label>
    			<input type="text" class="form-control" id="v3" name="v3" placeholder="Valores" value="<?php echo $v3;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="vd3">Descripción</label>
    			<textarea class="form-control" id="vd3" name="vd3" required autofocus><?php echo $vd3;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="v4">Valores 4</label>
    			<input type="text" class="form-control" id="v4" name="v4" placeholder="Valores" value="<?php echo $v4;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="vd4">Descripción</label>
    			<textarea class="form-control" id="vd4" name="vd4" required autofocus><?php echo $vd4;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="v5">Valores 5</label>
    			<input type="text" class="form-control" id="v5" name="v5" placeholder="Valores" value="<?php echo $v5;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="vd5">Descripción</label>
    			<textarea class="form-control" id="vd5" name="vd5" required autofocus><?php echo $vd5;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="v6">Valores 6</label>
    			<input type="text" class="form-control" id="v6" name="v6" placeholder="Valores" value="<?php echo $v6;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="vd6">Descripción</label>
    			<textarea class="form-control" id="vd6" name="vd6" required autofocus><?php echo $vd6;?></textarea>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">Gestionamos FUO</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="g1">Descripción</label>
    			<textarea class="form-control" id="g1" name="g1" required autofocus><?php echo $g1;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="g2">Descripción</label>
    			<textarea class="form-control" id="g2" name="g2" required autofocus><?php echo $g2;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="g3">Descripción</label>
    			<textarea class="form-control" id="g3" name="g3" required autofocus><?php echo $g3;?></textarea>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">Para ello</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="p1">Descripción</label>
    			<textarea class="form-control" id="p1" name="p1" required autofocus><?php echo $p1;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="p2">Descripción</label>
    			<textarea class="form-control" id="p2" name="p2" required autofocus><?php echo $p2;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="p3">Descripción</label>
    			<textarea class="form-control" id="p3" name="p3" required autofocus><?php echo $p3;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="p4">Descripción</label>
    			<textarea class="form-control" id="p4" name="p4" required autofocus><?php echo $p4;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="p5">Descripción</label>
    			<textarea class="form-control" id="p5" name="p5" required autofocus><?php echo $p5;?></textarea>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
    <?php
}else{
    $ph1='';
    $ph2='';
    $ph3='';
    $ng='';
    $mail='';
    $data1='';
    $data2='';
    $data3='';
    $data4='';
    $data5='';
    foreach($page->result() as $row)
    {
        $data1=$row->data1;
        $data2=$row->data2;
        $data3=$row->data3;
        $data4=$row->data4;
        $data5=$row->data5;
    }
    $data1=json_decode($data1);
    $data2=json_decode($data2);
    $data3=json_decode($data3);
    $ph1=$data1->ph1;
    $ph2=$data1->ph2;
    $ph3=$data1->ph3;
    $ng=$data2->ng;
    $mail=$data3->mail;
?>
    <form method="post" action="<?php echo base_url()?>AdminBack/addPage/3" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Tu teléfono</div>
        </div>
    	<div class="card-body">
    	    <div class="form-group">
    			<label for="ph1">Tu teléfono 1</label>
    			<input type="tel" class="form-control" id="ph1" name="ph1" value="<?php echo $ph1;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="ph2">Tu teléfono 2</label>
    			<input type="tel" class="form-control" id="ph2" name="ph2" value="<?php echo $ph2;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="ph3">Tu teléfono 3</label>
    			<input type="tel" class="form-control" id="ph3" name="ph3" value="<?php echo $ph3;?>" required autofocus>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">Navegación</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="ng">Navegación</label>
    			<textarea class="form-control" id="ng" name="ng" required autofocus><?php echo $ng;?></textarea>
    		</div>
    	</div>
    	<div class="card-header">
            <div class="card-title">Correo</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="mail">Correo</label>
    			<input type="email" class="form-control" id="mail" name="mail" value="<?php echo $mail;?>" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
<?php
}
?>
<script>
    
     function fileValidation(i){
        var fileInput = document.getElementById(i);
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>