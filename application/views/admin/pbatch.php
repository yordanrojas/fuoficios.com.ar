<?php
$tnum=$tbatch->num_rows();
?>
<h4 class="page-title">Estudiantes</h4>
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <?php
        $it=1;
        foreach($tbatch->result() as $row)
        {
            ?>
            <a class="nav-item nav-link text-success <?php if($it==1){ echo 'active';}?>" id="nav-home-tab" data-toggle="tab" href="#<?php echo $row->code;?>" role="tab" aria-controls="nav-home" aria-selected="true"><?php echo $row->code;?></a>
            <?php
            $it++;
        }
    ?>
    
  </div>
</nav>
<div class="tab-content pt-2 pb-2 pr-2 pl-2" id="nav-tabContent">
    <?php
        $it=1;
        foreach($tbatch->result() as $row)
        {
            ?>
            <div class="tab-pane fade show <?php if($it==1){ echo 'active';}?>" id="<?php echo $row->code;?>" role="tabpanel" aria-labelledby="<?php echo $row->code;?>-tab">
                <table id="table<?php echo $row->code;?>" class="table table-hover table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>Tu Nombre</th>
                        <th>Correo</th>
                        <th>telefono</th>
                        <th>Grupo</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
            <?php
            $ti=1;
            foreach($bbatch->result() as $r)
            {
                if($row->code == $r->code)
                {
                    ?>
                    <tr>
                        <td><?php echo $r->name.' '.$r->lname;?></td>
                        <td><?php echo $r->email;?></td>
                        <td><?php echo $r->mob;?></td>
                        <td>
                            <select id="input<?php echo $r->id;?>" name="input<?php echo $r->id;?>" class="form-control">
                                <?php
                                foreach($sbatch->result() as $r1)
                                if($row->code == $r1->code)
                                {
                                    ?>
                                    <option value='<?php echo json_encode($r1);?>'><?php echo $r1->code.sprintf("%02d",$r1->sr).date('y',strtotime('01-01-'.$r1->yr));?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td><butoon class="btn btn-success" onclick="formBatch(<?php echo $r->id?>)">Submit</butoon></td>
                    </tr>
                    <?php
                }
                $ti++;
            }
            ?>
            </tbody>
            </table>
            </div>
            <script>
$(document).ready(function() {
    $('#table<?php echo $row->code;?>').DataTable();
} );
</script>
            <?php
            $it++;
            
        }
    ?>
</div>
    <a class="btn btn-success mt-5" href="<?php echo base_url();?>Admin/addBatch">Añadir Grupos</a>
        <h4 class="page-title mt-3">Pendiente</h4>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Grupo</th>
                        <th>Estudiantes</th>
                        <th style="width:70px;">Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($batch->num_rows()>0)
                    {
                        foreach($batch->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr));?></td>
                                 <td><?php echo $row->count;?></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/pbatchView/".$row->id;?>'"><span class="la las la-pencil-square-o"></span></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            
            
            <script>
                
                        function formBatch(e)
        {
            var v=$('#input'+e).find(":selected").val();
            if(v!='')
            {
                $.post("<?php echo base_url();?>AdminBack/addBatchstd/",
                  {
                  b:v,
                  e:e
                  },
                  function(data, status){
                     if(status=='success')
                     {
                         var d=JSON.parse(data);
                         if(d.stat==1)
                         {
                            window.location.reload();
                         }else
                         {
                         $.notify({icon: 'la la-bell',title: 'Error',message: d.msg,},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});
                         }
                     }else
                     {
                     $.notify({icon: 'la la-bell',title: 'Error',message: 'Error al subir la imagen',},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});
                     }
                  });
                
            }
        }
            </script>