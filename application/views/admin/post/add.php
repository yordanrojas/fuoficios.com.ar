<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/post">Atrás</a>
<h4 class="page-title">Añadir Noticias y Eventos</h4>
<form method="post" action="<?php echo base_url()?>AdminBack/addPost/" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Noticias y Eventos</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Título</label>
    			<input type="text" class="form-control" id="title" name="title" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="bio">Descripción</label>
    			<textarea class="form-control" id="des" name="des" rows="10" required autofocus></textarea>
    		</div>
    		<div class="form-group">
    			<label for="mob">Tipo</label>
    			<select class="form-control" id="type" name="type" required autofocus>
    			    <option value="1">Noticias</option>
    			    <option value="2">Eventos</option>
    			</select>
    		</div>
    		<div class="form-group">
    			<label for="img">Imagen</label>
    			<input type="file" class="form-control-file" id="img" name="img" onchange="return fileValidation()" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>

<script>
    
     function fileValidation(){
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>