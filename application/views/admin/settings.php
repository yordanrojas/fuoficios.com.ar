<h4 class="page-title">Configuraciones</h4>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Tablero</div>
        </div>
    	<div class="card-body row">
    	    <div class="col-sm-2"></div>
    	    <div class="col-sm-2">Año:</div>
    	    <div class="col-sm-2">
    	        <input type="text" class="form-control" name="yr" value="<?php echo $year;?>">
    	        </div>
    	    <div class="col-sm-3"><button class="btn btn-success" onclick="conDelete()">Enviar</button></div>
    	    <div class="col-sm-3"></div>
    	</div>
    </div>
                <div class="modal" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Esta seguro?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="chYr()">Aceptar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
    <script>
        function conDelete()
                {
                    $('#myModal').modal('toggle');
                }
        function chYr()
        {
            var v=$('input[name=yr]').val();
            if(v!='')
            {
                window.location.href="<?php echo base_url();?>AdminBack/setting/1/"+v;
            }
        }
    </script>