  <style>
      [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
     position: relative; 
     left: inherit; 
}
  </style>
<?php
if($s==1)
{
    ?>
        <h4 class="page-title">Control deslizante principal</h4>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/addSlide/1">Añadir</a>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Posición</th>
                        <th>Título</th>
                        <th>Ocultar</th>
                        <th style="width:70px;">Editar</th>
                        <th style="width:70px;">Borrar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($slider->num_rows()>0)
                    {
                        foreach($slider->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->pos;?></td>
                                 <td><?php echo $row->title;?></td>
                                 <th class="text-center"><input type="checkbox" value="1" onclick="sendCheck(<?php echo $row->id;?>,this)" <?php if($row->hide==1){ echo "checked";}?>></th>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/editSlide/1/".$row->id;?>'"><span class="la las la-pencil-square-o"></span></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/deleteSlide/1/".$row->id;?>'"><span class="la la-bitbucket"></span></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
    <?php
}else
{
    ?>
        <h4 class="page-title">Control deslizante de producto</h4>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/addSlide/2">Añadir</a>
        <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Posición</th>
                        <th>Título</th>
                        <th style="width:70px;">Editar</th>
                        <th style="width:70px;">Borrar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($slider->num_rows()>0)
                    {
                        foreach($slider->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->pos;?></td>
                                 <td><?php echo $row->title;?></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/editSlide/2/".$row->id;?>'"><span class="la las la-pencil-square-o"></span></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/deleteSlide/2/".$row->id;?>'"><span class="la la-bitbucket"></span></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
    <?php
}
?>

            <script>
            function sendCheck(id,t)
                {
                    var c=0;
                    if($(t).prop("checked") == true){
                        c=1;
                    }
                $.post("<?php echo base_url();?>AdminBack/hideSlide",
                  {
                    id: id,
                    c: c
                  },
                  function(data, status){
                  });
                }
                
                
                </script>
