<?php
if($s==1)
{
    ?>
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/slider/<?php echo $s;?>">Atrás</a>
<?php
}
else
{
    ?>
    <a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/slider/<?php echo $s;?>">Atrás</a>
    <?php
}
?>
<h4 class="page-title">Agregar diapositiva</h4>
<?php
if($s==1)
{
    ?>
<form method="post" action="<?php echo base_url()?>AdminBack/addSlide/1" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Diapositivas</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="title">Título</label>
    			<input type="text" class="form-control" id="title" name="title" placeholder="Título" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="des">Descripción</label>
    			<textarea class="form-control" id="des" name="des" required autofocus></textarea>
    		</div>
    		<div class="form-group">
    			<label for="img">Imagen</label>
    			<input type="file" class="form-control-file" id="img" name="img" onchange="return fileValidation()" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="pos">Posición</label>
    			<input type="number" class="form-control" style="width:100px;" id="pos" name="pos" value="1" min="1" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
    
    
    
    
    <?php
}else
{
    ?>
<form method="post" action="<?php echo base_url()?>AdminBack/addSlide/2" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Diapositivas</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="title">Título</label>
    			<input type="text" class="form-control" id="title" name="title" placeholder="Título" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="img">Imagen</label>
    			<input type="file" class="form-control-file" id="img" name="img" onchange="return fileValidation()" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="pos">Posición</label>
    			<input type="number" class="form-control" style="width:100px;" id="pos" name="pos" value="1" min="1" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
    <?php
}
?>
<script>
    
     function fileValidation(){
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>