         <style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: inherit;
    left: inherit;
}
th
{
    text-align:justify;
}
</style>
        <h4 class="page-title">Estudiantes</h4>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Home/register/?role=admin" target="__new">Añadir</a>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Registro</th>
                        <th>Grupo</th>
                        <th>Tu Nombre</th>
                        <th>Tu Apellido</th>
                        <th>Correo</th>
                        <th>Documentos</th>
                        <th>Seguro Social</th>
                        <th style="width:70px;">Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($students->num_rows()>0)
                    {
                        foreach($students->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->eid;?></td>
                                 <td><?php echo $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr));?></td>
                                 <td><?php echo $row->name;?></td>
                                 <td><?php echo $row->lname;?></td>
                                 <td><?php echo $row->email;?></td>
                                 <td><?php $doc=$row->doc1*$row->doc2*$row->doc3*$row->doc4*$row->doc5*$row->doc6; if($doc==0){ echo "Pendiente";}else{echo"Entregados";}?></td>
                                 <td style="text-align:center;"><input type="checkbox" value="1" onclick="checkTool(this,<?php echo $row->eid;?>)" <?php if($row->ins==1){ echo "checked";}?>></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/viewStudent/".$row->sid;?>'"><span class="la las la-pencil-square-o"></span></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <script type="text/javascript">
                function conDelete(i)
                {
                    var check = confirm("Esta seguro?");
                    if(check== true)
                    {
                        window.location.href='<?php echo base_url()."Admin/deleteStudent/";?>'+i;
                    }
                }
            </script>
            
            <script>
                function checkTool(t,id)
                {
                    var c=0;
                    if($(t).prop("checked") == true){
                        c=1;
                    }
                $.post("<?php echo base_url();?>AdminBack/tool/",
                  {
                    id: id,
                    c: c
                  },
                  function(data, status){
                  });
                }
            </script>