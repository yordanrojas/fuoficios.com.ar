        <h4 class="page-title">Estudiantes</h4>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Home/registeradmin" target="__new">Añadir</a>
        <table id="user_data" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Registro</th>
                        <th>Tu Nombre</th>
                        <th>Tu Apellido</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>Documentos</th>
                        <th style="width:70px;">Editar</th>
                    </tr>
                </thead>
            </table>
            <script type="text/javascript">
                function conView(i)
                {
                        window.location.href='<?php echo base_url()."Admin/viewStudent/";?>'+i;
                }
                
                    $(document).ready(function(){
        var dataTable = $('#user_data').DataTable({

            "processing":true,
            "serverSide":true,
            "order":[],
            "ajax":{
                url:"<?php echo base_url() . 'AdminBack/fetch_students'; ?>",
                type:"POST"
            },
            "columnDefs":[
                {
                    "targets":[4,5],
                    "orderable":false,
                },
            ],
            "language": {
            "lengthMenu": "Mostrar _MENU_ datos por página",
            "zeroRecords": "Nada encontrado - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles.",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "paginate": {
        "first":      "Primero",
        "last":       "Último",
        "next":       "Próximo",
        "previous":   "Previo"
    }
        }
        });

        setInterval(function () {
            dataTable.ajax.reload(null,false);
        },30000);
    });
            </script>