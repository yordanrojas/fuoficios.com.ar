<?php
$id='';
$name='';
$lname='';
$email='';
$mob='';
$dni='';
$details='';
foreach($student->result() as $row)
{
    $id=$row->id;
    $name=$row->name;
    $lname=$row->lname;
    $email=$row->email;
    $mob=$row->mob;
    $dni=$row->pid;
    $details=json_decode($row->details);
}
?>
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/viewStudent/<?php echo $id;?>">Atrás</a>
<form method="post" action="<?php echo base_url()?>AdminBack/updateStudent/<?php echo $id;?>" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Alumno</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Tu Nombre</label>
    			<input type="text" class="form-control" name="name" value="<?php echo $name;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="name">Tu Apellido</label>
    			<input type="text" class="form-control" name="lname" value="<?php echo $lname;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="name">Tu Teléfono</label>
    			<input type="text" class="form-control" name="mob" value="<?php echo $mob;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="name">Correo</label>
    			<input type="text" class="form-control" name="email" value="<?php echo $email;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="name">DNI/Passport</label>
    			<input type="text" class="form-control" name="pid" value="<?php echo $dni;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="name">Fecha de Nacimiento</label>
    			<input type="date" class="form-control" name="dob" value="<?php echo $details->dob;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="name">Edad</label>
    			<input type="number" class="form-control" name="age" value="<?php echo $details->age;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="name">Nacionalidad</label>
    			<input type="text" class="form-control" name="nat" value="<?php echo $details->nat;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="name">Domicilio</label>
    			<input type="text" class="form-control" name="addr" value="<?php echo $details->addr;?>" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
