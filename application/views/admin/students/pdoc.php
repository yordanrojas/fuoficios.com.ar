        <style>
            [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
                position:relative;
                left: inherit;
            }
            th
            {
                text-align:justify;
            }
        </style>
        <h4 class="page-title">Documentos Pendientes</h4>
        <br>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>DNI</th>
                        <th>Ficha  de  Inscripción</th>
                        <th>Ficha  de  Salud</th>
                        <th>Reglamento</th>
                        <th>Contrato</th>
                        <th>Compromiso</th>
                        <th style="width:70px;">Enviar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($student->num_rows()>0)
                    {
                        foreach($student->result() as $row)
                        {
                            ?>
                            <tr>
                                <form method="post" action="<?php echo base_url()?>AdminBack/upDoc/<?php echo $row->id;?>/<?php echo $row->uid;?>" enctype="multipart/form-data" >
                                 <td><?php echo $row->id;?></td>
                                 <td><?php echo $row->name;?></td>
                                 <td><?php echo $row->email;?></td>
                                 <td class="text-center"><input type="checkbox" name="doc[]" value="1" <?php if($row->doc1==1){ echo "checked";}?>></td>
                                 <td class="text-center"><input type="checkbox" name="doc[]" value="2" <?php if($row->doc2==1){ echo "checked";}?>></td>
                                 <td class="text-center"><input type="checkbox" name="doc[]" value="3" <?php if($row->doc3==1){ echo "checked";}?>></td>
                                 <td class="text-center"><input type="checkbox" name="doc[]" value="4" <?php if($row->doc4==1){ echo "checked";}?>></td>
                                 <td class="text-center"><input type="checkbox" name="doc[]" value="5" <?php if($row->doc5==1){ echo "checked";}?>></td>
                                 <td class="text-center"><input type="checkbox" name="doc[]" value="6" <?php if($row->doc6==1){ echo "checked";}?>></td>
                                 <td class="text-center" style="font-size:1.1rem;"><input type="submit" class="btn btn-success" value="Enviar"></td>
                                 </form>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <script type="text/javascript">
                function conDelete(i)
                {
                    var check = confirm("Are you sure?");
                    if(check== true)
                    {
                        window.location.href='<?php echo base_url()."Admin/deleteStudent/";?>'+i;
                    }
                }
            </script>