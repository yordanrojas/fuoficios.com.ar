<style>
    
    .col-sm-6
    {
        margin-bottom:5px;
    }
</style>
<?php
$sid='';
$name='';
$lname='';
$email='';
$mob='';
$dni='';
$details='';
$other='';
foreach($student->result() as $row)
{
    $sid=$row->id;
    $name=$row->name;
    $lname=$row->lname;
    $email=$row->email;
    $mob=$row->mob;
    $dni=$row->pid;
    $details=json_decode($row->details);
    $other=json_decode($row->other);
}
?>
<script>
    function genTab(wid,id)
    {
        $.post("<?php echo base_url();?>Admin/getMarks/"+id,
                  {
                  },
                  function(data, status){
                      
                      $('#'+wid).html(data);
                  });
    }
</script>
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/students">Atrás</a>
<h4 class="page-title">Alumno</h4>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Detalles</div>
        </div>
    	<div class="card-body row">
    	    <div class="col-sm-6">Nombre: <?php echo $name.' '.$lname;;?></div>
    	    <div class="col-sm-6">Correo: <?php echo $email;?></div>
    	    <div class="col-sm-6">Teléfono: <?php echo $mob;?></div>
    	    <div class="col-sm-6">DNI/Passport: <?php echo $dni;?></div>
    	    <div class="col-sm-6">Edad: <?php echo $details->age;?></div>
    	    <div class="col-sm-6">Fecha de Nacimiento: <?php echo date('d/m/Y',strtotime($details->dob));?></div>
    	    <div class="col-sm-6">Nacionalidad: <?php echo $details->nat;?></div>
    	    <div class="col-sm-6">Domicilio: <?php echo $details->addr;?></div>
    	    <div class="col-sm-6">¿Como se enteró de la FUO?: <?php echo $other->refer;?></div>
    	    <div class="col-sm-6">¿Trabaja?: <?php echo $other->empstat;?></div>
    	    <div class="col-sm-6">¿Cúal es su situación laboral y cúal es su objetivo?: <?php echo $other->emp;?></div>
    	    <div class="col-sm-6">Nivel Educativo: <?php echo $other->edu;?></div>
    	</div>
    	<div class="card-action">
    		<a class="btn btn-success" href="<?php echo base_url()?>Admin/editStudent/<?php echo $sid;?>">Editar</a>
    	</div>
    </div>
    <form method="post" action="<?php echo base_url()?>AdminBack/addEnroll/<?php echo $sid;?>" enctype="multipart/form-data" >
        <div class="card">
            <div class="card-header">
                <div class="card-title">Inscribirse</div>
            </div>
        	<div class="card-body">
        		<div class="form-group">
        			<label for="name">Formación</label>
        			<select class="form-control" name="course" required autofocus>
        			    <option value=""></option>
        			    <?php
        			    foreach($courses->result() as $row)
        			    {
        			       
            			        ?>
            			        <option value="<?php echo $row->id?>"><?php echo $row->title.'('.$row->type.')'.'('.$row->cat.')';?></option>
            			        <?php
        			    }
        			    ?>
        			</select>
        		</div>
        	</div>
        	<div class="card-action">
        		<button class="btn btn-success">Enviar</button>
        	</div>
        </div>
    </form>
    
    <?php
    $tab=1;
    $eid='';
    $total='';
    foreach($enroll->result() as $row)
    {
        $total=$row->total;
        $eid=$row->enid;
        $data=json_decode($row->data);
        ?>
        <div class="card">
            <div class="card-header">
                <div class="card-title"><?php echo $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr));?></div>
            </div>
        	<div class="card-body">
        	    <div class="col-sm-12">
    	        Curso:-<?php echo $row->title;?>
    	        </div>
    	        <div class="col-sm-6">
    	            Profesor:- <?php echo $row->tname;?> (<?php echo $row->dsg;?>)
    	            </div>
    	    
        		<div class="col-sm-12">
        		   Desde:-<?php echo date('d/m/Y',strtotime($row->dfrm));?><br>
        		   A:-<?php echo date('d/m/Y',strtotime($row->dto));?>
        		</div>
        		<div class="col-sm-12">
        		   <?php
        		   foreach($data->daytime as $row)
        		   {
        		       switch($row->day)
                        {
                            case 1:
                            echo "Lunes:- ";
                            break;
                            case 2:
                            echo "Martes:- ";
                            break;
                            case 3:
                            echo "Miércoles:- ";
                            break;
                            case 4:
                            echo "Jueves:- ";
                            break;
                            case 5:
                            echo "Viernes:- ";
                            break;
                            case 6:
                            echo "Sábado:- ";
                            break;
                        }
        		       echo $row->from.'-'.$row->to.'<br>';
        		   }
        		   ?>
        		</div>
        		<div class="col-sm-6">
    	            Asistencia:- <?php echo intval($total);?>%
    	            </div>
        		<div class="col-sm-12" id="tab<?php echo $tab;?>">
        		    <script>genTab('tab<?php echo $tab;?>',<?php echo $eid;?>);</script>
        		</div>
        	</div>
        </div>
        
        <?php
        $tab++;
    }
    
    ?>
 

