        <h4 class="page-title">Profesores</h4>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/addTeacher/">Añadir</a>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Tu Nombre</th>
                        <th>Tu Apellido</th>
                        <th>Designacion</th>
                        <th style="width:70px;">Editar</th>
                        <th style="width:70px;">Borrar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($teachers->num_rows()>0)
                    {
                        foreach($teachers->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->name;?></td>
                                 <td><?php echo $row->lname;?></td>
                                 <td><?php echo $row->dsg;?></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/editTeacher/".$row->id;?>'"><span class="la las la-pencil-square-o"></span></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/deleteTeacher/".$row->id;?>'"><span class="la la-bitbucket"></span></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>