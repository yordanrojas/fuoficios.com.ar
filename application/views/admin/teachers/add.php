<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/teachers">Atrás</a>
<h4 class="page-title">Añadir profesor</h4>
<form method="post" action="<?php echo base_url()?>AdminBack/addTeacher/" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Profesores</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Tu Nombre</label>
    			<input type="text" class="form-control" id="name" name="name" placeholder="Tu Nombre" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="lname">Tu Apellido</label>
    			<input type="text" class="form-control" id="lname" name="lname" placeholder="Tu Apellido" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="dsg">Designacion</label>
    			<input type="text" class="form-control" id="dsg" name="dsg" placeholder="Designacion" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="bio">Bio</label>
    			<textarea class="form-control" id="bio" name="bio" rows="5" required autofocus></textarea>
    		</div>
    		<div class="form-group">
    			<label for="mob">Teléfono</label>
    			<input type="tel" class="form-control" id="mob" name="mob" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="email">Correo electrónico</label>
    			<input type="mail" class="form-control" id="email" name="email" placeholder="guest@gmail.com" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="pass">Contraseña</label>
    			<input type="text" class="form-control" id="pass" name="pass" placeholder="*******" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="img">Imagen</label>
    			<input type="file" class="form-control-file" id="img" name="img" onchange="return fileValidation()" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Submit</button>
    		<button class="btn btn-danger">Cancel</button>
    	</div>
    </div>
</form>

<script>
    
     function fileValidation(){
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>