<?php
$id='';
$name='';
$lname='';
$dsg='';
$bio='';
$mob='';
$email='';
foreach($teacher->result() as $row)
{
    $id=$row->id;
    $name=$row->name;
    $lname=$row->lname;
    $dsg=$row->dsg;
    $bio=$row->bio;
    $mob=$row->mob;
    $email=$row->email;  
}
?>

<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/teachers">Atrás</a>
<h4 class="page-title">Editar profesor</h4>
<form method="post" action="<?php echo base_url()?>AdminBack/updateTeacher/<?php echo $id;?>" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Profesores</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Tu Nombre</label>
    			<input type="text" class="form-control" id="name" name="name" value="<?php echo $name;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="lname">Tu Apellido</label>
    			<input type="text" class="form-control" id="lname" name="lname" value="<?php echo $lname;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="dsg">Designacion</label>
    			<input type="text" class="form-control" id="dsg" name="dsg" value="<?php echo $dsg;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="bio">Bio</label>
    			<textarea class="form-control" id="bio" name="bio" rows="5" required autofocus><?php echo $bio;?></textarea>
    		</div>
    		<div class="form-group">
    			<label for="mob">Teléfono</label>
    			<input type="tel" class="form-control" id="mob" name="mob" value="<?php echo $mob;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="email">Correo electrónico</label>
    			<input type="mail" class="form-control" id="email" name="email"  value="<?php echo $email;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="pass">Contraseña</label>
    			<input type="text" class="form-control" id="pass" name="pass" placeholder="*******" >
    		</div>
    		<div class="form-group">
    			<label for="img">Imagen</label>
    			<input type="file" class="form-control-file" id="img" name="img" onchange="return fileValidation()">
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Submit</button>
    		<button class="btn btn-danger">Cancel</button>
    	</div>
    </div>
</form>

<script>
    
     function fileValidation(){
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>