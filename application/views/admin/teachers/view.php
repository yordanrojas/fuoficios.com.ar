        <h4 class="page-title">Tabla de tiempos</h4>
        <table class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Designacion</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($teachers->num_rows()>0)
                    {
                        foreach($teachers->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->name.' '.$row->lname;?></td>
                                 <td id="t<?php echo $row->id;?>">..</td>
                                 </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            
            <script>
            $( document ).ready(function() {
                <?php
                    if($teachers->num_rows()>0)
                    {
                        foreach($teachers->result() as $row)
                        {
                            ?>
                            getTdata(<?php echo $row->id?>,'t<?php echo $row->id?>');
                            <?php
                        }
                    }
                    ?>
                
            });
            
            function getTdata(id,tid)
            {
                $.post("<?php echo base_url();?>AdminBack/teachData/",
                  {
                    id: id
                  },
                  function(data, status){
                      console.log(status+data);
                      if(status=='success')
                     {
                         $('#'+tid).html(data);
                     }else
                     {
                         getTdata(id,tid);
                     }
                  });
            }
            </script>