        <h4 class="page-title">Los Usuarios</h4>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/addUser/">Añadir</a>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Perfil</th>
                        <th style="width:70px;">Editar</th>
                        <th style="width:70px;">Borrar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($user->num_rows()>0)
                    {
                        foreach($user->result() as $row)
                        {
                            ?>
                            <tr>
                                 <td><?php echo $row->user;?></td>
                                 <td><?php if($row->role==1){ echo "Web";}if($row->role==2){ echo "Inscripción";}?></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/editUser/".$row->id;?>'"><span class="la las la-pencil-square-o"></span></td>
                                 <td class="text-center" style="font-size:1.1rem;" onclick="window.location.href='<?php echo base_url()."Admin/deleteUser/".$row->id;?>'"><span class="la la-bitbucket"></span></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>