
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Admin/user/">Atrás</a>
<h4 class="page-title">Agregar Usuario</h4>
<form id="reg-form" method="post" action="<?php echo base_url()?>AdminBack/addUser/" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Usuario</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Nombre de usuario</label>
    			<input type="text" class="form-control" id="title" name="username" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="dsg">Contraseña</label>
    			<input type="password" class="form-control" id="mark" name="password" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="dsg">Perfil</label>
    			<select class="form-control" id="role" name="role" required autofocus>
    			    <option value="1">Web</option>
    			    <option value="2">Inscripción</option>
    			    </select>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
<script>
    document.getElementById( 'reg-form' ).addEventListener('submit', function(e) {
        if(!/^[A-Za-z0-9]+$/.test($("input[name=username]").val()))
           {
               dspError('Ingrese un apellido válido');
               e.preventDefault();
           }
           var passw=  /^[A-Za-z]\w{7,14}$/;
           var t =document.getElementById( 'mark' );
               if(!t.value.match(passw))
               {
                   dspError('De 7 a 15 caracteres que contienen solo caracteres, dígitos numéricos, guiones bajos y el primer carácter debe ser una letra');
                   e.preventDefault();
               }
           
    });
    
    function dspError(msg)
    {
    $.notify({icon: 'la la-bell',title: 'Error',message: msg,},{type: 'danger',placement: {from: 'bottom',align: 'right'},time: 1000,});
    }
    </script>
