<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pay</title>
        <style>
        body
        {
            background-color:#ecf2f6;
        }
        .cont
        {
           width:50%;
           margin:auto;
           padding:50px 15px;
           margin-top:10vh;
           margin-bottom:10vh;
           -webkit-box-shadow: 0px 0px 14px 3px rgba(0,0,0,0.45);
            -moz-box-shadow: 0px 0px 14px 3px rgba(0,0,0,0.45);
            box-shadow: 0px 0px 14px 3px rgba(0,0,0,0.45);
            background-color:#fff;
            text-align:center;
        }
        .btn
        {
            display: flex;
            flex-direction: row;
            padding: 20px;
            pointer-events: none;
            background-color: #FF5722;
            border-radius: 6px;
            cursor: pointer;
            color: #fff;
            font-size: 15px;
        }
        
        @media only screen and (max-width: 1024px) {
            .cont
            {
                width:60%;
            }
        }
        
         @media only screen and (max-width: 767px) {
            .cont
            {
                width:75%;
            }
        }
        @media only screen and (max-width: 425px) {
            .cont
            {
                width:100%;
                margin:0px;
                -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            }
            body
            {
                margin:0px;
                background-color:#fff;
            }
        }
        </style>
  </head>
  <body>
  <div class="cont">
      <img src="<?php echo $img;?>" style="height:100px;">
      <br>
      <h3><?php echo $title;?></h3>
      <h4>Precio: $<?php echo $price;?></h4>
      <div id="dsp" style="display:flex;">
          <div style="width:45%; margin:auto;">
          <div id="mbbx-button"></div>
          </div>
          <div style="width:45%; margin:auto;" onclick="window.location.href='https://fuoficios.com.ar/Home/myaccount'">
          <a class="btn" href="https://fuoficios.com.ar/Home/myaccount">Atrás</a>
          </div>
      </div>
  </div>

<script type="text/javascript">
    var script = document.createElement('script');
    script.src = `<?php echo base_url();?>assest/mobbex.js?t=${Date.now()}`;
    script.async = true;
    script.addEventListener('load', () => {
      // Realizá la acción que sea necesaria aca :)
       renderMobbexButton();
      // initMobbexPayment();
    });
    document.body.appendChild(script);
  </script>
  <script>
  var d = '';
/*function renderMobbexButton() {
  var mbbxButton = window.MobbexButton.init({
      checkout: `<?php // echo $id;?>`,
      onPayment: (data) => {
          window.location.replace('<?php // echo base_url()?>Home/myaccount');
      },
      onOpen: () => {
        console.info('Pago iniciado.');
      },
      onClose: (cancelled) => {
        console.info(`${cancelled ? 'Cancelado' : 'Cerrado'}`);
      },
      onError: (error) => {
        console.error("ERROR: ", error);
      }
  });
  mbbxButton.open();
}*/

function renderMobbexButton() {
  window.MobbexButton.render({
      checkout: `<?php echo $id;?>`,
      onPayment: (data) => {
        window.location.replace('<?php echo base_url()?>Home/myaccount');
      },
      onOpen: () => {
        console.info('Pago iniciado.');
      },
      onClose: (cancelled) => {
        console.info(`${cancelled ? 'Cancelado' : 'Cerrado'}`);
      },
      onError: (error) => {
        console.error("ERROR: ", error);
      }
  }, '#mbbx-button');
}

</script>
  </body>
  </html>