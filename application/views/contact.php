<?php
    $ph1='';
    $ph2='';
    $ph3='';
    $ng='';
    $mail='';
    $data1='';
    $data2='';
    $data3='';
    $data4='';
    $data5='';
    foreach($data->result() as $row)
    {
        $data1=$row->data1;
        $data2=$row->data2;
        $data3=$row->data3;
        $data4=$row->data4;
        $data5=$row->data5;
    }
    $data1=json_decode($data1);
    $data2=json_decode($data2);
    $data3=json_decode($data3);
    $ph1=$data1->ph1;
    $ph2=$data1->ph2;
    $ph3=$data1->ph3;
    $ng=$data2->ng;
    $mail=$data3->mail;

?>

    <style>
        .mainmenu-area {
            background: #171932!important;
            
        }
        .mt-5,#contact
        {
            padding-bottom:50px;
        }
    </style>
    <section class="section-padding mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Contáctenos</h2>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.1s">
                        <div class="box-icon features-box-icon mx-auto">
                            <i class="fa fa-mobile"></i>
                        </div>
                        <p><?php echo '<a href="tel:'.$ph1.'">'.$ph1.'</a>';?><?php echo ' / <a href="tel:'.$ph2.'">'.$ph2.'</a>';?><?php echo ' / <a href="tel:'.$ph3.'">'.$ph3.'</a>';?></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0  wow fadeInUp padding30" data-wow-delay="0.2s">
                        <div class="box-icon features-box-icon mx-auto">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <p><a href="https://www.google.com/maps?q=Fundación Universitaria de Oficios" target="_blank"><?php echo $ng;?></a></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.3s">
                        <div class="box-icon features-box-icon mx-auto">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <p><a href="mailto:<?php echo $mail;?>"><?php echo $mail;?></a></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0 wow fadeInUp padding30 visible-sm" data-wow-delay="0.4s">
                        <div class="box-icon features-box-icon">
                            <i class="fa fa-cog"></i>
                        </div>
                        <h3 class="box-title">Quality Service</h3>
                        <p>A Google Docs scam that appears to be widespread began landing in Wednesday in what seemed to be a phishing attack.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section class="contact-area sky-gray-bg" id="contact">
        <div class="container-fluid no-padding">
            <div class="row no-margin">
                <div class="no-padding col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="map-area relative">
                        <div id="map" style="width: 100%; height: 600px;">
                            <iframe width="100%" height="600" id="gmap_canvas" src="https://maps.google.com/maps?q=Fundación Universitaria de Oficios&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="no-padding col-md-5 col-lg-5 col-sm-12 col-xs-12">
                    <div class="contact-form-content padding50 xs-padding20" style="padding-top:0px;">
                        <div class="contact-title wow fadeIn">
                            <h3 class="font28 mb50 xs-mb30 xs-font22 xs-mt20">¿Alguna pregunta en mente? Contáctanos.</h3>
                        </div>
                        <div class="contact-form wow fadeIn">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" name="name" placeholder="Tu Nombre" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" name="mob" placeholder="Tu Teléfono">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" name="email" placeholder="Tu Email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <select class="form-control" id="formselect1" name="form-phone">
                                                    <option value="{}">Tipo de Consulta</option>
                                                    <?php
                                                    foreach($form->result() as $row)
                                                    {
                                                        echo "<option value='".json_encode($row)."'>".$row->cat.":".$row->title.")</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <select class="form-control" id="formselect2" name="form-phone">
                                                    <option value="0">Cómo nos conociste?</option>
                                                    <option value="Amigo">Amigo</option>
                                                    <option value="En la FUO">En la FUO</option>
                                                    <option value="Folletería">Folletería</option>
                                                    <option value="Prensa Gráfica">Prensa Gráfica</option>
                                                    <option value="Radio">Radio</option>
                                                    <option value="Redes Sociales">Redes Sociales</option>
                                                    <option value="Televisión">Televisión</option>
                                                    <option value="Web">Web</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="message-field">
                                            <div class="form-input">
                                                <textarea class="form-control" rows="3" name="msg" id="msg" placeholder="Mensaje"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group mb0">
                                            <button type="submit" onclick="sendForm()">Enviar Mensaje</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
        <script>
        function sendForm()
        {
            var error=0;
            var name=$("input[name='name']").val();
            var mob=$("input[name='mob']").val();
            var email=$("input[name='email']").val();
            var course=$("#formselect1 option:selected").val();
            var refer=$("#formselect2 option:selected").val();
            var msg=$('#msg').val();
            if(name.length < 5 || name =='Tu Nombre')
            {
                $.toast({
                text: "Por favor, escriba su nombre.",
                heading: 'Error',
                icon: 'error',
                showHideTransition: 'fade',
                allowToastClose: true,
                hideAfter: 3000,
                stack: 10,
                position: 'bottom-right', 
                textAlign: 'left', 
                loader: true, 
                loaderBg: '#24b07b'
                });
                error=1;
            }
            if(mob.length < 9 || mob =='Tu Teléfono')
            {
                $.toast({
                text: "Por favor, escriba su teléfono.",
                heading: 'Error',
                icon: 'error',
                showHideTransition: 'fade',
                allowToastClose: true,
                hideAfter: 3000,
                stack: 10,
                position: 'bottom-right', 
                textAlign: 'left', 
                loader: true, 
                loaderBg: '#24b07b'
                });
                error=1;
            }
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email) || email =='Tu Email')
            {
                $.toast({
                text: "Por favor, escriba su teléfono.",
                heading: 'Error',
                icon: 'error',
                showHideTransition: 'fade',
                allowToastClose: true,
                hideAfter: 3000,
                stack: 10,
                position: 'bottom-right', 
                textAlign: 'left', 
                loader: true, 
                loaderBg: '#24b07b'
                });
                error=1;
            }
            if(error==0)
            {
                $('input').val('');
                $('textarea').val('');
                $('select').prop('selectedIndex',0);
                $.post("http://fuoficios.xyz/Home/saveForm", {name:name,mob:mob,email:email,course:course,refer:refer,msg:msg}, function(data) {
                    if(data==1)
                    {
                        $.toast({
                        text: "Por favor, escriba su teléfono.",
                        heading: 'Exitoso',
                        icon: 'success',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                    }else
                    {
                      $.toast({
                        text: "Inténtalo de nuevo.",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });  
                    }
                });
            }
            
        }
    </script>