<?php
if($data->num_rows()>0)
{
$id='';
$title='';
$stitle='';
$des='';
$cat='';
$type='';
$d='';
$img='';
$date='';
foreach($data->result() as $row)
{
    $id=$row->id;
    $title=$row->title;
    $stitle=$row->stitle;
    $des=$row->des;
    $cat=$row->cat;
    $type=$row->type;
    $d=$row->data;
    $img=$row->imgs;
    $date=$row->date;
}
$img=json_decode($img);
$d=json_decode($d);
?>
<style>
        .mainmenu-area {
            background: #171932!important;
            
        }
        .mb-5
        {
            padding-bottom:50px;
        }
        .mb-3
        {
            padding-bottom:30px;
        }
        .mt-5
        {
            padding-top:50px;
        }
        .admition-area
        {
            padding:25px;
        }
        .cont-out
        {
            background-size: cover;
            background-repeat: no-repeat;
        }
        .cont-in
        {
         background-color: #0000006e;   
        }
        .cont-in .admition-area h5
        {
           color:#fff;
        }
        .admition-area h5
        {
            color:#747b8c;
        }
        .second-table
        {
            display:flex;
        }
        .r1
        {
            width:20%;
        }
        .r2
        {
            width:80%;
        }
        #table2
        {
            display:none;
        }
        .row
        {
            display:flex;
        }
        @media only screen and (max-width: 575px) {
            .row
            {
                display:block;
            }
            .img-cont
            {
                height:250px;
            }
            .admition-area
            {
                padding:15px;
                
            }
            .top-table span
            {
                display:block;
            }
            .second-table
            {
                display:block;
                
            }
            .r1,.r2
            {
                width:100%;
            }
            #table1
            {
                display:none;
            }
            #table2
            {
                display:block;
            }
        }

</style>
<div style="background-image:url('<?php echo base_url().$img[0];?>');background-size: cover;background-repeat: no-repeat;">
<section class="section-padding" style="padding-bottom: 0px;background-color:#ffffff4a;">
    
    <div class="container mt-5">
        <div class="area-title text-center wow fadeIn">
            <h3 style="color:#000;"><?php echo $cat;?></h3>
            <h2 class="xs-font26" style="font-size: 36px;color:#000;"><?php echo $title;?></h2>
            <h4 class="xs-font26" style="color:#000;"><?php echo $stitle;?></h4>
            <table class="mt-5 table top-table"  id="table1" style="background-color:#ffffffad;text-align:left;">
                <tr>
                    <td><h4>Inicio: <span style="font-weight:500;"><?php echo date("d/m/Y", strtotime($d->start));?></span></h4></td>
                    <td><h4>Final: <span style="font-weight:500;"><?php echo date("d/m/Y", strtotime($d->end));?></span></h4></td>
                    <td><h4>Duración: <span style="font-weight:500;"><?php echo $d->dur." ".$d->durin;?></span></h4></td>
                </tr>
                <tr>
                    <td><div class="second-table"><div class="r1"><h4>Cursado:</h4></div>
                    <div class="r2">
                        <table class="table table-borderless" style="font-weight:500;color:#000;background-color:transparent;margin-top:-5px;margin-bottom:0px;">
                                        <?php
                                        foreach($d->daytime as $r)
                                        {
                                            ?>
                                            <tr>
                                                <td style="width:30%;border:0px;"><h4 style="font-weight:500;margin:0px;"><?php 
                                                switch($r->day)
                                                {
                                                    case 1:
                                                    echo "Lunes";
                                                    break;
                                                    case 2:
                                                    echo "Martes";
                                                    break;
                                                    case 3:
                                                    echo "Miércoles";
                                                    break;
                                                    case 4:
                                                    echo "Jueves";
                                                    break;
                                                    case 5:
                                                    echo "Viernes";
                                                    break;
                                                    case 6:
                                                    echo "Sábado";
                                                    break;
                                                }
                                                ?></h4></td>
                                                <td style="width:70%;border:0px;"><h4 style="font-weight:500;margin:0px;"><?php echo $r->from.' a '.$r->to;?> hs.</h4></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                        </tr>
                                </table>
                    </div></div></td>
                    <td><h4>Modalidad: <span style="font-weight:500;"><?php echo $type;?></span></h4></td>
                    <td><?php $date1=date_create($date);
                    $date2=date_create(date("Y-m-d"));
                    $diff=date_diff($date1,$date2);
                    $de=$diff->format("%R%a");
                    if($de<=0)
                    {
                    ?>
                            <div class="enroll-button center xs-center">
                                <a href="<?php 
                                if(!empty($this->session->userdata("fuouser")) && !empty($this->session->userdata("fuopass"))){ echo base_url().'Home/myaccount/'.$id;}else{ echo base_url().'Home/user/0/'.$id;}?>" style="width:180px;">Inscripciones Abiertas</a>
                            </div>
                            <?php
                            
                    }
                          ?></td>
                </tr>
            </table>
            <table class="mt-5 table top-table" id="table2" style="background-color:#ffffffad;text-align:left;">
                <tr>
                    <td><h4>Inicio: <span style="font-weight:500;"><?php echo date("d/m/Y", strtotime($d->start));?></span></h4></td>
                    <td><h4>Final: <span style="font-weight:500;"><?php echo date("d/m/Y", strtotime($d->end));?></span></h4></td>
                </tr>
                <tr>
                    <td><h4>Duración: <span style="font-weight:500;"><?php echo $d->dur." ".$d->durin;?></span></h4></td>
                    <td><div class="second-table"><div class="r1"><h4>Cursado:</h4></div>
                    <div class="r2">
                        <table class="table table-borderless" style="font-weight:500;color:#000;background-color:transparent;margin-top:-5px;margin-bottom:0px;">
                                        <?php
                                        foreach($d->daytime as $r)
                                        {
                                            ?>
                                            <tr>
                                                <td style="width:30%;border:0px;"><h4 style="font-weight:500;margin:0px;"><?php 
                                                switch($r->day)
                                                {
                                                    case 1:
                                                    echo "Lunes";
                                                    break;
                                                    case 2:
                                                    echo "Martes";
                                                    break;
                                                    case 3:
                                                    echo "Miércoles";
                                                    break;
                                                    case 4:
                                                    echo "Jueves";
                                                    break;
                                                    case 5:
                                                    echo "Viernes";
                                                    break;
                                                    case 6:
                                                    echo "Sábado";
                                                    break;
                                                }
                                                ?></h4></td>
                                                <td style="width:70%;border:0px;"><h4 style="font-weight:500;margin:0px;"><?php echo $r->from.' a '.$r->to;?> hs.</h4></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                        </tr>
                                </table>
                    </div></div></td>
                    </tr>
                    <tr>
                    <td><h4>Modalidad: <span style="font-weight:500;"><?php echo $type;?></span></h4></td>
                    <td><?php  $date1=date_create($date);
                    $date2=date_create(date("Y-m-d"));
                    $diff=date_diff($date1,$date2);
                    $de=$diff->format("%R%a");
                    if($de<=0)
                    {?>
                            <div class="enroll-button center xs-center">
                                <a href="<?php 
                                if(!empty($this->session->userdata("fuouser")) && !empty($this->session->userdata("fuopass"))){ echo base_url().'Home/myaccount/'.$id;}else{ echo base_url().'Home/user/0/'.$id;}?>" style="width:180px;">Inscripciones Abiertas</a>
                            </div>
                            <?php
                            
                    }
                          ?></td>
                </tr>
            </table>
        </div>
    </div>
</section>
</div>

<section class="section-padding" style="padding-bottom: 0px;padding-top: 50px;">
    
            <div class="area-title text-center wow fadeIn">
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="admition-area" style="text-align:justify;">
                            <p style="font-size:15px;color:#000;font-weight: 500;"><?php echo $des;?></p>
                            <br>
                            <block><b style="color:#000;">Dirigido a:</b></block>
                                <?php
                                foreach($d->elig as $r)
                                {
                                    echo "<h5 style='font-weight: 500;color:#000;'>".$r."</h5>";
                                }
                                ?>
                        </div>
                    </div>
                    <div class="col-md-6 img-cont" style="background-image:url('<?php echo base_url().$img[2];?>');background-size:cover;background-repeat:no-repeat;"></div>
                </div>
            </div>
            <div class="mt-3">
            <div class="area-title wow fadeIn" style="margin-bottom:50px;">
                <div class="row">
                    <div class="col-md-6 img-cont" style="background-image:url('<?php echo base_url().$img[1];?>');background-size:cover;background-repeat:no-repeat;"></div>
                    <div class="col-md-6 cont">
                        <div class="admition-area">
                        <h3 class="xs-font26" style="padding:10px;">Objetivo</h3>
                        <?php
                                foreach($d->obj as $r)
                                {
                                    echo "<h5 class='text-justify' style='font-weight: 500;color:#000;'>".$r."</h5>";
                                }
                                ?>
                        <h3 class="xs-font26">Contenidos</h3>
                        <div class="admition-area" style="text-align:justify;">
                            <ul>
                                <?php
                                foreach($d->cont as $r)
                                {
                                    echo "<li><h5 style='color:#000;'>".$r."</h5></li>";
                                }
                                ?>
                            </ul>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div style="background-image:url('<?php echo base_url();?>images/inst-back.png');background-size: cover;background-position: center;background-repeat: no-repeat;padding-top:30px;padding-bottom:30px;">
             <div style="background-color:#00000052;padding-top:40px;">
        <div class="container">
        <div class="mt-3">
            <div class="area-title text-center wow fadeIn">
                <h3 class="xs-font26" style="color:#fff;">Metodología</h3>
                        <div class="admition-area" style="text-align:center;">
                           <?php
                                        echo "<h4 style='font-weight:500;color:#fff;'>".$d->meth."</h5>";
                                        ?>
                        </div>
            </div>
        </div>
        <div class="mt-3">
            <div class="area-title text-center wow fadeIn" style="margin-bottom:40px;">
                <h3 class="xs-font26" style="color:#fff;">Beneficios</h3>
                        <div class="admition-area" style="text-align:center;">
                           <?php
                                        echo "<h4 style='font-weight:500;color:#fff;'>".$d->ben."</h5>";
                                    ?>
                        </div>
            </div>
        </div>
        </div>
        </div>
        </div>
        <div class="container">
        <div class="mt-5">
            <div class="area-title text-center wow fadeIn">
                <h3 class="xs-font26">Facilitadores</h3>
                            <div class="admition-area row">
                                <?php foreach($d->teacher as $row)
                                {
                                    if($row!="0")
                                    {
                                    $r=json_decode($row);
                                    ?>
                                    <div class="col-md-6 col-sm-12 col-xs-12 mx-auto text-center" style="margin-top:25px;">
                                    <div class="mx-auto" style="background-image:url('<?php echo base_url().$r->img;?>');height:250px;width:250px;background-position:center;background-size:cover;background-repeat:no-repeat;border-radius:250px;"></div>
                                    <div>
                                        <br>
                                        <h3><?php echo $r->name;?></h3>
                                        <p><?php echo substr($r->bio, 0, 255);?>...</p>
                                    </div>
                                    </div>
                                    <?php
                                    }
                                }
                                ?>
                                
                            </div>
                        
            </div>
        </div>
        <div class="mt-3">
            <div class="area-title text-center wow fadeIn">
                <h3 class="xs-font26">Apoyan al Proyecto</h3>
                            <div class="admition-area row"  style="display:flex;">
                                <?php foreach($d->pat as $row)
                                {
                                    ?>
                                    <div class="col-sm-3 mx-auto text-center" style="margin-top:15px;">
                                    <img src="<?php echo base_url().$row;?>" class="w-100">
                                    </div>
                                    <?php
                                }
                                ?>
                                
                            </div>
                        
            </div>
        </div>
    </div>
</section>
<?php
}
?>