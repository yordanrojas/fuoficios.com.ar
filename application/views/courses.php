    <style>
        .mainmenu-area {
            background: #171932!important;
        }
        .mb-5
        {
            padding-bottom:50px;
        }
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover
        {
            background-color: #24b07b;
            border-color: #24b07b;
        }
        .pagination>li>a, .pagination>li>span
        {
            color: #24b07b;
            border-color: #24b07b;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
    z-index: 2;
    color: #fff;
    background-color: #f18f71 ;
    border-color: #f18f71 ;
}
    </style>
    <!--ABOUT AREA-->
    <section class="section-padding about-area mb-5" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn mt-5">
                        <h2>Formación</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                foreach($courses as $row)
                {
                    $img=json_decode($row->imgs);
                    ?>
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12" style="cursor: pointer;">
                    <div class="single-course mb20" onclick="window.location.href='<?php echo base_url();?>Home/course/<?php echo $row->id;?>'">
                        <div style="background-image:url('<?php echo base_url().$img[0];?>');height:250px;background-position:center;background-size:cover;background-repeat:no-repeat;">
                            
                        </div>
                        <div class="course-details padding30">
                            <h4 class="font18" style="height:40px;"><?php echo $row->title;?></h4>
                            <p class="text-justify"><?php echo substr($row->des, 0, 100);?> ....</p>
                            <p class="mt30"><a class="enroll-button"><?php echo $row->cat;?></a>&nbsp;&nbsp;&nbsp;<a class="enroll-button"><?php echo $row->type;?></a></p>
                        </div>
                    </div>
                </div>
                    <?php
                }
                ?>
                
            </div>
            <div class="text-center"><?php echo $links; ?></div>
        </div>
    </section>
    <!--ABOUT AREA END-->