<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Feedback</title>
    </head>
    <body style="margin:0px">
        <!-- Normal form embed code, add an ID -->
        <iframe id="gform" src="https://docs.google.com/forms/d/e/1FAIpQLSc1FwljAdDDaYd54BE9BFM0k8CG_9iL5YmbofSjv5AHS3fNWw/viewform" width="100%" frameborder="0" marginheight="0" marginwidth="0" style="margin:0 auto; max-width:100%;height:100vh;">Loading…</iframe>
        <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
        <script type="text/javascript">
        
        var load = 0;
        document.getElementById('gform').onload = function(){
            /*Execute on every reload on iFrame*/
            load++;
            if(load > 2){
                $.post("<?php echo base_url();?>Home/updatefeed/",
                  {
                    id: '<?php echo $id?>',
                    stat: '0'
                  },
                  function(data, status){
                  });
            }
        }
        </script>
    </body>
</html>