<?php
    $link1='';
    $link2='';
    $link3='';
    $link4='';
    $link5='';
    $data5='';
    foreach($data->result() as $row)
    {
        $data5=$row->data5;
    }
    $data=json_decode($data5);
    $link1=$data->link1;
    $link2=$data->link2;
    $link3=$data->link3;
    $link4=$data->link4;
    $link5=$data->link5;
?>
<style>
    .foot-icon
    {
        background: #d3ffea none repeat scroll 0 0;
        border-radius: 30px;
        height: 60px;
        margin-bottom: 20px;
        padding-top: 15px;
        text-align: center;
        width: 60px;
        font-size:30px;
    }
    .foot-icon a
    {
        color: #31dc89;
    }
    .foot-icon:hover 
    {
        background-color:#fff;
    }
    .foot-icon a:hover
    {
        color:#f18f71;
    }
    .cclogo i
    {
        font-size:3.3rem;
        margin-left:5px;
        margin-right:5px;
    }
</style>
    <!--FOOER AREA-->
    <footer class="footer-area sky-gray-bg relative">
        <div class="footer-top-area padding-80-80 bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                        <div class="single-footer footer-about sm-mb50 xs-mb50 sm-center xs-center">
                            <div class="footer-logo mb30 text-center">
                                <a href="#"><img src="<?php echo base_url();?>assest/img/logo.png" alt=""></a>
                            </div>
                        </div>
                        <div class="single-footer footer-subscribe xs-center">
                            <h4 class="mb30 xs-font18 text-center white">Seguí con nosotros</h4>
                            <div style="display:flex;">
                            <div class="box-icon features-box-icon foot-icon" style="margin-left:auto;margin-right:auto;">
                            <a href="<?php echo $link1;?>" target="__new"><i class="fa fa-twitter"></i></a>
                            </div>
                            <div class="box-icon features-box-icon foot-icon" style="margin-left:auto;margin-right:auto;">
                            <a href="<?php echo $link2;?>" target="__new"><i class="fa fa-facebook"></i></a>
                            </div>
                            <div class="box-icon features-box-icon foot-icon" style="margin-left:auto;margin-right:auto;">
                            <a href="<?php echo $link3;?>" target="__new"><i class="fa fa-youtube"></i></a>
                            </div>
                            <div class="box-icon features-box-icon foot-icon" style="margin-left:auto;margin-right:auto;">
                            <a href="<?php echo $link4;?>" target="__new"><i class="fa fa-instagram"></i></a>
                            </div>
                            <div class="box-icon features-box-icon foot-icon" style="margin-left:auto;margin-right:auto;">
                            <a href="<?php echo $link5;?>" target="__new"><i class="fa fa-whatsapp"></i></a>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-4 col-xs-12">
                        <div class="single-footer footer-list white xs-center xs-mb50 text-center">
                            <ul>
                                <li><a href="<?php echo base_url();?>">Inicio</a></li>
                                <li><a href="<?php echo base_url();?>Home/institution">Institución</a></li>
                                <li><a href="<?php echo base_url();?>Home/courses">Formación</a></li>
                                <li><a href="http://fuonline.com.ar/" target="__new">FUOnline</a></li>
                                <?php
                                if(empty($this->session->userdata("fuouser")) && empty($this->session->userdata("tuser")))
                                {
                                    ?>
                                <li><a href="<?php echo base_url();?>Home/user">Ingresar</a></li>
                                <?php }?>
                                <li><a href="<?php echo base_url();?>Home/contact">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
                        <div class="single-footer footer-list white xs-center text-center">
                            <h4 class="mb20 xs-font18">Tarjetas Aceptadas</h4>
                            <ul>
                                <li><img src="<?php echo base_url();?>images/mobbex.png" style="height:50px;"></li>
                                <li><img src="<?php echo base_url();?>images/INSTITUCIONAL.png" style="height:50px;">&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>images/rapipago.png" style="height:60px;">&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>images/pagofacil.png" style="height:40px;"></li>
                                <li><img src="<?php echo base_url();?>images/mastercard-logo-7.png" style="height:50px;"></li>
                                <li><img src="<?php echo base_url();?>images/visa-logo (1).png" style="height:20px;"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-area white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--FOOER AREA END-->


    <!--====== SCRIPTS JS ======-->
    <script src="<?php echo base_url();?>assest/js/vendor/bootstrap.min.js"></script>

    <!--====== PLUGINS JS ======-->
    <script src="<?php echo base_url();?>assest/js/vendor/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url();?>assest/js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url();?>assest/js/vendor/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>assest/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assest/js/stellar.js"></script>
    <script src="<?php echo base_url();?>assest/js/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assest/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>assest/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assest/js/jquery-modal-video.min.js"></script>
    <script src="<?php echo base_url();?>assest/js/stellarnav.min.js"></script>
    <script src="<?php echo base_url();?>assest/js/placeholdem.min.js"></script>
    <script src="<?php echo base_url();?>assest/js/jquery.sticky.js"></script>
    <script src="<?php echo base_url();?>assest/js/jquery.toast.min.js"></script>

    <!--===== ACTIVE JS=====-->
    <script src="<?php echo base_url();?>assest/js/main.js"></script>
    <script>
        $(window).scrollTop();
    </script>

</body>

</html>
