<style>
     .mainmenu-area
     {
         background: #171932!important;
         
     }
</style>
    <section class="section-padding about-area mb-5" id="about" style="padding-bottom:50px;">
        <div class="container">
            <div class="row" style="margin-top:50px;">
                <?php if($stat==1){?>
                <div class="col-sm-3">
                </div>
                <div class="col-sm-6 area-title text-center">
                    <h3><?php echo $data?></h3>
                </div>
                <div class="col-sm-3">
                </div>
                <?php } ?>
                <?php if($stat==0){?>
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn mt-5" style="margin-bottom:50px;">
                        <h3>Establecer nueva contraseña</h3>
                    </div>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4 contact-form wow fadeIn mx-auto">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>Nueva contraseña</label>
                                                <input type="password" class="form-control" name="pass"  onchange="validPass(this)" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group mb0">
                                            <label></label>
                                            <button type="submit" onclick="chForm(this)">Cambia la contraseña</button>
                                        </div>
                                    </div>
                                </div>
                </div>
                <div class="col-sm-4"></div>
                <?php } ?>
            </div>
        </div>
    </section>
    
    <script>
    var epass=1;
        function validPass(t)
        {
            epass=1;
           if(t.value!='')
           {
               var passw=  /^[A-Za-z]\w{7,14}$/;
               if(!t.value.match(passw))
               {
                   $.toast({
                        text: "De 7 a 15 caracteres que contienen solo caracteres, dígitos numéricos, guiones bajos y el primer carácter debe ser una letra",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                        epass=1;
               }
               else
               {
                   epass=0;
               }
           }
        }
        
        function chForm(t)
        {
            $(t).attr("disabled", true);
            if(epass==1)
            {
                $.toast({
                        text: "la contraseña está vacía",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                        
                        $(t).attr("disabled", false);
            }
            else
            {
                $.post("<?php echo base_url();?>Home/updatePass/",
                  {
                    email: '<?php echo $data?>',
                    pass: $('input[name=pass]').val()
                  },
                  function(data, status){
                      if(status=='success')
                     {
                         if(data==1)
                         {
                             $.toast({
                            text: 'Contraseña cambiada con éxito',
                            heading: 'Exitoso',
                            icon: 'success',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 3000,
                            stack: 10,
                            position: 'bottom-right', 
                            textAlign: 'left', 
                            loader: true, 
                            loaderBg: '#24b07b'
                            });
                             window.location.replace("<?php echo base_url();?>");
                         }else
                         {
                        $.toast({
                        text: "Se produjo un error. ¡Por favor, inténtelo de nuevo más tarde!",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                         }
                         
                     }else
                     {
                         $.toast({
                        text: "Se produjo un error. ¡Por favor, inténtelo de nuevo más tarde!",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                     }
                     $(t).attr("disabled", false);
                  });
            }
        }
    </script>