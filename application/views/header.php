<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->

<head>
    <!--====== USEFULL META ======-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="La Fundación Universitaria de Oficios (FUO) está conformada por un equipo multidisciplinario de profesionales responsables y comprometidos con las personas y la comunidad que desean elevar su calidad de vida aprendiendo oficios. Somos una Fundación, creada por Mery y Ricardo Lunge en el año 2013, que ofrece servicios educativos al público en general, comunidades, pequeñas y medianas empresas." />
    <meta name="keywords" content="university,fuoficios,Fundación Universitaria de Oficios,Universitaria" />

    <!--====== TITLE TAG ======-->
    <title>Fundación Universitaria de Oficios</title>

    <!--====== FAVICON ICON =======-->
    <link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>favicon.ico" />

    <!--====== STYLESHEETS ======-->
    <link href="<?php echo base_url();?>assest/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assest/css/theme.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assest/css/icons.css" rel="stylesheet">

    <!--====== MAIN STYLESHEETS ======-->
    <link href="<?php echo base_url();?>style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assest/css/responsive.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assest/css/jquery.toast.min.css" rel="stylesheet">

    <script src="<?php echo base_url();?>assest/js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
    .preeloader
    {
        padding:50px;
    }
    .preeloader img
    {
        width:40%;
    }
    
    @media only screen and (min-width: 990px) {
    .navbar-brand img
    {
            -moz-transform: scale(2);
-webkit-transform: scale(2);
-o-transform: scale(2);
-ms-transform: scale(2);
transform: scale(2);
    }
    }
    @media only screen and (max-width: 425px) {
        .preeloader img
    {
        margin-top:10vh;
        width:100%;
    }
    .preeloader
    {
        padding:25px;
    }
    }
    </style>
        <!--====== SCRIPTS JS ======-->
    <script src="<?php echo base_url();?>assest/js/vendor/jquery-1.12.4.min.js"></script>
</head>

<body data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader text-center">
        <img src="<?php echo base_url();?>images/fuo.jpg">
    </div>

    <!--SCROLL TO TOP-->
    <a href="#scroolup" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header class="top-area" id="home">
        <div class="header-top-area" id="scroolup">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="<?php echo base_url();?>" class="navbar-brand"><img id="imglogo" src="<?php echo base_url();?>assest/img/logo.png" alt="fuo"></a>
                        </div>
                        <div id="main-nav" class="stellarnav">
                            <ul id="nav" class="nav navbar-nav pull-right">
                                <li <?php if($nav==1){ echo 'class="active"';}?>><a href="<?php echo base_url();?>">Inicio</a></li>
                                <li class="nav-item dropdown <?php if($nav==2){ echo 'active';}?>">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Institución</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?php echo base_url();?>Home/institution#about">Sobre Nosotros</a>
                                        <a class="dropdown-item" href="<?php echo base_url();?>Home/institution#vision">Visión</a>
                                        <a class="dropdown-item" href="<?php echo base_url();?>Home/institution#mision">Misión</a>
                                        <a class="dropdown-item" href="<?php echo base_url();?>Home/institution#valores">Valores</a>
                                        <a class="dropdown-item" href="<?php echo base_url();?>Home/institution#gestionamos">Gestionamos FUO</a>
                                        <a class="dropdown-item" href="<?php echo base_url();?>Home/institution#paraello">Para ello</a>
                                        <a class="dropdown-item" href="<?php echo base_url();?>Home/info">Noticias y Eventos</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown <?php if($nav==3){ echo 'active';}?>">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Formación</a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="<?php echo base_url();?>Home/courses">Todas</a>
                                            <?php foreach($this->UserData->getNav()->result() as $row)
                                            {
                                                if(!empty($row->area))
                                                {
                                                ?>
                                                <a class="dropdown-item" href="<?php echo base_url();?>Home/courses/?area=<?php echo $row->area;?>"><?php echo $row->area;?></a>
                                                <?php
                                                }
                                            }?>
                                            
                                        </div>
                                    </li>
                                <li <?php if($nav==4){ echo 'class="active"';}?>><a href="http://fuonline.com.ar/" target="__new">FUOnline</a></li>
                                <?php
                                if(!empty($this->session->userdata("fuouser")) && !empty($this->session->userdata("fuopass")))
                                {
                                    ?>
                                    <li class="nav-item dropdown <?php if($nav==5){ echo 'active';}?>">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mi cuenta</a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="<?php echo base_url();?>Home/myaccount">Mis cursos</a>
                                            <a class="dropdown-item" href="<?php echo base_url();?>Home/user/1">Cerrar sesión</a>
                                        </div>
                                    </li>
                                    <?php
                                }elseif(!empty($this->session->userdata("tuser")) && !empty($this->session->userdata("tpass")))
                                {
                                    ?><li <?php if($nav==5){ echo 'class="active"';}?>><a href="<?php echo base_url();?>Teacher">Ingresar</a></li><?php
                                }
                                else
                                {
                                ?>
                              <li <?php if($nav==5){ echo 'class="active"';}?>><a href="<?php echo base_url();?>Home/user">Ingresar</a></li>
                                <?php
                                }
                                ?>
                                <li <?php if($nav==6){ echo 'class="active"';}?>><a href="<?php echo base_url();?>Home/contact">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <!--END MAINMENU AREA END-->
        </div>
