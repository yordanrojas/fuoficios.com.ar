<style>
        .mainmenu-area {
            background: #171932!important;
        }
        .mt-5
        {
            padding-top:100px!important;
        }
        .mb-5
        {
            padding-bottom:50px;
        }
        </style>
    <!--COURSE AREA-->
    <section class="course-area padding-top mt-5" id="courses">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2 class="xs-font26">Noticias</h2>
                    </div>
                </div>
            </div>
            <div class="row course-list">
                <?php
                foreach($news->result() as $row)
                {
                    if($row->type==1)
                    {
                    ?>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20" onclick="window.location.href='<?php echo base_url();?>Home/viewInfo/<?php echo $row->id;?>'">
                        <div style="background-image:url('<?php echo base_url().$row->img;?>');height:250px;background-position:center;background-size:cover;background-repeat:no-repeat;">
                            
                        </div>
                        <div class="course-details padding30">
                            <h4 class="font18" style="height:40px;"><?php echo $row->title;?></h4>
                            <p class="text-justify" style="height:80px;"><?php echo substr($row->des, 0, 100);?> ....</p>
                        </div>
                    </div>
                </div>
                   <?php
                    }
                }
                ?>
            </div>
        </div>
        
    </section>
    <!--COURSE AREA END-->
        <section class="course-area padding-top" id="courses">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2 class="xs-font26">Eventos</h2>
                    </div>
                </div>
            </div>
            <div class="row course-list">
                <?php
                foreach($news->result() as $row)
                {
                    if($row->type==2)
                    {
                    ?>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20" onclick="window.location.href='<?php echo base_url();?>Home/viewInfo/<?php echo $row->id;?>'">
                        <div style="background-image:url('<?php echo base_url().$row->img;?>');height:250px;background-position:center;background-size:cover;background-repeat:no-repeat;">
                            
                        </div>
                        <div class="course-details padding30">
                            <h4 class="font18" style="height:40px;"><?php echo $row->title;?></h4>
                            <p class="text-justify" style="height:80px;"><?php echo substr($row->des, 0, 100);?> ....</p>
                        </div>
                    </div>
                </div>
                   <?php
                    }
                }
                ?>
            </div>
        </div>
        
    </section>