<?php
$abt='';
$img='';
$vision='';
$mission='';
$v1='';
$v2='';
$v3='';
$v4='';
$v5='';
$v6='';
$vd1='';
$vd2='';
$vd3='';
$vd4='';
$vd5='';
$vd6='';
$g1='';
$g2='';
$g3='';
$p1='';
$p2='';
$p3='';
$p4='';
$p5='';
$d1='';
$d2='';
$d3='';
$d4='';
$d5='';
foreach($data1->result() as $row)
{
    $d2=$row->data2;
}
$data=json_decode($d2);
$abt=$data->abt;
$img=$data->img;
foreach($data2->result() as $row)
{
    $d1=$row->data1;
    $d2=$row->data2;
    $d3=$row->data3;
    $d4=$row->data4;
    $d5=$row->data5;
}
$data=json_decode($d1);
$vision=$data->vission;
$data=json_decode($d2);
$mission=$data->mission;
$data=json_decode($d3);
$v1=$data->v1;
$v2=$data->v2;
$v3=$data->v3;
$v4=$data->v4;
$v5=$data->v5;
$v6=$data->v6;
$vd1=$data->vd1;
$vd2=$data->vd2;
$vd3=$data->vd3;
$vd4=$data->vd4;
$vd5=$data->vd5;
$vd6=$data->vd6;
$data=json_decode($d4);
$g1=$data->g1;
$g2=$data->g2;
$g3=$data->g3;
$data=json_decode($d5);
$p1=$data->p1;
$p2=$data->p2;
$p3=$data->p3;
$p4=$data->p4;
$p5=$data->p5;
?>
    <style>
        .mainmenu-area {
            background: #171932!important;
            
        }
        .mb-5
        {
            padding-bottom:50px;
        }
    </style>
    <!--ABOUT AREA-->
    <section class="section-padding about-area mb-5" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn mt-5">
                        <h2>Sobre Nosotros</h2>
                    </div>
                </div>
            </div>
            <div class="row flex-v-center">
                <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                    <div class="about-content xs-mb50 wow fadeIn">
                        <p class="text-justify"><?php echo $abt;?></p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
                    <img src="<?php echo base_url().$img;?>" class="w-100" style="border-radius:15px;">
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT AREA END-->
    
        <!--ABOUT TOP CONTENT AREA-->
    <section class="bg-theme section-padding" id="vision">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="text-center wow fadeIn">
                        <h2 class="xs-font20">Visión</h2>
                        <p class="text-justify"><?php echo $vision;?></p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="text-center wow fadeIn">
                        <h2 class="xs-font20">Misión</h2>
                        <p class="text-justify"><?php echo $mission;?></p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--ABOUT TOP CONTENT AREA END-->

       <!--ABOUT TOP CONTENT AREA-->
    <section class="section-padding" style="padding-bottom:0px;" id="valores">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="text-center wow fadeIn">
                        <h2 class="xs-font20">Valores</h2>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title"><?php echo $v1;?></h3>
                        <p class="text-justify"><?php echo $vd1;?></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title"><?php echo $v2;?></h3>
                        <p class="text-justify"><?php echo $vd2;?></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title"><?php echo $v3;?></h3>
                        <p class="text-justify"><?php echo $vd3;?></p>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title"><?php echo $v4;?></h3>
                        <p class="text-justify"><?php echo $vd4;?></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title"><?php echo $v5;?></h3>
                        <p class="text-justify"><?php echo $vd5;?></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title"><?php echo $v6;?></h3>
                        <p class="text-justify"><?php echo $vd6;?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT TOP CONTENT AREA END-->
    
    
            <!--ABOUT TOP CONTENT AREA-->
    <section class="bg-theme section-padding" style="padding-bottom:0px;" id="gestionamos">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="wow fadeIn">
                        <h2 class="xs-font20 text-center">Gestionamos FUO</h2>
                        <div class="row mt-5">
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                                    <p class="text-justify"><?php echo $g1;?></p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                                    <p class="text-justify"><?php echo $g2;?></p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                                    <p class="text-justify"><?php echo $g3;?></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT TOP CONTENT AREA END-->
        
    
            <!--ABOUT TOP CONTENT AREA-->
    <section class="section-padding" style="padding-bottom:0px;" id="paraello">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="wow fadeIn">
                        <h2 class="xs-font20 text-center">Para ello</h2>
                        <div class="row mt-5">
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                                    <p class="text-justify"><?php echo $p1;?></p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                                    <p class="text-justify"><?php echo $p2;?></p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                                    <p class="text-justify"><?php echo $p3;?></p>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                                    <p class="text-justify"><?php echo $p4;?></p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                                    <p class="text-justify"><?php echo $p5;?></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT TOP CONTENT AREA END-->