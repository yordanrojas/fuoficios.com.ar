    <style>
    .fog-form
    {
        display:none;
    }
        .mainmenu-area {
            background: #171932!important;
        }
        .mb-5
        {
            padding-bottom:50px;
        }

        
        input:-webkit-autofill, input:-webkit-autofill:hover, input:-webkit-autofill:focus {
    -webkit-box-shadow: 0 0 0 1px #cacfda!important;
    box-shadow: 0 0 0 1px #cacfda!important;
            
        }
        .emailvalid
        {
            display:none;
        }
        .emailvalid a
        {
                background: #3ee792 none repeat scroll 0 0;
    border: 0 none;
    border-radius: 50px;
    color: #ffffff;
    font-size: 14px;
    letter-spacing: 2px;
    padding: 13px 20px;
    text-transform: capitalize;
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    cursor: pointer;
        }
    </style>
    <!--ABOUT AREA-->
    <section class="section-padding about-area mb-5" id="about">
       
        <div class="container">
            <div class="row fog-form">
                <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn mt-5">
                        <h2>Restablecer la contraseña</h2>

                    </div>
                </div>
            </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4 contact-form wow fadeIn mx-auto">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>Tu Email</label>
                                                <input type="text" class="form-control" name="femail"  onchange="fogEmail(this)" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group mb0">
                                            <label></label>
                                            <button type="submit" onclick="fogForm(this)">iniciar sesión</button>
                                        </div>
                                    </div>
                                </div>
                </div>
                <div class="col-sm-4"></div>
            </div>
            <div class="row log-form">
                <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn mt-5">
                        <h2>Iniciar Sesión</h2>

                    </div>
                </div>
            </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4 contact-form wow fadeIn mx-auto">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>Tu Email</label>
                                                <input type="text" class="form-control" name="email"  onchange="logEmail(this)" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>Contraseña</label>
                                                <input type="password" class="form-control" name="lpass" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <a style="cursor:pointer;" onclick="showFog()">¿Se te olvidó tu contraseña?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group mb0">
                                            <label></label>
                                            <button type="submit" onclick="loginForm(this)">iniciar sesión</button>
                                        </div>
                                    </div>
                                </div>
                </div>
                <div class="col-sm-4"></div>
            </div>
            </section>
            
<script>

        function showFog()
        {
            $(".log-form").hide();
            $(".fog-form").show();
        }
        function dspError(m)
        {
            $.toast({
                        text: m,
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
        }
        
        var logem=0;
        function logEmail(t)
        {
            if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(t.value))
            {
                logem=1;
                dspError('Ingrese un email valido');
            }else
            {
                logem=0;
            }
        }
        
        function loginForm(t)
        {
            
            if($('input[name=lpass]').val()=='')
            {
                dspError('Ingrese una contraseña válida');
                
            }else if(logem==1)
            {
                dspError('Ingrese un email valido');
                
            }else
            {
                $(t).attr("disabled", true);
                $.post("<?php echo base_url();?>Home/loginData/",
                  {
                    email: $('input[name=email]').val(),
                    pass:$('input[name=lpass]').val()
                  },
                  function(data, status){
                      $(t).attr("disabled", false);
                     if(status=='success')
                     {
                         var d=JSON.parse(data);
                         if(d.status==1)
                         {
                             $.toast({
                            text: d.data,
                            heading: 'Exitoso',
                            icon: 'success',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 3000,
                            stack: 10,
                            position: 'bottom-right', 
                            textAlign: 'left', 
                            loader: true, 
                            loaderBg: '#24b07b'
                            });
                             window.location.replace("<?php echo base_url();?>Home/myaccount");
                         }else if(d.status==2)
                         {
                             $.toast({
                            text: d.data,
                            heading: 'Exitoso',
                            icon: 'success',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 3000,
                            stack: 10,
                            position: 'bottom-right', 
                            textAlign: 'left', 
                            loader: true, 
                            loaderBg: '#24b07b'
                            });
                             window.location.replace("<?php echo base_url();?>Teacher");
                         }else
                         {
                             dspError(d.data);
                         }
                     }else
                     {
                         dspError('Se produjo un error. ¡Por favor, inténtelo de nuevo más tarde!');
                     }
                  });
            }
        }
            var fogem=0;
        function fogEmail(t)
        {
            if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(t.value))
            {
                fogem=1;
                dspError('Ingrese un email valido');
            }else
            {
                fogem=0;
            }
        }
        function fogForm(t)
        {
             if(fogem==1)
            {
                dspError('Ingrese un email valido');
                
            }else
            {
                $(t).attr("disabled", true);
                $.post("<?php echo base_url();?>Home/forgotData/",
                  {
                    email: $('input[name=femail]').val()
                  },
                  function(data, status){
                      $(t).attr("disabled", false);
                     if(status=='success')
                     {
                         var d=JSON.parse(data);
                         if(d.status==1)
                         {
                             $.toast({
                            text: d.data,
                            heading: 'Exitoso',
                            icon: 'success',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 3000,
                            stack: 10,
                            position: 'bottom-right', 
                            textAlign: 'left', 
                            loader: true, 
                            loaderBg: '#24b07b'
                            });
                            $(".log-form").show();
                            $(".fog-form").hide();
                         }else
                         {
                             dspError(d.data);
                         }
                     }else
                     {
                         dspError('Se produjo un error. ¡Por favor, inténtelo de nuevo más tarde!');
                     }
                  });
            }
        }
    
</script>