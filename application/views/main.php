<style>
    .navbar
    {
        background-color:#1719329c;
    }
</style>
<?php
    $feat1='';
    $feat2='';
    $feat3='';
    $fdes1='';
    $fdes2='';
    $fdes3='';
    $abt='';
    $count1='';
    $count2='';
    $count3='';
    $why='';
    $vwhy='';
    $link1='';
    $link2='';
    $link3='';
    $link4='';
    $link5='';
    $data1='';
    $data2='';
    $data3='';
    $data4='';
    $data5='';
    foreach($data->result() as $row)
    {
        $data1=$row->data1;
        $data2=$row->data2;
        $data3=$row->data3;
        $data4=$row->data4;
        $data5=$row->data5;
    }
    $data=json_decode($data1);
    $feat1=$data->feat1;
    $feat2=$data->feat2;
    $feat3=$data->feat3;
    $fdes1=$data->des1;
    $fdes2=$data->des2;
    $fdes3=$data->des3;
    $data=json_decode($data2);
    $abt=$data->abt;
    $data=json_decode($data3);
    $count1=$data->count1;
    $count2=$data->count2;
    $count3=$data->count3;
    $data=json_decode($data4);
    $why=$data->why;
    $vwhy=$data->vwhy;
    $data=json_decode($data5);
    $link1=$data->link1;
    $link2=$data->link2;
    $link3=$data->link3;
    $link4=$data->link4;
    $link5=$data->link5;
?>
  <!--WELCOME SLIDER AREA-->
<?php
if(!empty($tslider))
{
    if($tslider->num_rows()>1)
    {
        ?>
        <div class="welcome-slider-area white font16">
        <?php
        foreach($tslider->result() as $row)
        {
            if($row->hide==0)
            {
            ?>
                <div class="welcome-single-slide">
                    <div class="slide-bg-one slide-bg-overlay" style="background-image:url('<?php echo base_url().$row->img;?>');"></div>
                    <div class="welcome-area">
                        <div class="container">
                            <div class="row flex-v-center">
                                <div class="col-md-8 col-lg-7 col-sm-12 col-xs-12">
                                    <div class="welcome-text">
                                        <h1><?php echo $row->title;?></h1>
                                        <p><?php echo $row->des;?></p>
                                        <div class="home-button enroll-button">
                                            <a href="<?php echo base_url();?>Home/courses" style="border:0px;">Ver Cursos</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
        ?>
        </div>
        <?php
    }
}
?>
        <!--WELCOME SLIDER AREA END-->
    </header>
    <!--END TOP AREA-->

    <!--FEATURES TOP AREA-->
    <section class="features-top-area" id="features">
        <div class="container">
            <div class="row promo-content">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.1s">
                        <div class="box-icon features-box-icon">
                            <i class="icofont icofont-certificate"></i>
                        </div>
                        <h3 class="box-title"><?php echo $feat1;?></h3>
                        <p class="text-justify"><?php echo $fdes1;?></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0  wow fadeInUp padding30" data-wow-delay="0.2s">
                        <div class="box-icon features-box-icon">
                            <i class="icofont icofont-people"></i>
                        </div>
                        <h3 class="box-title"><?php echo $feat2;?></h3>
                        <p class="text-justify"><?php echo $fdes2;?></p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.3s">
                        <div class="box-icon features-box-icon">
                            <i class="icofont icofont-ui-browser"></i>
                        </div>
                        <h3 class="box-title"><?php echo $feat3;?></h3>
                        <p class="text-justify"><?php echo $fdes3;?></p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--FEATURES TOP AREA END-->

    <!--ABOUT TOP CONTENT AREA-->
    <section class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="text-center wow fadeIn">
                        <h2 class="xs-font20">¿Quiénes Somos?</h2>
                        <p><?php echo $abt;?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT TOP CONTENT AREA END-->

    <!--FUN FACT AREA AREA-->
    <section class="fun-fact-area center white relative padding-100-70" id="fact">
        <div class="area-bg" data-stellar-background-ratio="0.6"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="font60 xs-font26"><span class="counter"><?php echo $count1;?></span></h3>
                        <p class="font600">Estudiantes Graduados</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.2s">
                        <h3 class="font60 xs-font26"><span class="counter"><?php echo $count2;?></span></h3>
                        <p class="font600">Profesores Expertos</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.3s">
                        <h3 class="font60 xs-font26"><span class="counter"><?php echo $count3;?></span></h3>
                        <p class="font600">Nuestros Cursos</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FUN FACT AREA AREA END-->

    <!--COURSE AREA-->
    <section class="course-area padding-top" id="courses">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2 class="xs-font26">Nuestros Cursos</h2>
                    </div>
                </div>
            </div>
            <div class="row course-list">
                <?php
                foreach($courses->result() as $row)
                {
                    $img=json_decode($row->imgs);
                    ?>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20" onclick="window.location.href='<?php echo base_url();?>Home/course/<?php echo $row->id;?>'">
                        <div style="background-image:url('<?php echo base_url().$img[0];?>');height:250px;background-position:center;background-size:cover;background-repeat:no-repeat;">
                            
                        </div>
                        <div class="course-details padding30">
                            <h4 class="font18" style="height:40px;"><?php echo $row->title;?></h4>
                            <p class="text-justify" style="height:80px;"><?php echo substr($row->des, 0, 100);?> ....</p>
                            <p class="mt30"><a class="enroll-button"><?php echo $row->cat;?></a>&nbsp;&nbsp;&nbsp;<a class="enroll-button"><?php echo $row->type;?></a></p>
                        </div>
                    </div>
                </div>
                   <?php
                }
                ?>
            </div>
        </div>
    </section>
    <!--COURSE AREA END-->

    <!--ABOUT AREA-->
    <section class="section-padding about-area" id="about">
        <div class="container">
            <div class="row flex-v-center">
                <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                    <div class="about-content xs-mb50 wow fadeIn">
                        <h3 class="xs-font20 mb30">¿Por qué FUO?</h3>
                        <p class="text-justify"><?php echo $why;?></p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
                    <div class="video-promo-details wow fadeIn">
                        <div class="video-promo-content">
                            <span data-video-id="<?php echo $vwhy;?>" class="video-area-popup mb30"><i class="fa fa-play"></i></span>
                        </div>
                        <img src="assest/img/about/about.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT AREA END-->


    <!--CONTACT US AREA-->
    <section class="contact-area sky-gray-bg" id="contact">
        <div class="container-fluid no-padding">
            <div class="row no-margin">
                <div class="no-padding col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="map-area relative">
                        <div id="map" style="width: 100%; height: 600px;">
                            <iframe width="100%" height="600" id="gmap_canvas" src="https://maps.google.com/maps?q=Fundación Universitaria de Oficios&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="no-padding col-md-5 col-lg-5 col-sm-12 col-xs-12">
                    <div class="contact-form-content padding50 xs-padding20" style="padding-top:0px;">
                        <div class="contact-title wow fadeIn">
                            <h3 class="font28 mb50 xs-mb30 xs-font22 xs-mt20">¿Alguna pregunta en mente? Contáctanos.</h3>
                        </div>
                        <div class="contact-form wow fadeIn">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" name="name" placeholder="Tu Nombre" required >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" name="mob" placeholder="Tu Teléfono">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" name="email" placeholder="Tu Email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <select class="form-control" id="formselect1" name="form-phone">
                                                    <option value="{}">Tipo de Consulta</option>
                                                    <?php
                                                    foreach($form->result() as $row)
                                                    {
                                                        echo "<option value='".json_encode($row)."'>".$row->cat.":".$row->title."(".$row->type.")</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <select class="form-control" id="formselect2" name="form-phone">
                                                    <option value="0">Cómo nos conociste?</option>
                                                    <option value="Amigo">Amigo</option>
                                                    <option value="En la FUO">En la FUO</option>
                                                    <option value="Folletería">Folletería</option>
                                                    <option value="Prensa Gráfica">Prensa Gráfica</option>
                                                    <option value="Radio">Radio</option>
                                                    <option value="Redes Sociales">Redes Sociales</option>
                                                    <option value="Televisión">Televisión</option>
                                                    <option value="Web">Web</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="message-field">
                                            <div class="form-input">
                                                <textarea class="form-control" rows="3" name="msg" id="msg" placeholder="Mensaje"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group mb0">
                                            <button type="submit" onclick="sendForm()">Enviar Mensaje</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->
    <?php
    if(!empty($bslider))
    {
        if($bslider->num_rows()>1)
        {
            ?>
            <!--CLIENT AREA-->
                <div class="client-area  padding-bottom mt100 sm-mt10 xs-mt0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <div class="client-slider">
            <?php
            foreach($bslider->result() as $row)
            {
                ?>
                <div class="single-client">
                    <img src="<?php echo base_url().$row->img;?>" alt="<?php echo $row->title;?>" style="height:80px;">
                </div>
                <?php
            }
            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--CLIENT AREA END-->
            <?php
        }
    }
    ?>
    <script>
        function sendForm()
        {
            var error=0;
            var name=$("input[name='name']").val();
            var mob=$("input[name='mob']").val();
            var email=$("input[name='email']").val();
            var course=$("#formselect1 option:selected").val();
            var refer=$("#formselect2 option:selected").val();
            var msg=$('#msg').val();
            if(name.length < 5 || name =='Tu Nombre')
            {
                $.toast({
                text: "Por favor, escriba su nombre.",
                heading: 'Error',
                icon: 'error',
                showHideTransition: 'fade',
                allowToastClose: true,
                hideAfter: 3000,
                stack: 10,
                position: 'bottom-right', 
                textAlign: 'left', 
                loader: true, 
                loaderBg: '#24b07b'
                });
                error=1;
            }
            if(mob.length < 9 || mob =='Tu Teléfono')
            {
                $.toast({
                text: "Por favor, escriba su teléfono.",
                heading: 'Error',
                icon: 'error',
                showHideTransition: 'fade',
                allowToastClose: true,
                hideAfter: 3000,
                stack: 10,
                position: 'bottom-right', 
                textAlign: 'left', 
                loader: true, 
                loaderBg: '#24b07b'
                });
                error=1;
            }
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email) || email =='Tu Email')
            {
                $.toast({
                text: "Por favor, escriba su teléfono.",
                heading: 'Error',
                icon: 'error',
                showHideTransition: 'fade',
                allowToastClose: true,
                hideAfter: 3000,
                stack: 10,
                position: 'bottom-right', 
                textAlign: 'left', 
                loader: true, 
                loaderBg: '#24b07b'
                });
                error=1;
            }
            if(error==0)
            {
                $('input').val('');
                $('textarea').val('');
                $('select').prop('selectedIndex',0);
                $.post("https://fuoficios.com.ar/Home/saveForm", {name:name,mob:mob,email:email,course:course,refer:refer,msg:msg}, function(data) {
                    if(data==1)
                    {
                        $.toast({
                        text: "Por favor, escriba su teléfono.",
                        heading: 'Exitoso',
                        icon: 'success',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                    }else
                    {
                      $.toast({
                        text: "Inténtalo de nuevo.",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });  
                    }
                });
            }
            
        }
    </script>
