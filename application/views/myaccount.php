<style>
     .mainmenu-area
     {
         background: #171932!important;
         
     }
     .mt-5
     {
         margin-top:50px;
     }
     #table2
        {
            display:none;
        }
        .single-course
        {
            display:flex;
        }
        .rm-but
        {
            background-color:#ff00008c!important;
            color:#fff!important;
        }
        @media only screen and (max-width: 767px) {
            .single-course
        {
            display:block;
        }
        .bg-img
        {
            height:250px;
        }
        }
        @media only screen and (max-width: 575px) {
            #table1
            {
                display:none;
            }
            #table2
            {
                display:block;
            }
        }
        @media only screen and (max-width: 425px) {
            #table2 td
            {
                padding:2px;
            }
            #table2 td h4
            {
                font-size:1.2rem;
            }
            #table2
            {
                margin-top:10px;
            }
        }
</style>
    <section class="section-padding about-area mb-5" id="about" style="padding-bottom:50px;">
        <div class="container mt-5">
            <div class="col-sm-12 area-title ">
                <h3>Mis Cursos</h3>
                <?php
                if($cart->num_rows()>0)
            {
                ?>
                    <div class="row">
                    <?php
                    foreach($cart->result() as $row)
                    {
                        $img=json_decode($row->imgs);
                        $d=json_decode($row->data);
                        ?>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="single-course mb20">
                            <div class="col-sm-3 col-xs-12 bg-img" style="background-image:url('<?php echo base_url().$img[0];?>');background-position:center;background-size:cover;background-repeat:no-repeat;">
                                
                            </div>
                            <div class="course-details padding30 col-sm-9 col-xs-12">
                                <h4 class="font18" style="height:60px;cursor: pointer;"  onclick="window.location.href='<?php echo base_url();?>Home/course/<?php echo $row->id;?>'"><?php echo $row->title;?></h4>
                                <table class="table top-table"  id="table1" style="background-color:#ffffffad;text-align:left;">
                                    <tr>
                                        <td><h4>Inicio: <span style="font-weight:500;"><?php echo date("d/m/Y", strtotime($d->start));?></span></h4></td>
                                        <td><h4>Final: <span style="font-weight:500;"><?php echo date("d/m/Y", strtotime($d->end));?></span></h4></td>
                                        <td><h4>Duración: <span style="font-weight:500;"><?php echo $d->dur." ".$d->durin;?></span></h4></td>
                                    </tr>
                                    <tr>
                                        <td><div class="second-table"><div class="r1"><h4>Cursado:</h4></div>
                                        <div class="r2">
                                            <table class="table table-borderless" style="font-weight:500;color:#000;background-color:transparent;margin-top:-5px;margin-bottom:0px;">
                                                            <?php
                                                            foreach($d->daytime as $r)
                                                            {
                                                                ?>
                                                                <tr>
                                                                    <td style="width:30%;border:0px;"><h4 style="font-weight:500;margin:0px;"><?php 
                                                                    switch($r->day)
                                                                    {
                                                                        case 1:
                                                                        echo "Lunes";
                                                                        break;
                                                                        case 2:
                                                                        echo "Martes";
                                                                        break;
                                                                        case 3:
                                                                        echo "Miércoles";
                                                                        break;
                                                                        case 4:
                                                                        echo "Jueves";
                                                                        break;
                                                                        case 5:
                                                                        echo "Viernes";
                                                                        break;
                                                                        case 6:
                                                                        echo "Sábado";
                                                                        break;
                                                                    }
                                                                    ?></h4></td>
                                                                    <td style="width:70%;border:0px;"><h4 style="font-weight:500;margin:0px;"><?php echo $r->from.' a '.$r->to;?> hs.</h4></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                            <tr>
                                                            </tr>
                                                    </table>
                                        </div></div></td>
                                        <td><h4>Modalidad: <span style="font-weight:500;"><?php echo $row->type;?></span></h4></td>
                                        <td><h4>Precio: <span style="font-weight:500;">	&#36;<?php echo $row->price;?></span></h4></td>
                                    </tr>
                                </table>
                                <table class="mt-5 table top-table" id="table2" style="background-color:#ffffffad;text-align:left;">
                                    <tr>
                                        <td><h4>Inicio: <span style="font-weight:500;"><?php echo date("d/m/Y", strtotime($d->start));?></span></h4></td>
                                        <td><h4>Final: <span style="font-weight:500;"><?php echo date("d/m/Y", strtotime($d->end));?></span></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Duración: <span style="font-weight:500;"><?php echo $d->dur." ".$d->durin;?></span></h4></td>
                                        <td><div class="second-table"><div class="r1"><h4>Cursado:</h4></div>
                                        <div class="r2">
                                            <table class="table table-borderless" style="font-weight:500;color:#000;background-color:transparent;margin-top:-5px;margin-bottom:0px;">
                                                            <?php
                                                            foreach($d->daytime as $r)
                                                            {
                                                                ?>
                                                                <tr>
                                                                    <td style="width:30%;border:0px;"><h4 style="font-weight:500;margin:0px;"><?php 
                                                                    switch($r->day)
                                                                    {
                                                                        case 1:
                                                                        echo "Lunes";
                                                                        break;
                                                                        case 2:
                                                                        echo "Martes";
                                                                        break;
                                                                        case 3:
                                                                        echo "Miércoles";
                                                                        break;
                                                                        case 4:
                                                                        echo "Jueves";
                                                                        break;
                                                                        case 5:
                                                                        echo "Viernes";
                                                                        break;
                                                                        case 6:
                                                                        echo "Sábado";
                                                                        break;
                                                                    }
                                                                    ?></h4></td>
                                                                    <td style="width:70%;border:0px;"><h4 style="font-weight:500;margin:0px;"><?php echo $r->from.' a '.$r->to;?> hs.</h4></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                            <tr>
                                                            </tr>
                                                    </table>
                                        </div></div></td>
                                        </tr>
                                        <tr>
                                        <td><h4>Modalidad: <span style="font-weight:500;"><?php echo $row->type;?></span></h4></td>
                                        <td><h4>Precio: <span style="font-weight:500;">	$ <?php echo $row->price;?></span></h4></td>
                                    </tr>
                                </table>
                                <a class="enroll-button rm-but" style="float:left;" href="<?php echo base_url().'Home/myaccount/?remove='.$row->crid;?>">Eliminar</a>
                                <?php
                                    $date1=date_create($row->date);
                                    $date2=date_create(date("Y-m-d"));
                                    $diff=date_diff($date1,$date2);
                                    $de=$diff->format("%R%a");
                                    if($de<=0)
                                    {
                                ?>
                                <a class="enroll-button" style="float:right;" href="<?php echo base_url().'Home/payment/'.$row->crid;?>">Pagar</a>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                    ?>
                    
                </div>
                <?php
                }
                ?>
            </div>
            <?php
            if($pen->num_rows()>0)
            {
                ?><div class="col-sm-12 area-title ">
                    <h3>Transacción en espera</h3>
                    <div class="row">
                    <?php
                    foreach($pen->result() as $row)
                    {
                        $img=json_decode($row->imgs);
                        $d=json_decode($row->data);
                        ?>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="single-course mb20">
                            <div class="col-sm-3 col-xs-12 bg-img" style="background-image:url('<?php echo base_url().$img[0];?>');background-position:center;background-size:cover;background-repeat:no-repeat;">
                                
                            </div>
                            <div class="course-details padding30 col-sm-9 col-xs-12">
                                <h4 class="font18" style="height:40px;cursor: pointer;"  onclick="window.location.href='<?php echo base_url();?>Home/course/<?php echo $row->id;?>'"><?php echo $row->title;?></h4>
                                <table class="table top-table"  id="table1" style="background-color:#ffffffad;text-align:left;">
                                    <tr>
                                        <td><h4>Estado: <span style="font-weight:500;">Transacción en espera</span></h4></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <table class="mt-5 table top-table" id="table2" style="background-color:#ffffffad;text-align:left;">
                                    <tr>
                                        <td><h4>Estado: <span style="font-weight:500;">Transacción en espera</span></h4></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                    ?>
                    
                </div>
            </div>
            <?php
            }
            ?>
            <div class="col-sm-12 area-title ">
                    
                    <?php
                    if($enroll->num_rows()>0)
                    {
                     ?>
                     <div class="row">
                    <?php
                    foreach($enroll->result() as $row)
                    {
                        $img=json_decode($row->imgs);
                        $d='';
                        if(!empty($row->data))
                        {
                            $d=json_decode($row->data);
                        }
                        ?>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="single-course mb20">
                            <div class="col-sm-3 col-xs-12 bg-img" style="background-image:url('<?php echo base_url().$img[0];?>');background-position:center;background-size:cover;background-repeat:no-repeat;">
                                
                            </div>
                            <div class="course-details padding30 col-sm-9 col-xs-12">
                                <h4 class="font18" style="height:40px;cursor: pointer;"  onclick="window.location.href='<?php echo base_url();?>Home/course/<?php echo $row->cid;?>'"><?php echo $row->title;?></h4>
                                <table class="table top-table"  id="table1" style="background-color:#ffffffad;text-align:left;">
                                    <tr>
                                        <td><h4>Estado: <span style="font-weight:500;">Pagado</span></h4></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <table class="mt-5 table top-table" id="table2" style="background-color:#ffffffad;text-align:left;">
                                    <tr>
                                        <td><h4>Estado: <span style="font-weight:500;">Pagado</span></h4></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                    ?>
                    
                </div>
                     <?php 
                    }
                    ?>
            </div>
        </div>
    </section>
    <script src="<?php echo base_url();?>assest/js/jquery.toast.min.js"></script>
    <script>

        function dspError(m)
        {
            $.toast({
                        text: m,
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
        }
        function dspMsg(m)
        {
            $.toast({
                            text: m,
                            heading: 'Exitoso',
                            icon: 'success',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 3000,
                            stack: 10,
                            position: 'bottom-right', 
                            textAlign: 'left', 
                            loader: true, 
                            loaderBg: '#24b07b'
                            });
        }
        
            <?php
    if(isset($_GET['pay']))
    {
        if($_GET['pay']==0)
        {
            ?>
            dspError('Procesamiento de pago fallido');
            <?php
        }
    }
    
    if(!empty($msg))
    {
        ?>
            dspError('<?php echo $msg;?>');
            <?php
    }
    ?>
    </script>