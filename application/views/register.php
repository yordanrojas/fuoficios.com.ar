    <style>
        .mainmenu-area {
            background: #171932!important;
        }
        .mb-5
        {
            padding-bottom:50px;
        }
        
        input:-webkit-autofill, input:-webkit-autofill:hover, input:-webkit-autofill:focus {
    -webkit-box-shadow: 0 0 0 1px #cacfda!important;
    box-shadow: 0 0 0 1px #cacfda!important;
            
        }
        .emailvalid
        {
            display:none;
        }
        .emailvalid a
        {
                background: #3ee792 none repeat scroll 0 0;
    border: 0 none;
    border-radius: 50px;
    color: #ffffff;
    font-size: 14px;
    letter-spacing: 2px;
    padding: 13px 20px;
    text-transform: capitalize;
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    cursor: pointer;
        }
    </style>
    <!--ABOUT AREA-->
    <section class="section-padding about-area mb-5" id="about">
       
        <div class="container">

            <div class="row reg-form">
                            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn mt-5">
                        <h2>Registro</h2>
                         <?php 
        if(!empty($_SESSION['emsg']))
        {
            echo $_SESSION['emsg'];
            $_SESSION['emsg']='';
        }
        ?>
                    </div>
                </div>
            </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-8 contact-form wow fadeIn mx-auto">
                            <form action="<?php echo base_url();?>Home/registerStudent" id="reg-form" method="post">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <label>Tu Nombre</label>
                                                <input type="text" class="form-control" name="first" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="email-field">
                                            <div class="form-input">
                                                <label>Tu Apellido</label>
                                                <input type="text" class="form-control" name="last" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>Tu Teléfono</label>
                                                <input type="text" class="form-control" name="mob" onchange="validateMob(this)" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>Tu Email</label>
                                                <input type="text" class="form-control" name="email"  onchange="validateEmail(this)" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 emailvalid">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <label>PIN</label>
                                                <input type="text" class="form-control" name="otp">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 emailvalid">
                                        <div class="form-group mb0" style="height:108px;padding-top:40px;">
                                            <label></label>
                                            <a onclick="validOtp(this)">Validar</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <label>Fecha de Nacimiento</label>
                                                <input type="date" class="form-control" name="dob" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="email-field">
                                            <div class="form-input">
                                                <label>Edad</label>
                                                <input type="number" class="form-control" name="age" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <label>DNI / PASAPORTE</label>
                                                <input type="text" class="form-control" name="pid" onchange="validId(this)" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="email-field">
                                            <div class="form-input">
                                                <label>Nacionalidad </label>
                                                <input type="text" class="form-control" name="nat" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="message-field">
                                            <div class="form-input">
                                                <label>Domicilio </label>
                                                <textarea class="form-control" rows="1" name="addr" required autofocus></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>¿Como se enteró de la FUO? </label>
                                                <select class="form-control" name="refer" required autofocus>
                                                    <option value="">--Seleccione--</option>
                                                    <option value="Redes Sociales">Redes Sociales</option>
                                                    <option value="Cadena 3">Cadena 3</option>
                                                    <option value="Web">Web</option>
                                                    <option value="Boca en Boca">Boca en Boca</option>
                                                    <option value="Otro">Otro</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>¿Trabaja?</label>
                                                <select class="form-control" name="empstat" required autofocus>
                                                    <option value="">--Seleccione--</option>
                                                    <option value="Relación de Dependencia">Relación de Dependencia</option>
                                                    <option value="Por Cuenta Propia">Por Cuenta Propia</option>
                                                    <option value="Jubilado">Jubilado</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>¿Cúal es su situación laboral y cúal es su objetivo?</label>
                                                <select class="form-control" name="emp" required autofocus>
                                                    <option value="">--Seleccione--</option>
                                                    <option value="Desocupado y desea capacitarse para ejercer el oficio.">Desocupado y desea capacitarse para ejercer el oficio.</option>
                                                    <option value="Trabajando en el rubro y quiero perfeccionarme en el oficio para ampliar posibilidades laborales.">Trabajando en el rubro y quiero perfeccionarme en el oficio para ampliar posibilidades laborales.</option>
                                                    <option value="Tengo otra actividad y quiero aprender para proyecto personal.">Tengo otra actividad y quiero aprender para proyecto personal.</option>
                                                    <option value="Sólo por Hobby.">Sólo por Hobby.</option>
                                                    <option value="Otro">Otro</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>Nivel Educativo</label>
                                                <select class="form-control" name="edu" required autofocus>
                                                    <option value="">--Seleccione--</option>
                                                    <option value="Primario">Primario</option>
                                                    <option value="Secundario">Secundario</option>
                                                    <option value="Terciario">Terciario</option>
                                                    <option value="Universitario">Universitario</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <label>Contraseña</label>
                                                <input type="password" class="form-control" name="pass" onchange="validPass(this)" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="checkbox" name="tc" required autofocus style="width:fit-content;min-height: auto;">&nbsp; Acepto los <a href="https://docs.google.com/document/d/14mNO7lTfyDdLnOlY1UCvUm4kkFq71chSOedu3EBaRv0/" target="__new">terminos y condiciones</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group mb0">
                                            <label></label>
                                            <button type="submit">Registrar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-2"></div>
            </div>
        </div>
    </section>
    <!--ABOUT AREA END-->
    <script>
        var erroremail=0;
        var emailvl=0;
        var eid=0;
        var epass=0;
        var emob=0;
        function showFrom(){
            $(".reg-form").show();
            $(".reg-wel").hide();
        }
        function showFroml(){
            $(".log-form").show();
            $(".reg-wel").hide();
        }
        function showFog()
        {
            $(".log-form").hide();
            $(".fog-form").show();
        }
        function dspError(m)
        {
            $.toast({
                        text: m,
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
        }
        
        var otp=0;
        var etry=0;
        function validateEmail(t)
        {
            emailvl=1;
            $("input[name=otp]").prop('disabled', true);
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(t.value))
            {
                erroremail=0;
                $('.emailvalid').css('display','block');
                  $.post("<?php echo base_url();?>Home/sendOtp/0/",
                  {
                    email: t.value
                  },
                  function(data, status){
                     if(status=='success')
                     {
                         var d=JSON.parse(data);
                         if(d.status==1)
                         {
                             $(t).css({"border": "0px solid #3ee792"});
                             $("input[name=otp]").prop('disabled', false);
                             otp=d.data;
                            $.toast({
                            text: "Por Favor revisa tu correo electrónico para encontrar el PIN.",
                            heading: 'Exitoso',
                            icon: 'success',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 3000,
                            stack: 10,
                            position: 'bottom-right', 
                            textAlign: 'left', 
                            loader: true, 
                            loaderBg: '#24b07b'
                            });
                         }else
                         {
                             $(t).css({"border": "1px solid #3ee792"});
                             $('.emailvalid').css('display','none');
                             t.value='';
                             $.toast({
                                text: d.data,
                                heading: 'Error',
                                icon: 'error',
                                showHideTransition: 'fade',
                                allowToastClose: true,
                                hideAfter: 3000,
                                stack: 10,
                                position: 'bottom-right', 
                                textAlign: 'left', 
                                loader: true, 
                                loaderBg: '#24b07b'
                                });
                         }
                     }else
                     {
                         etry++;
                         if(etry>5)
                         {
                         validateEmail(t);
                         }else
                         {
                             $(t).css({"border": "1px solid #3ee792"});
                             $.toast({
                                text: "El servidor está inactivo, intente más tarde",
                                heading: 'Error',
                                icon: 'error',
                                showHideTransition: 'fade',
                                allowToastClose: true,
                                hideAfter: 3000,
                                stack: 10,
                                position: 'bottom-right', 
                                textAlign: 'left', 
                                loader: true, 
                                loaderBg: '#24b07b'
                                });
                         }
                         
                     }
                  });
                  
            }else
            {
                erroremail=1;
                $('.emailvalid').css('display','none');
                      $.toast({
                        text: "email incorrecto.",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                        $(t).css({"border": "1px solid #3ee792"});
                        $(t).focus();
            }
        }
        function validId(t)
        {
            eid=1;
            if(t.value!='')
            {
            $.post("<?php echo base_url();?>Home/sendOtp/1/",
                  {
                    pid: t.value
                  },
                  function(data, status){
                      if(status=='success')
                     {
                         if(data==1)
                         {
                             eid=0;
                             $(t).css({"border": "0px solid #3ee792"});
                         }
                         else
                         {
                             $.toast({
                        text: data,
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                        t.value='';
                        $(t).css({"border": "1px solid #3ee792"});
                        $(t).focus();
                         }
                     }else
                     {
                         validId(t);
                     }
                  });
            }
            
        }
        var clickotp=0;
        function validOtp(t)
        {
            if(otp !=0)
            {
                var aotp=$("input[name=otp]").val();
                if(aotp==otp)
                {
                    emailvl=0;
                    $("input[name=otp]").css({"border": "0px solid #3ee792"});
                    $("input[name=otp]").val('');
                    $('.emailvalid').css('display','none');
                        $.toast({
                        text: "Correo electrónico validado con éxito.",
                        heading: 'Exitoso',
                        icon: 'success',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                }else
                {
                    emailvl=1;
                    $("input[name=otp]").css({"border": "1px solid #3ee792"});
                    $("input[name=otp]").focus();
                    $.toast({
                        text: "OTP es incorrecto.",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                        
                }
            }
            
        }
        
        function validPass(t)
        {
            epass=0;
           if(t.value!='')
           {
               var passw=  /^[A-Za-z]\w{7,14}$/;
               if(!t.value.match(passw))
               {
                   $(t).css({"border": "1px solid #3ee792"});
                    $(t).focus();
                   $.toast({
                        text: "De 7 a 15 caracteres que contienen solo caracteres, dígitos numéricos, guiones bajos y el primer carácter debe ser una letra",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                        epass=1;
               }
               else
               {
                   $(t).css({"border": "0px solid #3ee792"});
                   epass=0;
               }
           }
        }
        
        function validateMob(t)
        {
            emob=0;
           if(t.value!='')
           {
               if(!/^\d{8,14}$/.test(t.value))
           {
                   $(t).css({"border": "1px solid #3ee792"});
                    $(t).focus();
                   $.toast({
                        text: "Ingrese un número válido",
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 3000,
                        stack: 10,
                        position: 'bottom-right', 
                        textAlign: 'left', 
                        loader: true, 
                        loaderBg: '#24b07b'
                        });
                        emob=1;
               }
               else
               {
                   $(t).css({"border": "0px solid #3ee792"});
                   emob=0;
               }
           }
        }
        
        document.getElementById( 'reg-form' ).addEventListener('submit', function(e) {
           var er=0;

           $("input[name=mob]").css({"border": "0px solid #3ee792"});
           $("input[name=email]").css({"border": "0px solid #3ee792"});
           $("input[name=pid]").css({"border": "0px solid #3ee792"});
           $("input[name=pass]").css({"border": "0px solid #3ee792"});
           if(emob==1)
           {
               dspError('Ingrese un número válido');
               $("input[name=mob]").css({"border": "1px solid #3ee792"});
               er=1;
           }
           if(erroremail==1)
           {
               dspError('Ingrese un email valido');
               $("input[name=email]").css({"border": "1px solid #3ee792"});
               e.preventDefault();
           }
           if(eid==1)
           {
               dspError('Ingrese una identificación válida');
               $("input[name=pid]").css({"border": "1px solid #3ee792"});
               e.preventDefault();
           }
           if(emailvl==1)
           {
               dspError('por favor verifique su correo electrónico');
               $("input[name=email]").css({"border": "1px solid #3ee792"});
               e.preventDefault();
           }
           if(epass==1)
           {
               dspError('Ingrese una contraseña válida');
               $("input[name=pass]").css({"border": "1px solid #3ee792"});
               e.preventDefault();
           }
           
           if(er==1)
           {
               e.preventDefault();
           }
        });
        

    </script>
    
    
