
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Teacher/result/<?php echo $id;?>">Atrás</a>
<h4 class="page-title">Agregar hoja de cálculo</h4>
<form method="post" action="<?php echo base_url()?>TeacherBack/saveResult/<?php echo $id;?>" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Hoja de papel</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Examen</label>
    			<input type="text" class="form-control" id="title" name="title" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="dsg">Marcas</label>
    			<input type="text" class="form-control" id="mark" name="marks" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
