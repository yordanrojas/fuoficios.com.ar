<a class="btn btn-success mb-3" href="<?php echo base_url();?>Teacher/viewBtach/<?php echo $bid;?>">Atrás</a>
<?php
$ids=array();
$attend=array();
  foreach($batch->result() as $row)
  {
      $attend=json_decode($row->attend);
  }
  $a=array();
?>
<style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: inherit;
    left: inherit;
}
</style>
<h4 class="page-title">Asistencia</h4>
<form id="reg-form" method="POST" action="<?php echo base_url();?>TeacherBack/attend/<?php echo $bid;?>">
    	    <div class="table-responsive">
              <table class="table">
                  <thead>
                      <tr>
                      <th>Nombre</th>
                      <?php
                      foreach($attend as $key=>$row)
                      {
                         echo "<th>".date('d/m/Y',strtotime($key))."</th>";
                         $a[]=$key;
                      }
                      ?>
                      <th>Dias</th>
                      <th>Porcentaje</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach($s->result() as $row)
                      {
                          $ids[]=$row->id;
                          echo "<tr>";
                          echo "<td style='width:200px;'>".$row->name."&nbsp;".$row->lname."</td>";
                          $data=json_decode($row->data);
                          $c=0;
                          foreach($data as $k=>$r)
                          {
                                $date1=date_create(date('d-m-Y'));
                                $date2=date_create($a[$c]);
                                $diff=date_diff($date1,$date2);
                              ?><td class='text-center' style='width:200px;'><input type='checkbox' name='<?php echo 'id'.$row->id.'-'.$k;?>' value='1' <?php if($r==1){ echo 'checked';}?>  <?php 
                              if($diff->format("%R%a") < 0){ echo 'disabled';}
                              ?>></td><?php
                              $c++;
                          }
                          echo "<td style='width:200px;'>".$row->pday."/".$row->tday."</td>";
                          echo "<td style='width:200px;'>".intval($row->total)."%</td>";
                          echo "</tr>";
                      }
                      ?>
                  </tbody>
              </table>
            </div>
            <input type="hidden" name="ids" value='<?php echo json_encode($ids);?>'></input>
            <input type="submit" value="Enviar" class="btn btn-success"></input>
            <input type="hidden" name="dates" value='<?php echo json_encode($a);?>'></input>
</form>

<script>
    
    document.getElementById( 'reg-form' ).addEventListener('submit', function(e) {
        $( "input" ).prop( "disabled", false );
    });
</script>
