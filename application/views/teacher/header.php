<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Dashboard</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<link rel="stylesheet" href="<?php echo base_url();?>backend/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
	<link rel="stylesheet" href="<?php echo base_url();?>backend/assets/css/ready.css">
	<link rel="stylesheet" href="<?php echo base_url();?>backend/assets/css/demo.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
	<script src="<?php echo base_url();?>backend/assets/js/core/jquery.3.2.1.min.js"></script>
	<style>
	.main-panel {
	    background: #ffffff;
	    
	}
	    .page-item.active .page-link {
	        color: #fff;
	        background-color: #31dc89;
	        border-color: #31dc89;
	    }
	    .sidebar .nav .nav-item.active a:before {
	        background: #f18c6f;
	        
	    }
	    .sidebar .nav .nav-item.active a i,.sidebar .nav .nav-item.active a {
	        color: #f18c6f;
	        
	    }
	    .sidebar .nav .nav-item a:hover:before {
	        background: #f18c6f;
	        
	    }
	    .sidebar .nav .nav-item a:hover
	    {
	        color: #f18c6f;
	    }
	    .main-header .logo-header {
	        width: 50%;
	    }
	</style>
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<div class="logo-header">
				<a href="<?php echo base_url()?>Teacher/" class="logo">
					Sistema de Control Profesor
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<button class="topbar-toggler more"><i class="la la-ellipsis-v"></i></button>
			</div>
			<nav class="navbar navbar-header navbar-expand-lg">
				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item dropdown">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false"> <img src="<?php if(!empty($this->session->userData('timg'))){ echo base_url().$this->session->userData('timg');}else{ echo base_url().'backend/assets/img/profile.jpg';}?>" alt="user-img" width="36" class="img-circle"><span ><?php echo $this->session->userData('tuser');?></span></span> </a>
							<ul class="dropdown-menu dropdown-user">
								<li>
									<div class="user-box">
										<div class="u-img"><img src="<?php if(!empty($this->session->userData('timg'))){ echo base_url().$this->session->userData('timg');}else{ echo base_url().'backend/assets/img/profile.jpg';}?>" alt="user"></div>
										<div class="u-text">
											<h4><?php echo $this->session->userData('tuser');?></h4>
										</div>
									</li>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="<?php echo base_url()?>Teacher/myProfile"><i class="ti-user"></i> Mi perfil</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="<?php echo base_url()?>Teacher/logout"><i class="fa fa-power-off"></i> Cerrar sesión</a>
								</ul>
								<!-- /.dropdown-user -->
							</li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="sidebar">
				<div class="scrollbar-inner sidebar-wrapper">
					<ul class="nav">
						<li class="nav-item <?php if($nav==1){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Teacher/">
								<i class="la la-dashboard"></i>
								<p>Tablero</p>
							</a>
						</li>
                        <li class="nav-item <?php if($nav==2){ echo 'active';}?>">
							<a href="<?php echo base_url()?>Teacher/batch">
								<i class="la la-dashboard"></i>
								<p>Grupos</p>
							</a>
						</li>

					</ul>
				</div>
			</div>
			<div class="main-panel">
				<div class="content">
					<div class="container-fluid">
					