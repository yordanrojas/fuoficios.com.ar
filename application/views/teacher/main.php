<style type="text/css">
	table{
border-collapse:collapse;
}
td{
width: 50px;
height: 50px;
text-align: center;
border: 1px solid #e2e0e0;
font-size: 18px;
font-weight: bold;
}
th{
height: 50px;
padding-bottom: 8px;
background:#31dc89 ;
font-size: 20px;
}
.prev_sign a, .next_sign a{
color:white;
text-decoration: none;
}
tr.week_name{
font-size: 16px;
font-weight:400;
color:red;
width: 10px;
background-color: #efe8e8;
}
.highlight{
background-color:#31dc89;
color:white;
height: 79px;
padding-top: 13px;
padding-bottom: 7px;
}
.highlight2{
background-color:#f18f71;
color:white;
height: 79px;
padding-top: 13px;
padding-bottom: 7px;
}
.highlight a,.highlight2 a{
    color:#fff;
    text-decoration:none;
} 
.calender .days td
{
	width: 80px;
	height: 80px;
}
.calender .hightlight
{
	font-weight: 600px;
}
.calender .days td:hover
{
	background-color: #DEF;
}

.table > tbody > tr > td, .table > tbody > tr > th {
    height: 80px;
    padding:0px!important;
}
</style>
<div>
    <button class="btn btn-success" onclick="prvDate()"><<</button><button class="btn btn-success" style="float:right" onclick="nxtDate()">>></button>
</div>
<div class="cal">
    
</div>
<script>
var date='<?php echo date('Y-m');?>'
var v=0;
$( document ).ready(function() {
    $.post("<?php echo base_url();?>Teacher/cal/",
                  {
                    d: date,
                    v: v
                  },
                  function(data, status){
                      $('.cal').html(data);
                  });
});

 function nxtDate()
{
    v='+1';
        $.post("<?php echo base_url();?>Teacher/cal/",
                  {
                    d: date,
                    v: v
                  },
                  function(data, status){
                      $('.cal').html(data);
                      var dt = new Date(date);
                      dt.setMonth( dt.getMonth() + 1 );
                      date=dt.toISOString().substring(0, 10);
                  });
}

 function prvDate()
{
    v='-1';
        $.post("<?php echo base_url();?>Teacher/cal/",
                  {
                    d: date,
                    v: v
                  },
                  function(data, status){
                      $('.cal').html(data);
                      var dt = new Date(date);
                      dt.setMonth( dt.getMonth() - 1 );
                      date=dt.toISOString().substring(0, 10);
                  });
}
    

</script>