
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Teacher/">Atrás</a>
<form method="post" action="<?php echo base_url()?>TeacherBack/updateProfile/<?php echo $id;?>" enctype="multipart/form-data" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Mi perfil</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Nombre de usuario</label>
    			<input type="email" class="form-control" id="name" name="email" value="<?php echo $this->session->userdata('tuser');?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="pass">Contraseña</label>
    			<input type="text" class="form-control" id="pass" name="pass" placeholder="*******" >
    		</div>
    		<div class="form-group">
    			<label for="img">Imagen</label>
    			<input type="file" class="form-control-file" id="img" name="img" onchange="return fileValidation()">
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>

<script>
    
     function fileValidation(){
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg|\.gif|\.tiff|\.svg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Sube solo imágenes.');
            fileInput.value = '';
            return false;
        }
    }
</script>