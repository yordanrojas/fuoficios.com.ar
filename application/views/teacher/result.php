        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Teacher/addResult/<?php echo $id;?>">Añadir</a>
        <h4 class="page-title">Hoja de papel</h4>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Sr</th>
                        <th>Examen</th>
                        <th>Marcas</th>
                        <th style="width:70px;">Borrar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($result->num_rows()>0)
                    {
                        $i=1;
                        foreach($result->result() as $row)
                        {
                            ?>
                            <tr>
                                <td onclick="window.location.href='<?php echo base_url()."Teacher/viewResult/".$id."/".$row->id;?>'"><?php echo $i;?></td>
                                 <td onclick="window.location.href='<?php echo base_url()."Teacher/viewResult/".$id."/".$row->id;?>'"><?php echo $row->title;?></td>
                                 <td onclick="window.location.href='<?php echo base_url()."Teacher/viewResult/".$id."/".$row->id;?>'"><?php echo $row->marks;?></td>
                                 <td class="text-center" style="font-size:1.1rem;"  onclick="conDelete(<?php echo $row->id;?>)"><span class="la la-bitbucket"></span></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
            
            <script>
                function conDelete(i)
                {
                    var check = confirm("Esta seguro?");
                    if(check== true)
                    {
                        window.location.href='<?php echo base_url()."Teacher/deleteResult/".$id."/";?>'+i;
                    }
                }
            </script>