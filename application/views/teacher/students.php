        <style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: inherit;
    left: inherit;
}
</style>
        <a class="btn btn-success mb-3" href="<?php echo base_url();?>Teacher/viewBtach/<?php echo $id;?>">Atrás</a>
        <h4 class="page-title">Estudiantes</h4>
        <table id="example" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>Herramienta</th>
                        <th>Asignación</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($students->num_rows()>0)
                    {
                        $i=1;
                        foreach($students->result() as $row)
                        {
                            ?>
                            <tr>
                                <td><?php echo $row->id;?></td>
                                 <td><?php echo $row->name.'&nbsp;'.$row->lname;?></td>
                                 <td><?php echo $row->email;?></td>
                                 <td><?php echo $row->mob;?></td>
                                 <td style="text-align:center;"><input type="checkbox" value="1" onclick="checkTool(this,<?php echo $row->id;?>,<?php echo $row->uid;?>)" <?php if($row->stat==1){ echo "checked";}?>></td>
                                 <td style="text-align:center;"><a href="<?php echo base_url();?>Teacher/result/<?php echo $row->id;?>" class="btn btn-success" style="padding:5px;">Editar</a></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
            
            <script>
                function checkTool(t,id,uid)
                {
                    var c=0;
                    if($(t).prop("checked") == true){
                        c=1;
                    }
                $.post("<?php echo base_url();?>Teacher/tool/",
                  {
                    id: id,
                    uid: uid,
                    c: c
                  },
                  function(data, status){
                  });
                }
            </script>