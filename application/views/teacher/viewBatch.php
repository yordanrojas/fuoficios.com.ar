<a class="btn btn-success mb-3" href="<?php echo base_url();?>Teacher/batch">Atrás</a>
<h4 class="page-title">Grupo</h4>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Detalles</div>
        </div>
    	<div class="card-body row">
    	    <?php
    	    $id='';
    	    $data='';
    	    foreach($batch->result() as $row)
    	    {
    	        $id=$row->id;
    	        $data=json_decode($row->data);
    	        ?>
    	    <div class="col-sm-12">
    	        Curso:-<?php echo $row->title;?>
    	    </div>
    	    <div class="col-sm-6">
    	        Grupo:-<?php echo $row->code.sprintf("%02d",$row->sr).date('y',strtotime('01-01-'.$row->yr));?>
    	        </div>
    	        <div class="col-sm-6">
    	        Estudiantes:-<?php echo $row->count;?>
    	        </div>
    	    <?php }?>
    		<div class="col-sm-12">
    		   Desde:-<?php echo date('m-d-Y',strtotime($data->start));?><br>
    		   A:-<?php echo date('m-d-Y',strtotime($data->end));?>
    		</div>
    		<div class="col-sm-12">
    		   <?php
    		   foreach($data->daytime as $row2)
    		   {
    		       switch($row2->day)
                    {
                        case 1:
                        echo "Lunes:- ";
                        break;
                        case 2:
                        echo "Martes:- ";
                        break;
                        case 3:
                        echo "Miércoles:- ";
                        break;
                        case 4:
                        echo "Jueves:- ";
                        break;
                        case 5:
                        echo "Viernes:- ";
                        break;
                        case 6:
                        echo "Sábado:- ";
                        break;
                    }
    		       echo $row2->from.'-'.$row2->to.'<br>';
    		   }
    		   ?>
    		</div>
    		<div class="col-sm-12">Enlace de grupo de chat: <a href='<?php echo $row->link;?>'>Ver</a></div>
    		<?php
    	        $room=json_decode($row->room);
    	        ?>
    	        <div class="col-sm-6">
    	            Salón de clases:- <?php echo $room->croom;?>
    	            </div>
    	           <div class="col-sm-6">
    	            Laboratorio practico:- <?php echo $room->proom;?>
    	            </div>
    	</div>
    	
    	<div class="card-action">
    	    <button class="btn btn-success" onclick="window.location.href='<?php echo base_url();?>Teacher/attend/<?php echo $id;?>'">Asistencia</button>&nbsp;&nbsp;<button class="btn btn-success" onclick="window.location.href='<?php echo base_url();?>Teacher/students/<?php echo $id;?>'">Estudiantes</button>
    	</div>
    </div>