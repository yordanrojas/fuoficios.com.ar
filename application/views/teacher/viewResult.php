<?php
$rid='';
$title='';
$mark='';
foreach($result->result() as $row)
{
    $rid=$row->id;
    $title=$row->title;
    $marks=$row->marks;
}

?>
<a class="btn btn-success mb-3" href="<?php echo base_url();?>Teacher/result/<?php echo $id;?>">Atrás</a>
<h4 class="page-title">Agregar hoja de cálculo</h4>
<form method="post" action="<?php echo base_url()?>TeacherBack/editResult/<?php echo $id;?>/<?php echo $rid;?>" >
    <div class="card">
        <div class="card-header">
            <div class="card-title">Hoja de papel</div>
        </div>
    	<div class="card-body">
    		<div class="form-group">
    			<label for="name">Examen</label>
    			<input type="text" class="form-control" id="title" name="title" value="<?php echo $title;?>" required autofocus>
    		</div>
    		<div class="form-group">
    			<label for="dsg">Marcas</label>
    			<input type="text" class="form-control" id="mark" name="marks" value="<?php echo $marks;?>" required autofocus>
    		</div>
    	</div>
    	<div class="card-action">
    		<button class="btn btn-success">Enviar</button>
    	</div>
    </div>
</form>
