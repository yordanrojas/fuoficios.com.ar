 <style>
        .mainmenu-area {
            background: #171932!important;
        }
        .mb-5
        {
            padding-bottom:50px;
        }
        .col-bo
        {
            border-left:1px solid;
        }
        @media only screen and (max-width: 575px) {
            .col-bo
            {
                border-left:0px;
                
            }
        }
        </style>
    <section class="section-padding about-area mb-5" id="about">
       
        <div class="container">
            <div class="row reg-wel" style="margin-top:50px;">
                
                <div class="col-sm-6 area-title text-center">
                    <div class="box-icon features-box-icon mx-auto">
                            <i class="icofont icofont-user"></i>
                        </div>
                    <h3>Nuevo registro</h3>
                    <div class="enroll-button center xs-center">
                        <a href="<?php echo base_url();?>Home/register">Registro</a>
                    </div>
                </div>
                <div class="col-sm-6 area-title text-center col-bo">
                    <div class="box-icon features-box-icon mx-auto">
                            <i class="icofont icofont-paper"></i>
                        </div>
                    <h3>Ya registrado</h3>
                    <div class="enroll-button center xs-center">
                        <a href="<?php echo base_url();?>Home/login">iniciar sesión</a>
                    </div>
                </div>
            </div>
            </section>