<style>
        .mainmenu-area {
            background: #171932!important;
        }
        .mt-5
        {
            padding-top:100px!important;
        }
        .mb-5
        {
            padding-bottom:50px;
        }
        </style>
        
        <section class="course-area padding-top mt-5" id="courses">
            <div class="container">
              <?php foreach($news->result() as $row)
            {
                ?>
                <div class="area-title text-center wow fadeIn" style="margin-bottom:0px;">
                        <h2 class="xs-font26"><?php echo $row->title;?></h2>
                </div>
                <img src="<?php echo base_url().$row->img;?>" width="100%">
                <br>
                <br>
                <p style="text-align:justify;"><?php echo $row->des;?></p>
                <br><br><br>
                <?php
                
            }
            ?>  
            </div>
            
        </section>